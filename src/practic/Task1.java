package practic;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.Map;

/**
 * Created by Administrator on 26.08.2015.
 */
public class Task1 {
    private static Map<String, String> iSO3;
    private static Map<String, String> iSO2;
    private static Map<String, String> numI;
    private static Map<String, String> fIPS;
    private static Map<String, String> countries;

    public Task1() {
        ObjectMapper mapper = new ObjectMapper();
        try {
            iSO3 = mapper.readValue(new File("D:\\Kain\\KainsJavaLocalGit\\src\\resources\\Iso3.json"),
                    new TypeReference<Map<String, String>>() {
                    });
            iSO2 = mapper.readValue(new File("D:\\Kain\\KainsJavaLocalGit\\src\\resources\\Iso2.json"),
                    new TypeReference<Map<String, String>>() {
                    });
            numI = mapper.readValue(new File("D:\\Kain\\KainsJavaLocalGit\\src\\resources\\numI.json"),
                    new TypeReference<Map<String, String>>() {
                    });
            fIPS = mapper.readValue(new File("D:\\Kain\\KainsJavaLocalGit\\src\\resources\\fIPS.json"),
                    new TypeReference<Map<String, String>>() {
                    });
            countries = mapper.readValue(new File("D:\\Kain\\KainsJavaLocalGit\\src\\resources\\Countries.json"),
                    new TypeReference<Map<String, String>>() {
                    });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        Task1 task1 = new Task1();
        task1.printMap();

    }

    public void printMap() {
        System.out.println(iSO2);
        System.out.println(fIPS);
        System.out.println(iSO3);
        System.out.println(numI);
    }

    public String checkUserInput (String input){
        String out = "";
        if (numI.containsKey(input)){
            return numI.get(input);
        }
        input = input.toUpperCase();
        if(input.length()==3 && iSO3.containsKey(input)){
            return iSO3.get(input);
        }
        if(fIPS.containsKey(input) && iSO2.containsKey(input)){

        }
        return null;
    }




}

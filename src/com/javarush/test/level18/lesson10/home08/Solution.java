package com.javarush.test.level18.lesson10.home08;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

/* Нити и байты
Читайте с консоли имена файлов, пока не будет введено слово "exit"
Передайте имя файла в нить ReadThread
Нить ReadThread должна найти байт, который встречается в файле максимальное число раз, и добавить его в словарь resultMap,
где параметр String - это имя файла, параметр Integer - это искомый байт.
Закрыть потоки. Не использовать try-with-resources
*/

public class Solution {
    public static Map<String, Integer> resultMap = new HashMap<String, Integer>();

    public static void main(String[] args) {

       /* ReadThread readThread = new ReadThread("D:\\javaRushTest\\testFile.txt");
        readThread.start();
        ReadThread readThread1 = new ReadThread("D:\\javaRushTest\\testFile1.txt");
        readThread1.start();
        ReadThread readThread2 = new ReadThread("D:\\javaRushTest\\testFile2.txt");
        readThread2.start();
        try {
            readThread.join();
            readThread1.join();
            readThread2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(resultMap);*/
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        try {
            String path;
            while (!"exit".equals(path = bufferedReader.readLine())){
                ReadThread readThread = new ReadThread(path);
                readThread.start();
            }
        } catch (IOException e) {
            e.printStackTrace();
            try {
                bufferedReader.close();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
        System.out.println(resultMap);
    }

    public static class ReadThread extends Thread {
        String name;

        public ReadThread(String fileName) {
            //implement constructor body
            name = fileName;
        }
        // implement file reading here - реализуйте чтение из файла тут

        @Override
        public void run() {
            int[] allBytes = new int[256];

            try {
                byte[] fileBytes = Files.readAllBytes(Paths.get(name));
                for (byte b : fileBytes) {
                    ++allBytes[ b&255];
                }
                int maxbyte = 0;
                int maxCount = 0;
                for (int i = 0; i < allBytes.length; i++) {
                    if (allBytes[i] > maxCount) {
                        maxCount = allBytes[i];
                        maxbyte = i;
                    }
                }

                resultMap.put(name, maxbyte);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}

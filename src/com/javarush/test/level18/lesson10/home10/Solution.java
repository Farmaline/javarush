package com.javarush.test.level18.lesson10.home10;

/* Собираем файл
Собираем файл из кусочков
Считывать с консоли имена файлов
Каждый файл имеет имя: [someName].partN. Например, Lion.avi.part1, Lion.avi.part2, ..., Lion.avi.part37.
Имена файлов подаются в произвольном порядке. Ввод заканчивается словом "end"
В папке, где находятся все прочтенные файлы, создать файл без приставки [.partN]. Например, Lion.avi
В него переписать все байты из файлов-частей используя буфер.
Файлы переписывать в строгой последовательности, сначала первую часть, потом вторую, ..., в конце - последнюю.
Закрыть все потоки ввода-вывода
Темповые файлы создавать нельзя, т.к. на сервере заблокирована возможность создания каких любо файлов
*/

import java.io.*;
import java.util.ArrayList;

public class Solution {
    static final String NAME = "\\.part";

    public static void main(String[] args) throws IOException {

        ArrayList<String> fileNameArray = new ArrayList<>();

        BufferedReader scan = new BufferedReader(new InputStreamReader(System.in));

        String fileName;

        while (!"end".equals(fileName = scan.readLine())) {
            fileNameArray.add(fileName);
        }

        FileOutputStream fileOutputStream = new FileOutputStream(resultFileName(fileNameArray.get(0)));

        int start = 1;
        FileInputStream fileReader = null;

        for (int i = 0; i < fileNameArray.size(); i++) {
            for (String part : fileNameArray) {
                if (getIntFromName(part) == start) {

                    fileReader = new FileInputStream(part);

                    byte [] buffer = new byte[fileReader.available()];

                    if (fileReader.available()> 0)
                    {
                        fileReader.read(buffer);
                        fileOutputStream.write(buffer);
                        fileOutputStream.flush();

                    }
                    start++;
                }


            }
        }
        scan.close();
        fileOutputStream.close();
        fileReader.close();
    }

    public static int getIntFromName(String fileName) {
        String[] arrayString = fileName.split(NAME);
        return Integer.parseInt(arrayString[arrayString.length - 1]);
    }

    public static String resultFileName(String name) {
        String[] stringURL = name.split(NAME);

        return stringURL[0];
    }


}

package com.javarush.test.level18.lesson10.bonus01;

/* Шифровка
Придумать механизм шифровки/дешифровки

Программа запускается с одним из следующих наборов параметров:
-e fileName fileOutputName
-d fileName fileOutputName
где
fileName - имя файла, который необходимо зашифровать/расшифровать
fileOutputName - имя файла, куда необходимо записать результат шифрования/дешифрования
-e - ключ указывает, что необходимо зашифровать данные
-d - ключ указывает, что необходимо расшифровать данные
*/

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class Solution {
    public static void main(String[] args) {

        File fileOne = new File(args[1]);
        File fileTwo = new File(args[2]);
        FileInputStream fileInputStream = null;
        FileOutputStream fileOutputStream = null;
        try {
            fileInputStream = new FileInputStream(fileOne);
            fileOutputStream = new FileOutputStream(fileTwo);
        } catch (IOException e) {
            e.printStackTrace();
        }


        if ("-e".equals(args[0])) {
            try {
                while (fileInputStream.available() > 0) {
                    int data = fileInputStream.read() + 1;
                    fileOutputStream.write(data);

                }
            } catch (IOException e) {
                e.printStackTrace();
            }


        }
        if ("-d".equals(args[0])) {
            try {
                while (fileInputStream.available() > 0) {
                    int data = fileInputStream.read() - 1;
                    fileOutputStream.write(data);

                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        try {
            fileInputStream.close();
            fileOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}

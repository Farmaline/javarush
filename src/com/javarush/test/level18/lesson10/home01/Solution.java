package com.javarush.test.level18.lesson10.home01;

/* Английские буквы
В метод main первым параметром приходит имя файла.
Посчитать количество букв английского алфавита, которое есть в этом файле.
Вывести на экран число (количество букв)
Закрыть потоки
*/



import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException {



        int latter = 0;

        FileInputStream fileInputStream = new FileInputStream(args[0]);

        while  (fileInputStream.available() > 0)
        {
            int data = fileInputStream.read();
            if (data >= 97 && data <= 122 || data >= 65 && data <= 90)
            {
                latter++;
            }


        }

        System.out.println(latter);



    }
}

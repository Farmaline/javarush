package com.javarush.test.level18.lesson10.home07;

/* Поиск данных внутри файла
Считать с консоли имя файла
Найти в файле информацию, которая относится к заданному id, и вывести ее на экран в виде, в котором она записана в файле.
Программа запускается с одним параметром: id (int)
Закрыть потоки

В файле данные разделены пробелом и хранятся в следующей последовательности:
id productName price quantity

где id - int
productName - название товара, может содержать пробелы, String
price - цена, double
quantity - количество, int

Информация по каждому товару хранится в отдельной строке
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException {
        String result = args[0];
        BufferedReader scan = new BufferedReader(new InputStreamReader(System.in));


        FileReader fr = new FileReader(scan.readLine());
        BufferedReader in = new BufferedReader(fr);
        String text;
        while ((text = in.readLine()) !=null)
        {

            String [] idText = text.split(" ");
            if (result.equals(idText[0]))
            {
                System.out.println(text);
            }
        }
        scan.close();
        fr.close();
        in.close();
    }
}

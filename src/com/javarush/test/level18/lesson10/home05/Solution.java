package com.javarush.test.level18.lesson10.home05;

/* Округление чисел
Считать с консоли 2 имени файла
Первый файл содержит вещественные(дробные) числа, разделенные пробелом. Например, 3.1415
Округлить числа до целых и записать во второй файл
Закрыть потоки
Принцип округления:
3.49 - 3
3.50 - 4
3.51 - 4
*/

import java.io.*;
import java.util.ArrayList;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader scan = new BufferedReader(new InputStreamReader(System.in));
        String fileOne = scan.readLine();
        String fileTwo = scan.readLine();


        ArrayList<Integer> arrayList = new ArrayList<Integer>();

        FileReader fileReader = new FileReader(new File(fileOne));

        String buffer = "";
        while (fileReader.ready()) {

            buffer += (char) fileReader.read();
        }


        String[] stringBuff = buffer.split(" ");


        for (int i = 0; i < stringBuff.length; i++) {
            arrayList.add((int) Math.round(Double.parseDouble(stringBuff[i])));
        }


        String result = "";

        for (Integer text : arrayList) {
            result += text + " ";
        }
        result.trim();

        Writer out = new FileWriter(fileTwo, true);
        out.write(result);
        out.flush();
        out.close();

    }
}

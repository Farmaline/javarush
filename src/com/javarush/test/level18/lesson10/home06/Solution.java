package com.javarush.test.level18.lesson10.home06;

/* Встречаемость символов
Программа запускается с одним параметром - именем файла, который содержит английский текст.
Посчитать частоту встречания каждого символа.
Отсортировать результат по возрастанию кода ASCII (почитать в инете). Пример: ','=44, 's'=115, 't'=116
Вывести на консоль отсортированный результат:
[символ1]  частота1
[символ2]  частота2
Закрыть потоки

Пример вывода:
, 19
- 7
f 361
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException {

        int[] chars = new int[255];


        FileInputStream fileInputStream = new FileInputStream(args[0]);


        while (fileInputStream.available() > 0) {
            int data = fileInputStream.read();

            if (chars[data] == 0) {
                chars[data] = 1;
            } else {
                chars[data]++;
            }

        }

        for (int i = 0; i < chars.length; i++) {
            if (chars[i] != 0) {
                System.out.println((char) i + " " + chars[i]);
            }
        }

    }
}

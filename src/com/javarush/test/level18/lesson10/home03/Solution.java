package com.javarush.test.level18.lesson10.home03;

/* Два в одном
Считать с консоли 3 имени файла
Записать в первый файл содержимого второго файла, а потом дописать содержимое третьего файла
Закрыть потоки
Темповые файлы создавать нельзя, т.к. на сервере заблокирована возможность создания каких любо файлов
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException {

        BufferedReader scan = new BufferedReader(new InputStreamReader(System.in));

        String fileOne = scan.readLine();
        String fileTwo = scan.readLine();
        String fileThree = scan.readLine();

        FileOutputStream fileOutputStreamOne = new FileOutputStream(fileOne);
        FileInputStream fileInputTwo = new FileInputStream(fileTwo);

        while (fileInputTwo.available()>0)
        {
            int data = fileInputTwo.read();
            fileOutputStreamOne.write(data);
        }

        FileInputStream fileInputStreamThree = new FileInputStream(fileThree);

        while ( fileInputStreamThree.available()> 0)
        {
            int data = fileInputStreamThree.read();
            fileOutputStreamOne.write(data);
        }


        scan.close();
        fileOutputStreamOne.close();
        fileInputTwo.close();
        fileInputStreamThree.close();
    }
}

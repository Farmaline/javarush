package com.javarush.test.level18.lesson10.home04;

/* Объединение файлов
Считать с консоли 2 имени файла
В начало первого файла записать содержимое второго файла так, чтобы получилось объединение файлов
Закрыть потоки
Темповые файлы создавать нельзя, т.к. на сервере заблокирована возможность создания каких любо файлов
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException {

        BufferedReader scan = new BufferedReader(new InputStreamReader(System.in));
        String fileOne = scan.readLine();
        String fileTwo = scan.readLine();


        FileInputStream fileInputStreamOne = new FileInputStream(fileOne);
        FileInputStream fileInputStreamTwo = new FileInputStream(fileTwo);



        int count = 0;
        int bytSize = fileInputStreamOne.available() + fileInputStreamTwo.available();
        byte[] buffOne = new byte[bytSize];


        if (fileInputStreamTwo.available() > 0) {
            count = fileInputStreamTwo.read(buffOne);
        }
                int oneSize = fileInputStreamOne.available();
        if (fileInputStreamOne.available() > 0) {
            fileInputStreamOne.read(buffOne, count, oneSize);
        }

        File file = new File(fileOne);
        file.delete();
        file.createNewFile();

        FileOutputStream fileOutputStreamOne = new FileOutputStream(fileOne);
        fileOutputStreamOne.write(buffOne);


        fileInputStreamOne.close();
        fileInputStreamTwo.close();
        fileOutputStreamOne.close();


    }
}

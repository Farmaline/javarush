package com.javarush.test.level18.lesson10.bonus02;

/* Прайсы
CrUD для таблицы внутри файла
Считать с консоли имя файла для операций CrUD
Программа запускается со следующим набором параметров:
-c productName price quantity
Значения параметров:
где id - 8 символов
productName - название товара, 30 chars (60 bytes)
price - цена, 8 символов
quantity - количество, 4 символа
-c  - добавляет товар с заданными параметрами в конец файла, генерирует id самостоятельно, инкрементируя максимальный id

В файле данные хранятся в следующей последовательности (без разделяющих пробелов):
id productName price quantity
Данные дополнены пробелами до их длины

Пример:
19846   Шорты пляжные синие           159.00  12
198478  Шорты пляжные черные с рисунко173.00  17
19847983Куртка для сноубордистов, разм10173.991234
*/

import java.io.*;


public class Solution {
    public static void main(String[] args) throws Exception {


            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

            File file = new File(bufferedReader.readLine());

            int newId = getMaxId(file) + 1;

            String resultString = getFullStringName(args, newId);


            FileWriter writer = new FileWriter(file, true);

            writer.write("\r\n" + resultString);
            writer.flush();
            writer.close();
            bufferedReader.close();


    }

    static int getMaxId(File file) throws Exception {


        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));

        int maxId = 0;
        String lineText;

        while (bufferedReader.ready()) {
            lineText = bufferedReader.readLine();
            //System.out.println(lineText);
            if (!lineText.isEmpty()) {
                lineText = lineText.substring(0, 8);
                int id = Integer.parseInt(lineText.trim());
                //System.out.println(id);
                if (maxId < id) {
                    maxId = id;
                }
            }
        }
        bufferedReader.close();
        return maxId;
    }

    static String getFullStringName(String[] array, int mId) {


        String id = mId + "          ";
        id = id.substring(0, 8);

        if (array.length < 4) {
            return id + "                                          ";
        }

        String name = array[1] + "                               ";
        name = name.substring(0, 30);

        String prise = array[2] + "         ";
        prise = prise.substring(0, 8);

        String quantity = array[3] + "    ";
        quantity = quantity.substring(0, 4);

        return id + name + prise + quantity;
    }
}

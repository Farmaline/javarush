package com.javarush.test.level18.lesson10.bonus03;

/* Прайсы 2
CrUD для таблицы внутри файла
Считать с консоли имя файла для операций CrUD
Программа запускается с одним из следующих наборов параметров:
-u id productName price quantity
-d id
Значения параметров:
где id - 8 символов
productName - название товара, 30 chars (60 bytes)
price - цена, 8 символов
quantity - количество, 4 символа
-u  - обновляет данные товара с заданным id
-d  - производит физическое удаление товара с заданным id (все данные, которые относятся к переданному id)

В файле данные хранятся в следующей последовательности (без разделяющих пробелов):
id productName price quantity
Данные дополнены пробелами до их длины

Пример:
19846   Шорты пляжные синие           159.00  12
198478  Шорты пляжные черные с рисунко173.00  17
19847983Куртка для сноубордистов, разм10173.991234
*/

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Solution {
    static File file;
    static BufferedReader bufferedReader;
    static List<String> fileLines;

    public static void main(String[] args) throws IOException {
        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        file = new File(bufferedReader.readLine());
        fileLines = getFileLines(file);

        if (args.length == 5) {
            if ("-u".equals(args[0])) {
                System.out.println("Update method used!");
            updateLineU(args[1],args);
                writeInFile(file);
                bufferedReader.close();
            }
        }
        if (args.length == 2) {
            if ("-d".equals(args[0])) {
                deleteLineD(args[1]);
                writeInFile(file);
                bufferedReader.close();
            }
        }
    }

    static List<String> getFileLines(File file) throws IOException {
        List<String> resultList = new ArrayList<>();
        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
        while (bufferedReader.ready()) {
            resultList.add(bufferedReader.readLine());
        }
        return resultList;
    }

    static void deleteLineD(String id) {
        String deleteLine = "";

        for (String line : fileLines) {
            if (id.equals(line.substring(0, 8).trim())) {
                deleteLine = line;
                break;
            }
        }
        if (!deleteLine.isEmpty()) {
            fileLines.remove(deleteLine);
        }
    }

    static void updateLineU(String id, String[] array) {
        String lineUpdate = "";
        int temp = 0;
        for (String line : fileLines) {
            if ( !line.isEmpty() && id.equals(line.substring(0, 8).trim())) {

                String nId= id + "          ";
                nId = nId.substring(0, 8);

                String name = array[2] + "                               ";
                name = name.substring(0, 30);

                String prise = array[3] + "         ";
                prise = prise.substring(0, 8);

                String quantity = array[4] + "    ";
                quantity = quantity.substring(0, 4);
                String finalString = nId + name + prise + quantity;
                lineUpdate = finalString;
                break;
            }
            temp++;
        }
        if (!lineUpdate.isEmpty()) {
            fileLines.set(temp, lineUpdate);
        }
    }

    static void writeInFile(File file) throws IOException {
        boolean isFirst = true;
        FileWriter writer = new FileWriter(file);
        String result ="";
        for (String line : fileLines) {
            result += "\r\n" + line;
        }
        writer.write(result.trim());
        writer.flush();
        writer.close();
    }


}

package com.javarush.test.level18.lesson10.home02;

/* Пробелы
В метод main первым параметром приходит имя файла.
Вывести на экран частоту встречания пробела. Например, 10.45
1. Посчитать количество всех символов.
2. Посчитать количество пробелов.
3. Вывести на экран п2/п1*100, округлив до 2 знаков после запятой
Закрыть потоки
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException {
        double symbol = 0;
        double empty = 0;

        FileInputStream fileInputStream = new FileInputStream(args[0]);

        while (fileInputStream.available() > 0) {
            int data = fileInputStream.read();
            symbol++;
            if (data == 32) {
                empty++;
            }

        }
        double result = (empty / symbol) * 100;
        System.out.println(formatDouble(result, 2));


    }

    static double formatDouble(double d, int dz) {
        double dd = Math.pow(10, dz);
        return Math.round(d * dd) / dd;
    }

}

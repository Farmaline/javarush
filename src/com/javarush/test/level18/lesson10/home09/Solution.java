package com.javarush.test.level18.lesson10.home09;

/* Файлы и исключения
Читайте с консоли имена файлов
Если файла не существует (передано неправильное имя файла), то
перехватить исключение, вывести в консоль переданное неправильное имя файла и завершить работу программы
Не забудьте закрыть все потоки
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) {

        BufferedReader scan = new BufferedReader(new InputStreamReader(System.in));
        String fileName = null;
        while (true)
        {
            try {
                fileName = scan.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                FileReader file = new FileReader(fileName);
            } catch (FileNotFoundException e) {
                System.out.println(fileName);

                break;
            }

        }
        try {
            scan.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}

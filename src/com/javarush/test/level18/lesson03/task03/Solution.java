package com.javarush.test.level18.lesson03.task03;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.*;

/* Самые частые байты
Ввести с консоли имя файла
Найти байты, которые чаше всех встречаются в файле
Вывести их на экран через пробел.
Закрыть поток ввода-вывода
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        List<Integer> resultArray = new ArrayList<Integer>();
        List<Integer> sorttArray = new ArrayList<Integer>();
        Set<Integer> chekDataInt = new HashSet<Integer>();

        BufferedReader scan = new BufferedReader(new InputStreamReader(System.in));

        FileInputStream inputStream = new FileInputStream(scan.readLine());

        short byteSize = 256;
        byte count[] = new byte[byteSize];
        while (inputStream.available() > 0) {
            count[inputStream.read()]++;
        }
        //find max
        short max = 0;
        for (int i = 0; i < byteSize; i++) {
            if (count[i] >= max) {
                max = count[i];
            }
        }
        
        //print result
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < byteSize; i++) {
            if (count[i] == max) {
                result.append(i + " ");
            }
        }
        System.out.println(result.toString().trim());


       /* while (inputStream.available() > 0) {
            int data = inputStream.read();
            sorttArray.add(data);
            chekDataInt.add(data);

        }
        int temp;
        Iterator<Integer> iterator = chekDataInt.iterator();

        while (iterator.hasNext()) {
            temp = 0;
            int setInt = iterator.next();

            for (int arrayTemp : sorttArray) {
                if (setInt == arrayTemp) {
                    temp++;
                }
            }

            if (resultArray.isEmpty()) {
                resultArray.add(0, temp);
                resultArray.add(1, setInt);

            } else {
                if (temp > resultArray.get(0)) {
                    resultArray.clear();
                    resultArray.add(0, temp);
                    resultArray.add(1, setInt);
                    continue;

                }
                if (temp == resultArray.get(0)) {
                    resultArray.add(setInt);
                }

            }


        }

        String resultString = "";

        int size = resultArray.size();

        for (int i = 1; i < size; i++) {
            resultString += resultArray.get(i) + " ";
        }

        System.out.println(resultString.trim());*/


    }
}

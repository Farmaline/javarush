package com.javarush.test.level18.lesson03.task05;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.*;

/* Сортировка байт
Ввести с консоли имя файла
Считать все байты из файла.
Не учитывая повторений - отсортировать их по байт-коду в возрастающем порядке.
Вывести на экран
Закрыть поток ввода-вывода

Пример байт входного файла
44 83 44

Пример вывода
44 83
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        List<Integer> sorttArray = new ArrayList<Integer>();
        Set<Integer> chekDataInt = new HashSet<Integer>();

        BufferedReader scan = new BufferedReader(new InputStreamReader(System.in));

        FileInputStream inputStream = new FileInputStream(scan.readLine());

        while (inputStream.available() > 0) {
            int data = inputStream.read();

            chekDataInt.add(data);

        }


        Iterator<Integer> iterator = chekDataInt.iterator();

        while (iterator.hasNext()) {
            sorttArray.add(iterator.next());


        }


        int sortSize = sorttArray.size();

        for (int i = 0; i < sortSize; i++) {
            for (int k = 0; k < sortSize - i - 1; k++) {
                int tempA = sorttArray.get(k);
                int tempB = sorttArray.get(k + 1);

                if (tempA > tempB) {
                    sorttArray.set(k, tempB);
                    sorttArray.set(k + 1, tempA);
                }
            }
        }

        String resultString = "";

        int size = sorttArray.size();

        for (int i = 0; i < size; i++) {
            resultString += sorttArray.get(i) + " ";
        }

        System.out.println(resultString.trim());
    }
}
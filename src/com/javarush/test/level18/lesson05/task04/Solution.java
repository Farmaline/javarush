package com.javarush.test.level18.lesson05.task04;

/* Реверс файла
Считать с консоли 2 имени файла: файл1, файл2.
Записать в файл2 все байты из файл1, но в обратном порядке
Закрыть потоки ввода-вывода
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException{

        BufferedReader scan = new BufferedReader(new InputStreamReader(System.in));

        FileInputStream inputStream = new FileInputStream(scan.readLine());
        FileOutputStream outputStream = new FileOutputStream(scan.readLine());

        byte [] buff = new byte[inputStream.available()];

        if (inputStream.available()> 0)
        {
            int cout = inputStream.read(buff);

            for (int i = cout -1; i >= 0; i--)
            {
                outputStream.write(buff[i]);
            }
        }
        inputStream.close();
        outputStream.close();

    }
}

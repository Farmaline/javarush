package com.javarush.test.level18.lesson05.task03;

/* Разделение файла
Считать с консоли три имени файла: файл1, файл2, файл3.
Разделить файл1 по следующему критерию:
Первую половину байт записать в файл2, вторую половину байт записать в файл3.
Если в файл1 количество байт нечетное, то файл2 должен содержать бОльшую половину.
Закрыть потоки ввода-вывода
*/

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.SourceDataLine;
import java.awt.*;
import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException {

        BufferedReader scan = new BufferedReader(new InputStreamReader(System.in));

        FileInputStream inputStream = new FileInputStream(scan.readLine());

        FileOutputStream outputStreamFirst = new FileOutputStream(scan.readLine());
        FileOutputStream outputStreamSecond = new FileOutputStream(scan.readLine());


        byte[] buffer1 = new byte[inputStream.available()/2  + inputStream.available()%2];
        byte[] buffer2 = new byte[inputStream.available()/2];

        outputStreamFirst.write(buffer1, 0, inputStream.read(buffer1));
        outputStreamSecond.write(buffer2, 0, inputStream.read(buffer2));

        inputStream.close();
        outputStreamFirst.close();
        outputStreamSecond.close();

    }

   /* public static void main(String[] args) throws Exception {
        int seconds = 60*7;
        while (true) {

            if (seconds < 0) {
                byte[] buf = new byte[ 1 ];;
                AudioFormat af = new AudioFormat( (float )44100, 8, 1, true, false );
                SourceDataLine sdl = AudioSystem.getSourceDataLine( af );
                sdl = AudioSystem.getSourceDataLine( af );
                sdl.open( af );
                sdl.start();
                for( int i = 0; i < 1000 * (float )44100 / 1000; i++ ) {
                    double angle = i / ( (float )44100 / 440 ) * 2.0 * Math.PI;
                    buf[ 0 ] = (byte )( Math.sin( angle ) * 100 );
                    sdl.write( buf, 0, 1 );
                }
                sdl.drain();
                sdl.stop();
                sdl.close();
            } else {
                seconds--;
            }
            Thread.sleep(1000);

        }
    }*/

}

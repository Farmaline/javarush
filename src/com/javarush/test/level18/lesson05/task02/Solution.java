package com.javarush.test.level18.lesson05.task02;

/* Подсчет запятых
С консоли считать имя файла
Посчитать в файле количество символов ',', количество вывести на консоль
Закрыть потоки ввода-вывода

Подсказка: нужно сравнивать с ascii-кодом символа ','
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException{

        BufferedReader scan = new BufferedReader(new InputStreamReader(System.in));

        FileInputStream file = new FileInputStream(new File(scan.readLine()));
        int temp = 0;

        while (file.available()>0)
        {
            if(file.read() == 44)
            {
                temp ++;
            }
        }


        System.out.println(temp);

        scan.close();
        file.close();
    }
}

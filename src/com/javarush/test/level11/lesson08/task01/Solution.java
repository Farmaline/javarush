package com.javarush.test.level11.lesson08.task01;

/* Все скрыто
Скрыть все внутренние переменные класса Cat.
*/

public class Solution
{

    public void foo()
    {
        Cat tom = new Cat("Tom", 9, 100);
        //tom.age= -19;

        System.out.println(tom.getAge());
        tom.setAge(-19);
        System.out.println(tom.getAge());

    }

    public static void main(String[] args)
    {
        Solution s = new Solution();
        s.foo();


    }


}

package com.javarush.test.level11.lesson08.task01;

/**
 * Created by Administrator on 8/27/2014.
 */
public class Cat
{
    public String name;
    private int age;
    private int weight;

    public Cat()
    {
    }

    public Cat(String name, int age, int weight)
    {
        this.name = name;
        this.age = age;
        this.weight = weight;
    }

    public int getAge()
    {
        return age;
    }

    public void setAge(int age)
    {
            this.age = age;
    }
}

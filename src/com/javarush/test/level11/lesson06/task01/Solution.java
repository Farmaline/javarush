package com.javarush.test.level11.lesson06.task01;

/* Лошадь и пегас
Написать два класса: Horse (лошадь) и Pegas (пегас).
Унаследовать пегаса от лошади.
*/

import java.util.ArrayList;
import java.util.List;

public class Solution
{
    public static void main(String[] args)
    {
        Pegas nord = new Pegas();
        nord.begat();
        nord.begat2();


        List<Baby> babyList = new ArrayList<Baby>();
        babyList.add(new Pegas());
        babyList.add(new Horse());
        babyList.add(new Pegas());
        babyList.add(new Horse());
        babyList.add(new Pegas());
        babyList.add(new Horse());
        babyList.add(new Pegas());
        babyList.add(new Horse());

        for(Baby b : babyList){
            b.begat();
        }
    }

    public static class Horse implements Baby, Baby2
    {

        @Override
        public void begat()
        {
            System.out.println("begaiu");
        }

        @Override
        public void draka()
        {
            System.out.println("derus");
        }

        @Override
        public void drink()
        {
            System.out.println("drinking");
        }

        @Override
        public void begat2()
        {

        }

        @Override
        public void draka2()
        {

        }

        @Override
        public void drink2()
        {

        }
    }

    public static class Pegas extends Horse
    {

    }

    interface Baby{
        final double PI = 3.14;

        public void begat();
        public void draka();
        public void drink();

    }
    interface Baby2{
        final double PI = 3.14;

        public void begat2();
        public void draka2();
        public void drink2();

    }
}

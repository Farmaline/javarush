package com.javarush.test.level22.lesson09.task03;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

/* Составить цепочку слов
В методе main считайте с консоли имя файла, который содержит слова, разделенные пробелом.
В методе getLine используя StringBuilder расставить все слова в таком порядке,
чтобы последняя буква данного слова совпадала с первой буквой следующего не учитывая регистр.
Каждое слово должно участвовать 1 раз.
Метод getLine должен возвращать любой вариант.
Слова разделять пробелом.
В файле не обязательно будет много слов.

Пример тела входного файла:
Киев Нью-Йорк Амстердам Вена Мельбурн

Результат:
Амстердам Мельбурн Нью-Йорк Киев Вена
*/
public class Solution {
    public static void main(String[] args) throws IOException {
        //...
        String fileName;
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        //fileName = "C:\\k.txt";
        // bufferedReader.readLine();
        fileName = bufferedReader.readLine();
        BufferedReader fileReader = new BufferedReader(new FileReader(fileName));
        List<String> list = new ArrayList<>();
        boolean isFirst = true;
        if (fileReader.ready()) {
            for (String word : fileReader.readLine().split(" ")) {

                list.add(word);
            }
        }

        String[] array = new String[list.size()];
        list.toArray(array);

        StringBuilder result = getLine(array);
        System.out.println(result);
    }

    public static StringBuilder getLine(String... words) {

        //for empty file
        if (words.length == 0) {
            return new StringBuilder();
        }

        if (!words[0].startsWith("~")) { //first time here add [0] to ~

            int allInputCharactersCount = 0;
            for (int i = 0; i < words.length; i++) {
                allInputCharactersCount += words[i].length();
            }
            allInputCharactersCount += words.length - 1;

            for (int j = 0; j < words.length; j++) {
                System.out.println("++++++++++++++++++++++++++++++++++++++");
                System.out.println();
                System.out.println("++++++++++++++++++++++++++++++++++++++");
                //insert ~ and first word
                String inserted[] = insert(words, 0, "~" + words[j]);
                inserted = removeElement(inserted, j + 1);
                printArr(inserted);

                StringBuilder result = getLine(inserted);
                if (result.length() == allInputCharactersCount) {
                    //found
                    System.out.println("Holly FOUND " + result);
                    return result;
                } else {
                    System.out.println("NOT FOUND + " + result);
                }

            }

            //return getLine(insert(words, 0, "~"));

        } else {//do recursion logic
            ///System.out.println("Here we go");

            String first = words[0].substring(1);
            ///System.out.println("first = " + first);

            boolean isFound = false;
            List<StringBuilder> results = new ArrayList<>();
            for (int i = 1; i < words.length; i++) {

                String second = words[i];

                if (first.toLowerCase().charAt(first.length() - 1) == second.toLowerCase().charAt(0)) {
                    isFound = true;
                    System.out.println("Match: " + first + " " + second);
                    String inserted[] = insert(words, 0, words[0] + " " + second);
                    inserted = removeElement(inserted, 1);
                    inserted = removeElement(inserted, i);

                    System.out.println("Inserted After Match: ");
                    printArr(inserted);

                    results.add(getLine(inserted));


                }
            }

            if (!isFound) {
                ///System.out.println("STOPPED TO SEARCH returning " + first);
                return new StringBuilder(first);
            } else {

                int maxWinnerLength = -1;
                int indexWinner = -1;
                int sizeResults = results.size();
                for (int i = 0; i < sizeResults; i++) {
                    StringBuilder possibleWinner = results.get(i);
                    int length = possibleWinner.length();
                    if (length > maxWinnerLength) {
                        maxWinnerLength = length;
                        indexWinner = i;
                    }
                }

                System.out.println("Founded " + results.size() + " combinations. Max winner is " + results.get(indexWinner));
                return results.get(indexWinner);
            }

        }
        return new StringBuilder();
    }

    private static String[] removeElement(String[] arr, int pos) {
        String[] newArray = new String[arr.length - 1];

        for (int i = 0, k = 0; i < arr.length; i++) {
            if (i == pos) {
                k = 1;
                continue;
            }
            newArray[i - k] = arr[i];
        }
        return newArray;
    }

    private static String[] insert(String[] arr, int pos, String element) {
        String[] newArray = new String[arr.length + 1];


        for (int i = 0, k = 0; i < newArray.length; i++, k++) {
            if (i == pos) {
                newArray[i] = element;
                k--;
                continue;
            }
            newArray[i] = arr[k];

        }
        return newArray;

    }

    private static void printArr(String[] arr) {
        for (String s : arr) {
            System.out.print("[" + s + "] ");
        }
        System.out.println();
    }





}

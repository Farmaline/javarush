package com.javarush.test.level22.lesson09.task01;

import java.io.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/* Обращенные слова
В методе main с консоли считать имя файла, который содержит слова, разделенные пробелами.
Найти в тексте все пары слов, которые являются обращением друг друга. Добавить их в result.
Порядок слов first/second не влияет на тестирование.
Использовать StringBuilder.
Пример, "мор"-"ром", "трос"-"сорт"
*/
public class Solution {
    public static List<Pair> result = new LinkedList<>();

    public static void recurtion(List<String> arrayList) {
        if (arrayList.size() < 2) {
            return;
        }
        StringBuilder stringBuilder = new StringBuilder(arrayList.get(0));
        String reverse = stringBuilder.reverse().toString();
        int siZe = arrayList.size();
        for (int i = 1; i < siZe; i++) {
            if (reverse.equals(arrayList.get(i))) {
                result.add(new Pair(arrayList.get(0), arrayList.get(i)));
                arrayList.remove(i);
                arrayList.remove(0);

                recurtion(arrayList);
                return;
            }

        }
        arrayList.remove(0);
        recurtion(arrayList);

    }

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String fileName = reader.readLine();
        List<String> date = new ArrayList<>();
        try (FileReader fileReader = new FileReader(fileName)
        ) {
            reader = new BufferedReader(fileReader);

            while (reader.ready()) {
                for (String s : reader.readLine().split(" ")) {
                    date.add(s);

                }
            }
        } catch (FileNotFoundException e) {
            System.err.println("File "+ fileName + " not found." + e.getMessage());

        } catch (IOException e) {
            System.err.println("IOException catch!!! " + e.getMessage());

        }

        recurtion(date);

        for (Pair p : result)
            System.out.println(p);

    }

    public static class Pair {
        String first;
        String second;

        public Pair(String first, String second) {
            this.first = first;
            this.second = second;
        }

        @Override
        public String toString() {
            return first == null && second == null ? "" :
                    first == null && second != null ? second :
                            second == null && first != null ? first :
                                    first.compareTo(second) < 0 ? first + " " + second : second + " " + first;

        }
    }

}

package com.javarush.test.level22.lesson09.task02;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* Формируем Where
Сформируйте часть запроса WHERE используя StringBuilder.
Если значение null, то параметр не должен попадать в запрос.
Пример:
{"name", "Ivanov", "country", "Ukraine", "city", "Kiev", "age", null}
Результат:
"name = 'Ivanov' and country = 'Ukraine' and city = 'Kiev'"
*/
public class Solution {
    public static void main(String[] args) {
        Map<String, String> map = new HashMap<>();
        map.put("name", "Ivanov");
        map.put("country", "Ukraine");
        map.put(null, "Kiev");
        map.put("age", null);
        map.put("sex", "mail");


        for (Map.Entry<String, String> text : map.entrySet()) {
            System.out.println(text);
        }

        System.out.println(getCondition(map));

    }

    public static StringBuilder getCondition(Map<String, String> params) {
        StringBuilder stringBuilder = new StringBuilder();
        if (params == null || params.isEmpty()) {
            return stringBuilder;
        }


        boolean isFirstCondition = true;
        for (Map.Entry<String, String> map : params.entrySet()) {
            if (map.getKey() != null && map.getValue() != null) {

                    if (isFirstCondition) {
                        isFirstCondition = false;
                    } else {
                        stringBuilder.append(" and ");
                    }
                    stringBuilder.append(String.format("%s = \'%s\'", map.getKey(), map.getValue()));
                


            }
        }


        return stringBuilder;
    }
}

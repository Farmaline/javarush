package com.javarush.test.level22.lesson05.task02;

/* Между табуляциями
Метод getPartOfString должен возвращать подстроку между первой и второй табуляцией.
На некорректные данные бросить исключение TooShortStringException.
Класс TooShortStringException не менять.
*/
public class Solution {
    public static void main(String[] args) throws TooShortStringException {
        System.out.println(getPartOfString(null));
    }
    public static String getPartOfString(String string) throws TooShortStringException {
        try {
            int start = 0;
            int finish = 0;
            int indexTab = -1;
            String result = "";


            for (int i = 0; i < 2; i++) {

                indexTab = string.indexOf('\t', indexTab + 1);

                if (i == 0) start = indexTab;
                if (i == 1) finish = indexTab;
            }

            result = string.substring(start + 1, finish);


            return result;
        }catch (Exception e)
        {
            throw  new TooShortStringException();
        }
    }

    public static class TooShortStringException extends Exception {
    }
}

package com.javarush.test.level22.lesson05.task01;

/* Найти подстроку
Метод getPartOfString должен возвращать подстроку начиная с символа после 1-го пробела и до конца слова,
которое следует после 4-го пробела.
Пример: "JavaRush - лучший сервис обучения Java."
Результат: "- лучший сервис обучения"
На некорректные данные бросить исключение TooShortStringException (сделать исключением).
Сигнатуру метода getPartOfString не менять.
*/
public class Solution {



    public static String getPartOfString(String string) throws TooShortStringException {
        if (string == null)throw  new TooShortStringException();
        if (string.isEmpty()) throw  new  TooShortStringException();
        int start = 0;
        int finish = 0;
        int index =0;

        for (int i = 0; i < 5; i++)
        {
            index = string.indexOf(' ', index + 1);

            if (i == 0)
            {
                start = index;
            }
            if (i == 4)
            {
                finish = index;
            }
            if (index == -1)throw new TooShortStringException();
        }
        String result = string.substring(start + 1, finish);
        if (result.isEmpty()) throw new TooShortStringException();
        return result;
    }

    public static class TooShortStringException extends Exception {
    }
}

package com.javarush.test.level04.lesson13.task02;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/* Рисуем прямоугольник
Ввести с клавиатуры два числа m и n.
Используя цикл for вывести на экран прямоугольник размером m на n из восьмёрок.
Пример: m=2, n=4
8888
8888
*/

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        //Напишите тут ваш код
        BufferedReader scan = new BufferedReader(new InputStreamReader(System.in));
        int m1 = Integer.parseInt(scan.readLine());
        int n1 = Integer.parseInt(scan.readLine());

        for (int m = m1; m > 0; m--)

        {
            for (int n = n1; n > 0; n--)
            {
                System.out.print("8");

            }

            System.out.println();
        }


    }


}

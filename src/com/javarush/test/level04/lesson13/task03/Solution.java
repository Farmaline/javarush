package com.javarush.test.level04.lesson13.task03;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/* Рисуем треугольник
Используя цикл for вывести на экран прямоугольный треугольник из восьмёрок со сторонами 10 и 10.
Пример:
8
88
888
...
*/

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        //Напишите тут ваш код


        for (int i = 0, b = 1; i < 10; i++, b++)
        {


            for (int a = 0; a < b; a++)
            {
                System.out.print(8);
            }
            System.out.println();
        }
        System.out.println("NORD");

        String piramid = "";
        for (int n = 0; n < 10; n++)
        {
            //piramid += "8";
            piramid = piramid + "8";
            System.out.println(piramid);
        }

    }
}

package com.javarush.test.level04.lesson13.task04;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/* Рисуем линии
Используя цикл for вывести на экран:
-	горизонтальную линию из 10 восьмёрок
-	вертикальную линию из 10 восьмёрок.
*/

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        //Напишите тут ваш код
        for (int i=0, g=9; i<10; i++, g = -1)
        {

            for (int c=0; c<g; c++)
            {
                System.out.print(8);
            }
            System.out.println(8);
        }
        //System.out.println("NORD");

       /* for (int g = 0; g < 20; g++)
        {
            if (g<9)
            {
                System.out.print("8");
            }else{
                System.out.println("8");
            }
        }*/

    }
}

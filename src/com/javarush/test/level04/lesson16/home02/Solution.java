package com.javarush.test.level04.lesson16.home02;

/* Среднее такое среднее
Ввести с клавиатуры три числа, вывести на экран среднее из них. Т.е. не самое большое и не самое маленькое.
*/

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        //Напишите тут ваш код

        BufferedReader scan = new BufferedReader(new InputStreamReader(System.in));
        String a1 = scan.readLine();
        String b1 = scan.readLine();
        String c1 = scan.readLine();
        int a = Integer.parseInt(a1);
        int b = Integer.parseInt(b1);
        int c = Integer.parseInt(c1);

        if (a < b && a < c && b < c)
        {
            System.out.println(b);
        } else
        {
            if (b < a && b < c && c < a)
            {
                System.out.println(c);
            } else
            {
                if (c < a && c < b && a < b)
                {
                    System.out.println(a);
                }
            }
        }

    }
}

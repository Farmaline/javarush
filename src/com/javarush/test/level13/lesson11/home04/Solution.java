package com.javarush.test.level13.lesson11.home04;

/* Запись в файл
1. Прочесть с консоли имя файла.
2. Считывать строки с консоли, пока пользователь не введет строку "exit".
3. Вывести все строки в файл.
*/

import java.io.*;

public class Solution
{
    public static void main(String[] args)
    {
        FileWriter writeFile = null;
        BufferedReader scan = new BufferedReader(new InputStreamReader(System.in));
        try
        {
            File file = new File(scan.readLine());
            writeFile = new FileWriter(file);

            while (true)
            {
                String line = scan.readLine();
                if (line.equals("exit"))
                {
                    writeFile.write(line);
                    break;
                } else
                {
                    writeFile.write(line);
                    writeFile.write(System.getProperty("line.separator"));
                }
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        finally
        {
            if (writeFile != null)
            {
                try
                {
                    writeFile.close();
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            }


        }
    }
}

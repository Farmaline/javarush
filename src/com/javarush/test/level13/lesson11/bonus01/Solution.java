package com.javarush.test.level13.lesson11.bonus01;

/* Сортировка четных чисел из файла
1. Ввести имя файла с консоли.
2. Прочитать из него набор чисел.
3. Вывести на консоль только четные, отсортированные по возрастанию.
Пример ввода:
5
8
11
3
2
10
Пример вывода:
2
8
10
*/


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Objects;

public class Solution
{
    public static void main(String[] args)
    {
        // напишите тут ваш код
        BufferedReader scan = new BufferedReader(new InputStreamReader(System.in));

        ArrayList<Integer> arrayList = new ArrayList<Integer>();
        BufferedReader br = null;
        try
        {
            String sCurrentLine;
            br = new BufferedReader(new FileReader(scan.readLine()));

            while ((sCurrentLine = br.readLine()) != null)
            {
                Integer value = Integer.valueOf(sCurrentLine);
                if (value%2==0){
                    arrayList.add(value);
                }
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        finally
        {
            try
            {
                if (br != null) br.close();
            }
            catch (IOException ex)
            {
                ex.printStackTrace();
            }
        }

        Integer a[] = new Integer[arrayList.size()];
        arrayList.toArray(a);

        Arrays.sort(a);

        for(Integer b : a){
            System.out.println(b);
        }


    }
}

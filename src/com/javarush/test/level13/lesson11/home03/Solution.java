package com.javarush.test.level13.lesson11.home03;

/* Чтение файла
1. Считать с консоли имя файла.
2. Вывести в консоль(на экран) содержимое файла.
3. Не забыть закрыть файл и поток.
*/

import java.io.*;

public class Solution
{
    public static void main(String[] args)
    {

        //add your code here
        BufferedReader scan = new BufferedReader(new InputStreamReader(System.in));
        InputStream inputStream = null;
        try
        {
            File file = new File(scan.readLine());


            //System.out.println(file.exists() + " " + file.isDirectory());
            inputStream = new FileInputStream(file);


            while (inputStream.available() > 0)
            {
                int line = inputStream.read();
                System.out.print((char) line);
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        finally
        {
            try
            {
                inputStream.close();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }


    }
}

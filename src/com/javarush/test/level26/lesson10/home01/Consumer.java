package com.javarush.test.level26.lesson10.home01;

import java.util.concurrent.BlockingQueue;

/**
 * Created by Administrator on 04.07.2015.
 */
public class Consumer extends Producer{


    public Consumer(BlockingQueue blockQ) {
        super(blockQ);
    }

    @Override
    public void run() {
        try {
            while (true)
                if (!this.queue.isEmpty())System.out.println(this.queue.take());

        } catch (InterruptedException e) {
            System.out.println(String.format("[%s] thread was terminated", Thread.currentThread().getName()));
        }
    }
}

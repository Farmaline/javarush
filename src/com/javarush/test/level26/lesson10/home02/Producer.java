package com.javarush.test.level26.lesson10.home02;

import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Administrator on 04.07.2015.
 */
public class Producer extends Consumer {
    public Producer(ConcurrentHashMap<String, String> map) {
        super(map);
    }

    @Override
    public void run() {
        Thread currentThread = Thread.currentThread();

        while (!currentThread.isInterrupted())
        {
            try {
                int i = 0;
                while (true) {
                    this.map.put(String.valueOf(i++),String.format("Some text for %s", i));
                    Thread.sleep(500);
                }
            } catch (InterruptedException e) {
                System.out.println(String.format("[%s] thread was terminated", Thread.currentThread().getName()));
            }
        }
    }
}

package com.javarush.test.level26.lesson02.task01;

import java.util.Arrays;
import java.util.Comparator;

/* Почитать в инете про медиану выборки
Реализовать логику метода sort, который должен сортировать данные в массиве по удаленности от его медианы
Вернуть отсортированный массив от минимального расстояния до максимального
Если удаленность одинаковая у нескольких чисел, то выводить их в порядке возрастания
*/
public class Solution {
    public static Integer[] sort(Integer[] array) {
        //implement logic here
        final double mediana;

        if (array != null) {
            Arrays.sort(array);
            int sizeArray = array.length;
            if (sizeArray % 2 == 1) {
                mediana = array[(sizeArray - 1) / 2];
            } else {
                mediana = ( (((double) array[sizeArray / 2]) + ((double)array[(sizeArray / 2) - 1])) / 2);
            }

            Comparator<Integer> integerComparator = new Comparator<Integer>() {
                @Override
                public int compare(Integer o1, Integer o2) {
                    int result;
                    double one = Math.abs(o1 - mediana);
                    double two = Math.abs(o2 - mediana);
                    result = (int) (one - two);
                    if (result == 0) {
                        result = o1 - o2;
                    }
                    return result;
                }
            };

            Arrays.sort(array,integerComparator);
        }
        return array;
    }
}

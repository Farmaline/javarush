package com.javarush.test.level26.lesson15.big01;

import com.javarush.test.level26.lesson15.big01.command.CommandExecutor;
import com.javarush.test.level26.lesson15.big01.exception.InterruptOperationException;

import java.util.Locale;

/**
 * Created by Administrator on 10.07.2015.
 */
public class CashMachine {
    public static void main(String[] args) {
        Locale.setDefault(Locale.ENGLISH);
        Operation operation = Operation.INFO;
        try {


            CommandExecutor.execute(Operation.LOGIN);
            do {
                operation = ConsoleHelper.askOperation();
                CommandExecutor.execute(operation);
            } while (!(operation.equals(Operation.EXIT)));
        }catch (InterruptOperationException e){
            try {
                CommandExecutor.execute(Operation.EXIT);
            } catch (InterruptOperationException e1) {
                e1.printStackTrace();
            }
        }

    }

    /*public static void main(String[] args) throws NotEnoughMoneyException {
        CurrencyManipulator currencyManipulator = CurrencyManipulatorFactory.getManipulatorByCurrencyCode("Usd");
        currencyManipulator.addAmount(200,2);
        currencyManipulator.addAmount(100,5);
        currencyManipulator.addAmount(50,1);
        currencyManipulator.addAmount(25,4);
        System.out.println(currencyManipulator.withdrawAmount(625));
    }*/
    /*public static void main(String[] args) throws NotEnoughMoneyException {
        CurrencyManipulator currencyManipulator = CurrencyManipulatorFactory.getManipulatorByCurrencyCode("Usd");
        currencyManipulator.addAmount(200,3);
        currencyManipulator.addAmount(100,0);
        currencyManipulator.addAmount(50,0);
        currencyManipulator.addAmount(25,1);
        System.out.println(currencyManipulator.withdrawAmount(590));
    }*/
   /* public static void main(String[] args) throws InterruptOperationException {
        CurrencyManipulator currencyManipulator = CurrencyManipulatorFactory.getManipulatorByCurrencyCode("USD");
        currencyManipulator.addAmount(200,2);
        currencyManipulator.addAmount(100,5);
        currencyManipulator.addAmount(50,1);
        currencyManipulator.addAmount(25,4);
        System.out.println(currencyManipulator.getTotalAmount());
        CommandExecutor.execute(Operation.WITHDRAW);
    }*/
}

package com.javarush.test.level26.lesson15.big01;

import com.javarush.test.level26.lesson15.big01.exception.InterruptOperationException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Administrator on 10.07.2015.
 */
public class ConsoleHelper {
    public static void writeMessage(String message) {
        System.out.println(message);
    }

    static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public static String readString() throws InterruptOperationException {
        String read ="";
        String resultExit="";
        try{
            read = reader.readLine();
            resultExit = read.toUpperCase();
            if(resultExit.equals("EXIT")){
                throw new InterruptOperationException();}
        }catch (IOException e){
            e.printStackTrace();
        }

        return read;
    }

    public static String askCurrencyCode() throws InterruptOperationException {
        String valute = "";

        while (true) {
            writeMessage("введите валюту");
            valute = readString();
            if (valute.length() == 3) {
                valute = valute.toUpperCase();
                break;
            } else {
                writeMessage("не верная операция повторите ввод");
            }
        }


        return valute;
    }

    public static String[] getValidTwoDigits(String currencyCode) throws InterruptOperationException {
        String[] currency;
        String splitLine;
        writeMessage("введите номиналы валюты " + currencyCode);

        splitLine = readString();
        int a;
        int b;
        while (true) {
            currency = splitLine.split(" ");

            try {
                a = Integer.parseInt(currency[0]);
                b = Integer.parseInt(currency[1]);
            } catch (Exception e) {
                writeMessage("ошибка заполнения");
                continue;
            }
            if (a <= 0 || b <= 0 || currency.length > 2) {
                writeMessage("шибка заполнения");
                continue;
            }
            break;
        }
        return currency;
    }

    public static Operation askOperation() throws InterruptOperationException {
        while (true)
        {
            String line = readString();
            if (checkWithRegExp(line))
                return Operation.getAllowableOperationByOrdinal(Integer.parseInt(line));
            else
                writeMessage("не правильный ввод");
        }

    }

    public static boolean checkWithRegExp(String Name)
    {
        Pattern p = Pattern.compile("^[1-4]$");
        Matcher m = p.matcher(Name);
        return m.matches();
    }
}

package com.javarush.test.level26.lesson15.big01;

import com.javarush.test.level26.lesson15.big01.exception.NotEnoughMoneyException;

import java.util.*;

/**
 * Created by Administrator on 10.07.2015.
 */
public class CurrencyManipulator {

    private String currencyCode;

    private Map<Integer, Integer> denominations = new HashMap<>();

    public String getCurrencyCode() {
        return currencyCode;

    }

    public boolean isAmountAvailable(int expectedAmount) {
        boolean answer = false;
        if (expectedAmount <= getTotalAmount()) {
            answer = true;
        }
        return answer;
    }

    public boolean hasMoney() {
        boolean result = false;
        int money = 0;
        for (Map.Entry<Integer, Integer> map : denominations.entrySet()) {
            money += map.getKey() * map.getValue();
        }
        if (money > 0) {
            result = true;
        }
        return result;
    }

    public CurrencyManipulator(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public void addAmount(int denomination, int count) {
        if (denominations.containsKey(denomination))
            denominations.put(denomination, denominations.get(denomination) + count);
        else
            denominations.put(denomination, count);
    }

    public int getTotalAmount() {
        int resultTotalAmount = 0;

        for (Map.Entry<Integer, Integer> elem : denominations.entrySet()) {
            resultTotalAmount += elem.getKey() * elem.getValue();
        }


        return resultTotalAmount;
    }

    public Map<Integer, Integer> withdrawAmount(int expectedAmount) throws NotEnoughMoneyException {
        if (!this.isAmountAvailable(expectedAmount)) {
            ConsoleHelper.writeMessage("привышен лимит");
            return null;
        }
        Map<Integer, Integer> sortResult = new HashMap<>();
        String stringRes = "";
        Map<Integer, Integer> result = new TreeMap(new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
                Integer i1 = (Integer) o1;
                Integer i2 = (Integer) o2;
                return i2.compareTo(i1);
            }
        });
        result.putAll(denominations);

        if (expectedAmount <= 0)
        {
            throw new NotEnoughMoneyException();
        }
        try{

        int balance = expectedAmount;
        for (Map.Entry<Integer, Integer> e : result.entrySet()) {
            int nominal = e.getKey();
            int count = balance / nominal;
            if (count > e.getValue()) {
                count = e.getValue();
            }
            if (count!=0) {
                stringRes += '\t' + String.valueOf(nominal) + " - " + String.valueOf(count) + '\n';
                sortResult.put(e.getKey(), count);
                balance -= nominal * count;
            }
        }

            if (balance != 0) {
                throw new NotEnoughMoneyException();
            } else {
                for (Map.Entry<Integer, Integer> map : sortResult.entrySet()) {
                    denominations.put(map.getKey(), denominations.get(map.getKey()) - map.getValue());

                }
                ConsoleHelper.writeMessage('\t'+stringRes.trim());
            }
        }
        catch (ConcurrentModificationException ignore)
        {
        }

        return sortResult;
    }
}

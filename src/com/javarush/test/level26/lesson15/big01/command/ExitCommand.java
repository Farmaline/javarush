package com.javarush.test.level26.lesson15.big01.command;

import com.javarush.test.level26.lesson15.big01.ConsoleHelper;
import com.javarush.test.level26.lesson15.big01.exception.InterruptOperationException;

/**
 * Created by Administrator on 07.08.2015.
 */
class ExitCommand implements Command {
    @Override
    public void execute() throws InterruptOperationException {
        ConsoleHelper.writeMessage("Действительно ли хотите выйти? <y,n> :");
        String answer = ConsoleHelper.readString();
        if (answer.toUpperCase().equals("Y")){
            ConsoleHelper.writeMessage("До свидания!");
        }

    }
}

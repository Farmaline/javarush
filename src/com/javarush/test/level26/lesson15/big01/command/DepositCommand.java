package com.javarush.test.level26.lesson15.big01.command;

import com.javarush.test.level26.lesson15.big01.ConsoleHelper;
import com.javarush.test.level26.lesson15.big01.CurrencyManipulator;
import com.javarush.test.level26.lesson15.big01.CurrencyManipulatorFactory;
import com.javarush.test.level26.lesson15.big01.exception.InterruptOperationException;


/**
 * Created by Administrator on 07.08.2015.
 */
 class DepositCommand implements Command {
    @Override
    public void execute() throws InterruptOperationException {
        CurrencyManipulator manipulator = CurrencyManipulatorFactory.getManipulatorByCurrencyCode(ConsoleHelper.askCurrencyCode());
        try {
            String[] count = ConsoleHelper.getValidTwoDigits(manipulator.getCurrencyCode());
            manipulator.addAmount(Integer.parseInt(count[0]), Integer.parseInt(count[1]));
        }catch (NumberFormatException e){
            ConsoleHelper.writeMessage("ошибка ввода");
        }

    }
}

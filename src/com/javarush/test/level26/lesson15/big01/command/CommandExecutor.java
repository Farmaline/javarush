package com.javarush.test.level26.lesson15.big01.command;

import com.javarush.test.level26.lesson15.big01.Operation;
import com.javarush.test.level26.lesson15.big01.exception.InterruptOperationException;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 07.08.2015.
 */
public final class CommandExecutor {
    static Map<Operation, Command> operationCommandMap = new HashMap<>();
    static{
        CommandExecutor.operationCommandMap.put(Operation.DEPOSIT, new DepositCommand());
        CommandExecutor.operationCommandMap.put(Operation.WITHDRAW, new WithdrawCommand());
        CommandExecutor.operationCommandMap.put(Operation.EXIT, new ExitCommand());
        CommandExecutor.operationCommandMap.put(Operation.INFO, new InfoCommand());
        CommandExecutor.operationCommandMap.put(Operation.LOGIN, new LoginCommand());
    }
    private CommandExecutor() {}

    public static final void execute(Operation operation) throws  InterruptOperationException {
        operationCommandMap.get(operation).execute();
    }
}


package com.javarush.test.level26.lesson15.big01.command;

import com.javarush.test.level26.lesson15.big01.ConsoleHelper;
import com.javarush.test.level26.lesson15.big01.exception.InterruptOperationException;

import java.util.ResourceBundle;

/**
 * Created by Administrator on 11.08.2015.
 */
class LoginCommand implements Command {
    private ResourceBundle validCreditCards = ResourceBundle.getBundle("com.javarush.test.level26.lesson15.big01.resources." + "verifiedCards");
    @Override
    public void execute() throws InterruptOperationException {

        //ConsoleHelper.writeMessage(res.getString("before"));
        while (true)
        {
            //ConsoleHelper.writeMessage(res.getString("specify.data"));
            String s1 = ConsoleHelper.readString();
            String s2 = ConsoleHelper.readString();
            if (validCreditCards.containsKey(s1))
            {
                if (validCreditCards.getString(s1).equals(s2))
                    ConsoleHelper.writeMessage("авторизация прошла успешно");
                    //ConsoleHelper.writeMessage(String.format(res.getString("success.format"), s1));
                else
                {
                    ConsoleHelper.writeMessage("не правильный формат");
                    //ConsoleHelper.writeMessage(String.format(res.getString("not.verified.format"), s1));
                    //ConsoleHelper.writeMessage(res.getString("try.again.or.exit"));
                    continue;
                }
            }
            else
            {
                ConsoleHelper.writeMessage("не правильный формат");
                //ConsoleHelper.writeMessage(String.format(res.getString("not.verified.format"), s1));
                //ConsoleHelper.writeMessage(res.getString("try.again.with.details"));
                continue;
            }

            break;
        }

        /*
        String cardRead = "";
        String pinRead = "";
        while (true) {
            ConsoleHelper.writeMessage("введите номер карты и пин");
            cardRead = ConsoleHelper.readString();
            pinRead = ConsoleHelper.readString();
            *//*if (!cardRead.matches("^[0-9]{12}$") || !pinRead.matches("^[0-9]{4}$")) {
                ConsoleHelper.writeMessage("данные не валидны");
                continue;
            }*//*
            if (validCreditCards.containsKey(cardRead) && validCreditCards.getString(cardRead).equals(pinRead)) {
                ConsoleHelper.writeMessage("авторизация прошла успешно");
                break;
            }
        }*/

        /*while (true) {
            String s1 = ConsoleHelper.readString();
            String s2 = ConsoleHelper.readString();
            if (s1.matches("^[0-9]{12}") && s2.matches("^[0-9]{4}"))
            {
                if (s1.equals(card) && s2.equals(pin))
                    ConsoleHelper.writeMessage("даныне валидны");
                else
                {
                    ConsoleHelper.writeMessage("не правильный формат");
                    continue;
                }
            }
            else
            {
                ConsoleHelper.writeMessage("неверынй логин");
                continue;
            }

            break;
        }*/
    }
}


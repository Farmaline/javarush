package com.javarush.test.level26.lesson15.big01.command;

import com.javarush.test.level26.lesson15.big01.exception.InterruptOperationException;


/**
 * Created by Administrator on 07.08.2015.
 */
interface Command {
    void execute() throws InterruptOperationException;
}

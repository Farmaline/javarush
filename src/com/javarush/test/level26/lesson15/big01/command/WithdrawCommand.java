package com.javarush.test.level26.lesson15.big01.command;

import com.javarush.test.level26.lesson15.big01.ConsoleHelper;
import com.javarush.test.level26.lesson15.big01.CurrencyManipulator;
import com.javarush.test.level26.lesson15.big01.CurrencyManipulatorFactory;
import com.javarush.test.level26.lesson15.big01.exception.InterruptOperationException;
import com.javarush.test.level26.lesson15.big01.exception.NotEnoughMoneyException;

/**
 * Created by Administrator on 07.08.2015.
 */
 class WithdrawCommand implements Command{
    @Override
    public void execute() throws InterruptOperationException {
        String currencyCode = ConsoleHelper.askCurrencyCode();
        CurrencyManipulator manipulator = CurrencyManipulatorFactory.getManipulatorByCurrencyCode(currencyCode);
        int sum;
        while (true) {
            ConsoleHelper.writeMessage("введите сумму");
            try {
                sum = Integer.parseInt(ConsoleHelper.readString());
                if (!manipulator.isAmountAvailable(sum)){
                    ConsoleHelper.writeMessage("привышен лимит");
                    continue;
                }
            }catch (NumberFormatException e){
                ConsoleHelper.writeMessage("повторите попытку");
                continue;
            }
            if (!manipulator.isAmountAvailable(sum)) {
                continue;
            }
            try {
                manipulator.withdrawAmount(sum);
                ConsoleHelper.writeMessage("удачная транзакция");
                break;
            } catch (NotEnoughMoneyException e) {
                e.printStackTrace();
                ConsoleHelper.writeMessage("операция неудачна");
                continue;
            }


        }

    }
}

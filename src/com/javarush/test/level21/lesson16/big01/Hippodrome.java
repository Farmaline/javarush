package com.javarush.test.level21.lesson16.big01;

import java.util.ArrayList;

/**
 * Created by Administrator on 8/31/2014.
 */
public class Hippodrome
{
    static ArrayList<Horse> horses = new ArrayList<Horse>();
    public static Hippodrome game = null;


    public ArrayList<Horse> getHorses()
    {
        return horses;
    }
    public static void main(String[] args)
    {
          Hippodrome.game = new Hippodrome();
        game.horses.add(new Horse("Kobila",3,0));
        game.horses.add(new Horse("Shama",3,0));
        game.horses.add(new Horse("Paha",3,0));


        try
        {
            game.run();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

       game.printWinner();



    }
   public void run() throws Exception
    {
        for (int i = 0; i < 100; i++) {

            move();
            print();
            Thread.sleep(500);

        }
    }
    public void move(){

        for(Horse temp : horses){
            temp.move();
        }
    }
    public void print(){

        for(Horse temp : horses){
            temp.print();
        }
        System.out.println();
        System.out.println();
    }

    public Horse getWinner()
    {
        Horse horse = new Horse("dfdf",0,0);
        for(Horse h : getHorses()){
            if(h.getDistance()>horse.getDistance()){
                horse=h;
            }
        }
        return horse;

    }

    public void printWinner()
    {
        System.out.println("Winner is " +getWinner().getName() + "!");
    }


}

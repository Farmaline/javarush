package com.javarush.test.level21.lesson10.task03;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/* Найти и исправить ошибку
Найти и исправить ошибку
*/
public class Solution {

    public Long number;
    public String snumber;

    public Solution(int aLong) {
        number = (long) aLong;
    }

    public static void main(String[] args) {

        /*for (int i = 0; i < 1; i++)
            try {
                new Solution().readFile("ggg");


            } catch (Throwable throwable) {
                System.out.println(i + " " + throwable.getClass().getSimpleName());
            }*/




        List<Solution> list = new ArrayList<>();

        list.add(new Solution(2));
        list.add(new Solution(1));
        list.add(new Solution(5));
        list.add(new Solution(3));

        System.out.println(list);
        Collections.sort(list,new Comparator<Solution>() {
            @Override
            public int compare(Solution o1, Solution o2) {
                Long n1 = o1.number;
                Long n2 = o2.number;
                return ((Long)(n2-n1)).intValue();
                //return n1.compareTo(n2);
            }
        });
        System.out.println(list);





    }

    @Override
    public String toString() {
        return "Solution{" +
                "number=" + number +
                '}';
    }

    public void readFile(String path) throws Throwable {
        try (BufferedReader br = new BufferedReader(new FileReader(path))) {
            System.out.println(br.readLine());
            finalize();
            dispose();

        } catch (FileNotFoundException ignored) {
            dispose();
        } catch (IOException ignored) {
            dispose();
        } finally {
            System.out.println("finally");

        }
    }

    public void dispose() {
        //pretend to call some method that throws an exception
        throw new RuntimeException("no matter");
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        System.out.println("inside finalize - before throwing");
        dispose();   //исключения игнорируются в finalize
        System.out.println("inside finalize - after throwing");
    }



}

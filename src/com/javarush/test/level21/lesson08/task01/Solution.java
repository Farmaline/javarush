package com.javarush.test.level21.lesson08.task01;

import java.util.LinkedHashMap;
import java.util.Map;

/* Глубокое клонирование карты
Клонируйтие объект класса Solution используя глубокое клонирование.
Данные в карте users также должны клонироваться.
*/
public class Solution implements Cloneable {
    @Override
    protected Solution clone() throws CloneNotSupportedException {
        Solution cloneable =  new Solution();
        for (Map.Entry<String, User> map : this.users.entrySet())
        {
            cloneable.users.put(map.getKey(), new User(map.getValue().age, map.getValue().name));
        }

        return cloneable;
    }

    public static void main(String[] args) {
        Solution solution = new Solution();
        solution.users.put("Hubert", new User(172, "Hubert"));
        solution.users.put("Zapp", new User(41, "Zapp"));
        Solution clone = null;
        try {
            clone = solution.clone();
            System.out.println(solution);
            System.out.println(clone);

            System.out.println(solution.users);
           /* for (Map.Entry<String, User> map : solution.users.entrySet() )
            {
                System.out.println( map.getKey() + " " + map.getValue().age);
            }*/
            System.out.println(clone.users);
           /* for (Map.Entry<String, User> map : clone.users.entrySet() )
            {
                System.out.println( map.getKey() + " " + map.getValue().age);
            }*/

        } catch (CloneNotSupportedException e) {
            e.printStackTrace(System.err);
        }
    }

    protected Map<String, User> users = new LinkedHashMap();

    public static class User {
        int age;
        String name;

        public User(int age, String name) {
            this.age = age;
            this.name = name;
        }


    }
}

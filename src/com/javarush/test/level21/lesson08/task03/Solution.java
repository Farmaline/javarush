package com.javarush.test.level21.lesson08.task03;

/* Запретить клонирование
Запретите клонировать класс B
Разрешите клонировать класс C
*/
public class Solution {

    public static void main(String[] args) {
        A a = new A(1, 2);
        try {
            A aClonned = (A) a.clone();
            System.out.println("Clonned A=[i=" + aClonned.getI() + ", j=" + aClonned.getJ()+"]");
        } catch (CloneNotSupportedException e) {
            System.out.println("ERROR: class A did not clone " + e.getMessage());
            e.printStackTrace();
        }


        B b = new B(3, 4, "nord");
        try {
            B bClonned = (B) b.clone();
            System.out.println("Clonned B=[i=" + bClonned.getI() + ", j=" + bClonned.getJ()+"]");
        } catch (Exception e) {
            System.out.println("ERROR: class B did not clone " + e.getMessage());
            e.printStackTrace();
        }


        C c = new C(5, 6, "kain");
        try {
            C cClonned = (C) c.clone();
            System.out.println("Clonned C=[i=" + cClonned.getI() + ", j=" + cClonned.getJ()+", name="+ cClonned.getName()+"]");
        } catch (CloneNotSupportedException e) {
            System.out.println("ERROR: class C did not clone " + e.getMessage());
            e.printStackTrace();
        }


    }

    public static class A {
        private int i;
        private int j;

        public A(int i, int j) {
            this.i = i;
            this.j = j;
        }

        public int getI() {
            return i;
        }

        public int getJ() {
            return j;
        }

        @Override
        public Object clone() throws CloneNotSupportedException {
            return new A(i,j);
        }
    }

    public static class B extends A {
        private String name;

        public B(int i, int j, String name) {
            super(i, j);
            this.name = name;
        }

        public String getName() {
            return name;
        }

        @Override
        public Object clone() throws CloneNotSupportedException {
            throw new CloneNotSupportedException();
        }
    }

    public static class C extends B{
        public C(int i, int j, String name) {
            super(i, j, name);
        }

        @Override
        public C clone() throws CloneNotSupportedException {
            return new C(getI(),getJ(),getName());
        }
    }
}

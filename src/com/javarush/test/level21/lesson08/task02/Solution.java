package com.javarush.test.level21.lesson08.task02;

/* Клонирование
Класс Plant не должен реализовывать интерфейс Cloneable
Реализуйте механизм глубокого клонирования для Tree.
*/
public class Solution {
    public static void main(String[] args) {
       // Tree tree = new Tree("willow", new String[]{"s1", "s2", "s3", "s4"});
        Tree tree = new Tree("willow",null);
        Tree clone = null;
        try {
            clone =  tree.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }

        System.out.println(tree);
        System.out.println(clone);

        System.out.println(tree.branches);
       /* for (String text : tree.branches)
        {
            System.out.println(text);
        }*/
        System.out.println(clone.branches);
        /*for (String text : clone.branches)
        {
            System.out.println(text);
        }*/
    }



    public static class Plant{
        private String name;

        public Plant(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }

    public static class Tree extends Plant implements Cloneable{
        private String[] branches;

        @Override
        public Tree clone() throws CloneNotSupportedException {
            Tree clone = null;
            if(getBranches() != null) {
                clone = new Tree(this.getName(), getBranches().clone());
            }
            else
            {
                clone = new Tree(this.getName(),getBranches());
            }

            return clone;

        }


        public Tree(String name, String[] branches) {
            super(name);
            this.branches = branches;
        }

        public String[] getBranches() {
            return branches;
        }
    }
}

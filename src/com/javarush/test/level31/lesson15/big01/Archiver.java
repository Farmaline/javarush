package com.javarush.test.level31.lesson15.big01;


import com.javarush.test.level31.lesson15.big01.command.ExitCommand;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by Administrator on 07.12.2015.
 */


public class Archiver {
    public static void main(String[] args) {

        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Enter zip path");
            Path zipPath = Paths.get(bufferedReader.readLine());
            ZipFileManager zipFileManager = new ZipFileManager(zipPath);
            System.out.println("Enter file to archieve");
            Path filePath = Paths.get(bufferedReader.readLine());
            zipFileManager.createZip(filePath);
            ExitCommand exitCommand = new ExitCommand();
            exitCommand.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }



    }
}

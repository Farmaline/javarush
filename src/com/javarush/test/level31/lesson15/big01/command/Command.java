package com.javarush.test.level31.lesson15.big01.command;

/**
 * Created by Administrator on 07.12.2015.
 */
public interface Command {
   public void execute() throws Exception;

}

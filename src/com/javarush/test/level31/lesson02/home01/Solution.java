package com.javarush.test.level31.lesson02.home01;


import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/* Проход по дереву файлов
1. На вход метода main подаются два параметра.
Первый - path - путь к директории, второй - resultFileAbsolutePath - имя файла, который будет содержать результат.
2. Для каждого файла в директории path и в ее всех вложенных поддиректориях выполнить следующее:
2.1. Если у файла длина в байтах больше 50, то удалить его.
2.2. Если у файла длина в байтах НЕ больше 50, то для всех таких файлов:
2.2.1. отсортировать их по имени файла в возрастающем порядке, путь не учитывать при сортировке
2.2.2. переименовать resultFileAbsolutePath в 'allFilesContent.txt'
2.2.3. в allFilesContent.txt последовательно записать содержимое всех файлов из п. 2.2.1. Тела файлов разделять "\n"
2.3. Удалить директории без файлов (пустые).
Все файлы имеют расширение txt.
*/
public class Solution {

    private static File resultFileAbsolutePath;
    private static List<File> sortedFiles = new ArrayList<>();

    private static void recursiveDelete(File file) {
        if (file.isFile()) {
            //System.out.println(file.getAbsolutePath());

            if (!file.getAbsolutePath().equals(resultFileAbsolutePath.getAbsolutePath()) && !file.getName().equals("allFilesContent.txt")) {
                //System.out.println("check and delete file " + file.getAbsolutePath());

                if (file.length() > 50) {
                    file.delete();
                    //System.out.println("was file deleted filePath = " + file.getAbsolutePath());
                } else {
                    sortedFiles.add(file);
                }
            }
        } else {
            for (File f : file.listFiles()) {
                recursiveDelete(f);
                if (f.exists() && f.isDirectory()) {
                    if (f.listFiles().length == 0) {
                        f.delete();
                    }
                }
            }
        }
    }

    public static void main(String[] args) {

        File path = new File(args[0]);
        resultFileAbsolutePath = new File(args[1]);

        if (path.isDirectory()) {
            recursiveDelete(path);
            Collections.sort(sortedFiles, new Comparator<File>() {
                @Override
                public int compare(File o1, File o2) {
                    String one = o1.getName();
                    String two = o2.getName();
                    return one.compareTo(two);
                }
            });

            try {
                FileOutputStream outputStream = new FileOutputStream(resultFileAbsolutePath);
                int N = sortedFiles.size();
                for (int i = 0; i < N; i++) {
                    File sortedFile = sortedFiles.get(i);
                    FileInputStream inputStreamReader = new FileInputStream(sortedFile);

                    byte dd [] = new byte[inputStreamReader.available()];
                    inputStreamReader.read(dd);
                    outputStream.write(dd);


                    if (i < N - 1) {
                        outputStream.write(System.lineSeparator().getBytes());
                    }
                    inputStreamReader.close();

                }
                outputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            //Rename file
            File renamedFile = new File(resultFileAbsolutePath.getParent() + "/allFilesContent.txt");
            if (renamedFile.exists()){
                renamedFile.delete();
            }
            resultFileAbsolutePath.renameTo(renamedFile);

        }
    }
}

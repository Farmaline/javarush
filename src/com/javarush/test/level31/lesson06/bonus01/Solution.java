package com.javarush.test.level31.lesson06.bonus01;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipInputStream;

/* Разархивируем файл
В метод main приходит список аргументов.
Первый аргумент - имя результирующего файла resultFileName, остальные аргументы - имена файлов fileNamePart.
Каждый файл (fileNamePart) - это кусочек zip архива. Нужно разархивировать целый файл, собрав его из кусочков.
Записать разархивированный файл в resultFileName.
Архив внутри может содержать файл большой длины, например, 50Mb.
Внутри архива может содержаться файл с любым именем.

Пример входных данных. Внутри архива находится один файл с именем abc.mp3:
C:/result.mp3
C:/pathToTest/test.zip.003
C:/pathToTest/test.zip.001
C:/pathToTest/test.zip.004
C:/pathToTest/test.zip.002
*/
public class Solution {
    public static void main(String[] args) throws IOException {
        List<String> fileParts = new ArrayList<>();
        int sizeArgs = args.length;
        for (int i = 1; i < sizeArgs; i++) {
            fileParts.add(args[i]);
        }
        System.out.println("resultFile path = " + args[0]);
        System.out.println("file parts = " + fileParts);

        Collections.sort(fileParts, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o1.compareTo(o2);
            }
        });

        System.out.println("file parts after sort");
        System.out.println(fileParts);

        byte[] buffer = new byte[2048];


        for (String zipPart : fileParts) {
            FileOutputStream resultFile = new FileOutputStream(args[0]+".zip", true);
            System.out.println(zipPart + " is writing");
            InputStream partInputStream = new FileInputStream(zipPart);
            copyInputStreamToOutputStream(partInputStream, resultFile);
        }
        //extract from one zip

        ZipInputStream zipIn = new ZipInputStream(new FileInputStream(args[0]+".zip"));
        ZipEntry entry = zipIn.getNextEntry();
        // iterates over entries in the zip file
        while (entry != null) {
            String filePath = args[0];
            if (!entry.isDirectory()) {
                // if the entry is a file, extracts it
                BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(filePath));
                byte[] bytesIn = new byte[2048];
                int read = 0;
                while ((read = zipIn.read(bytesIn)) != -1) {
                    bos.write(bytesIn, 0, read);
                }
                bos.close();

            }
            zipIn.closeEntry();
            entry = zipIn.getNextEntry();
        }
        zipIn.close();

        File file = new File(args[0]+".zip");
        file.delete();


    }

    public static void copyInputStreamToOutputStream(final InputStream in,
                                                     final OutputStream out) throws IOException
    {
        try
        {
            try
            {
                final byte[] buffer = new byte[1024];
                int n;
                while ((n = in.read(buffer)) != -1)
                    out.write(buffer, 0, n);
            }
            finally
            {
                out.close();
            }
        }
        finally
        {
            in.close();
        }
    }
}

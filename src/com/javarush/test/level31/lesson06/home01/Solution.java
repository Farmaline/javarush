package com.javarush.test.level31.lesson06.home01;


import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

/* Добавление файла в архив
В метод main приходит список аргументов.
Первый аргумент - полный путь к файлу fileName.
Второй аргумент - путь к zip-архиву.
Добавить файл (fileName) внутрь архива в директорию 'new'.
Если в архиве есть файл с таким именем, то заменить его.

Пример входных данных:
C:/result.mp3
C:/pathToTest/test.zip

Файлы внутри test.zip:
a.txt
b.txt

После запуска Solution.main архив test.zip должен иметь такое содержимое:
new/result.mp3
a.txt
b.txt

Подсказка: нужно сначала куда-то сохранить содержимое всех энтри,
а потом записать в архив все энтри вместе с добавленным файлом.
Пользоваться файловой системой нельзя.
*/
public class Solution {


    private static final char SYSTEM_FILE_SEPARATOR = File.separatorChar;
    private static final char ZIP_FILE_SEPARATOR = '/';

    public static void main(String[] args) throws IOException {
       // System.out.println("args[0]=" + args[0]);
      //  System.out.println("args[1]=" + args[1]);

        String fileNamePath = args[0];
        String zipPath = args[1];
        Map<String, byte[]> map = new HashMap<>();

        String fileName = getNameFromFilePath(fileNamePath, SYSTEM_FILE_SEPARATOR);
      //  System.out.println("fileName = " + fileName);


        FileInputStream zipIn = new FileInputStream(zipPath);
        ZipInputStream zis = new ZipInputStream(zipIn);

        ZipEntry entry = null;
        ByteArrayOutputStream data = new ByteArrayOutputStream();
        while ((entry = zis.getNextEntry()) != null) {
            String zipEntryPath = entry.getName();
            String zipEntryFileName = getNameFromFilePath(zipEntryPath, ZIP_FILE_SEPARATOR);
            while (zis.available() > 0) {
                int res = zis.read();
                if (res!=-1){
                    data.write(res);
                }
                //System.out.println(res);
            }
            map.put(zipEntryPath, data.toByteArray());
           // System.out.println("zipEntryPath=" + zipEntryPath + " zipEntryFileName=" + zipEntryFileName);
        }
        data.close();
        zis.close();

       // System.out.println("isMapEmtry? =" + map.isEmpty() + " " + map);


        FileInputStream inputStream = new FileInputStream(fileNamePath);
        ByteArrayOutputStream fileData = new ByteArrayOutputStream();
        while (inputStream.available() > 0) {
            int readData = inputStream.read();
            if (readData!=-1){
                fileData.write(readData);
            }
        }
        fileData.close();
        inputStream.close();

        //FileOutputStream zipWriteFile = new FileOutputStream(zipPath);
        //ZipOutputStream outZip = new ZipOutputStream(zipWriteFile);
        boolean b = false;
        for (Map.Entry<String, byte []> mapElement : map.entrySet()){
            if (getNameFromFilePath(mapElement.getKey(), ZIP_FILE_SEPARATOR).equals(fileName)) {
               // System.out.println("wow file found!!!");
              // System.out.println("Changing value for " + mapElement.getKey());
                //map.put(mapElement.getKey(), fileData.toByteArray());
                map.remove(mapElement.getKey());
                map.put("new/" + fileName, fileData.toByteArray());
                b = true;
                break;

                //outZip.putNextEntry(new ZipEntry(mapElement.getKey()));
                //outZip.write(mapElement.getValue());
            } /*else {
                //write to new folder
              //  System.out.println("file not found");
                map.put("new/" + fileName, fileData);
                break;

            }*/
        }

      //  System.out.println("isMapEmtry? =" + map.isEmpty() + " " + map);
        if (b) writeMapToZip(map, zipPath);
    }

    private static void writeMapToZip(Map<String, byte[]> map, String zipPath) throws IOException {

        FileOutputStream zipWriteFile = new FileOutputStream(zipPath);
        ZipOutputStream outZip = new ZipOutputStream(zipWriteFile);
        for (Map.Entry<String, byte[]> pair : map.entrySet())
        {
            outZip.putNextEntry(new ZipEntry(pair.getKey()));
            outZip.write(pair.getValue());
        }
        outZip.close();
        zipWriteFile.close();

    }

    private static String getNameFromFilePath(String filePath, char fileSeparator) {
        return filePath.substring(filePath.lastIndexOf(fileSeparator) + 1);
    }


}

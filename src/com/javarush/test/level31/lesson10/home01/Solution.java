package com.javarush.test.level31.lesson10.home01;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.InvalidPropertiesFormatException;
import java.util.Properties;

/* Читаем конфиги
Реализовать метод getProperties, который должен считывать свойства из переданного файла fileName.
fileName может иметь любое расширение - как xml, так и любое другое, или вообще не иметь.
Нужно обеспечить корректное чтение свойств.
При возникновении ошибок должен возвращаться пустой объект.
Метод main не участвует в тестировании.
Подсказка: возможно, Вам понадобится File.separator.
*/
public class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();
        Properties properties = solution.getProperties("src/com/javarush/test/level31/lesson10/home01/properties.xml");
        System.out.println(properties.isEmpty());
        properties.list(System.out);

        properties = solution.getProperties("src/com/javarush/test/level31/lesson10/home01/properties.txt");
        System.out.println(properties.isEmpty());
        properties.list(System.out);

        properties = solution.getProperties("src/com/javarush/test/level31/lesson10/home01/notExists");
        properties.list(System.out);
    }

    public Properties getProperties(String fileName) {

        Properties properties = new Properties();
        Path path = Paths.get(fileName).toAbsolutePath();
        System.out.println(path.toAbsolutePath());

        String resultPath = String.valueOf(path.toAbsolutePath());
        System.out.println("resultPath="+resultPath);

        try {
            properties.loadFromXML(new FileInputStream(resultPath));
        }
        catch (InvalidPropertiesFormatException e) {
            try {
                properties.load(new FileInputStream(resultPath));
            } catch (IOException e1) {
                System.out.println(e1);
                properties = new Properties();
            }
        } catch (Exception e){
            System.out.println("e");
            properties = new Properties();
        }

        File file = new File(fileName);
        System.out.println("x="+file.getAbsolutePath());

        return properties;
    }
}

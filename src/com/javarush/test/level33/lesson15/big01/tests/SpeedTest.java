package com.javarush.test.level33.lesson15.big01.tests;

import com.javarush.test.level33.lesson15.big01.Helper;
import com.javarush.test.level33.lesson15.big01.Shortener;
import com.javarush.test.level33.lesson15.big01.strategies.HashBiMapStorageStrategy;
import com.javarush.test.level33.lesson15.big01.strategies.HashMapStorageStrategy;
import org.junit.Assert;
import org.junit.Test;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Administrator on 22.01.2016.
 */
public class SpeedTest {
    public long getTimeForGettingIds(Shortener shortener, Set<String> strings, Set<Long> ids) {
        long startTime = new Date().getTime();
        for (String string : strings) {
            ids.add(shortener.getId(string));
        }
        long endTime = new Date().getTime();
        return endTime - startTime;
    }

    public long getTimeForGettingStrings(Shortener shortener, Set<Long> ids, Set<String> strings) {
        long startTime = new Date().getTime();
        for (Long id : ids) {
            strings.add(shortener.getString(id));
        }
        long endTime = new Date().getTime();
        return endTime - startTime;
    }

    @Test
    public void testHashMapStorage() {
        Shortener shortener1 = new Shortener(new HashMapStorageStrategy());
        Shortener shortener2 = new Shortener(new HashBiMapStorageStrategy());
        Set<String> origStrings = new HashSet<>();
        for (int i = 0; i < 10000; i++) {
            origStrings.add(Helper.generateRandomString());
        }
        Set<Long> ids1 = new HashSet<>();
        Set<Long> ids2 = new HashSet<>();

        long timeShoter1 = getTimeForGettingIds(shortener1,origStrings,ids1);
        long timeShoter2 = getTimeForGettingIds(shortener2,origStrings,ids2);
        Assert.assertTrue(timeShoter1>timeShoter2);

        timeShoter1 = getTimeForGettingStrings(shortener1,ids1,new HashSet<String>());
        timeShoter2 = getTimeForGettingStrings(shortener2,ids2,new HashSet<String>());

        Assert.assertEquals(timeShoter1,timeShoter2,5);

    }
}


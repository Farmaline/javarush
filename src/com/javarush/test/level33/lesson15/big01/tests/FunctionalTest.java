package com.javarush.test.level33.lesson15.big01.tests;

import com.javarush.test.level33.lesson15.big01.Shortener;
import com.javarush.test.level33.lesson15.big01.strategies.*;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by Administrator on 21.01.2016.
 */

public class FunctionalTest {
    public void testStorage(Shortener shortener) {
        String first = "123456789";
        String second = "000000000123";
        String third = "123456789";
        long firstStringId = shortener.getId(first);
        long secondStringId = shortener.getId(second);
        long thirdStringId = shortener.getId(third);
        Assert.assertNotEquals(secondStringId, firstStringId);
        Assert.assertNotEquals(secondStringId, thirdStringId);
        Assert.assertEquals(firstStringId, thirdStringId);
        String finalFirst = shortener.getString(firstStringId);
        String finalSecond = shortener.getString(secondStringId);
        String finalThird = shortener.getString(thirdStringId);
        Assert.assertEquals(first, finalFirst);
        Assert.assertEquals(second, finalSecond);
        Assert.assertEquals(third, finalThird);
    }

    @Test
    public void testHashMapStorageStrategy() {
        testStorage(new Shortener(new HashMapStorageStrategy()));
    }

    @Test
    public void testOurHashMapStorageStrategy() {
        testStorage(new Shortener(new OurHashMapStorageStrategy()));
    }

    @Test
    public void testFileStorageStrategy() {
        testStorage(new Shortener(new FileStorageStrategy()));
    }

    @Test
    public void testHashBiMapStorageStrategy() {
        testStorage(new Shortener(new HashBiMapStorageStrategy()));
    }

    @Test
    public void testDualHashBidiMapStorageStrategy() {
        testStorage(new Shortener(new DualHashBidiMapStorageStrategy()));
    }

    @Test
    public void testOurHashBiMapStorageStrategy() {
        testStorage(new Shortener(new OurHashBiMapStorageStrategy()));
    }

}

package com.javarush.test.level33.lesson15.big01;

import com.javarush.test.level33.lesson15.big01.strategies.*;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Administrator on 12.01.2016.
 */

public class Solution {
    public static Set<Long> getIds(Shortener shortener, Set<String> strings) {
        Set<Long> resultSet = new HashSet<>();
        for (String text : strings) {
            resultSet.add(shortener.getId(text));
        }
        return resultSet;
    }

    public static Set<String> getStrings(Shortener shortener, Set<Long> keys) {
        Set<String> resultSet = new HashSet<>();
        for (Long longKey : keys) {
            resultSet.add(shortener.getString(longKey));
        }
        return resultSet;
    }

    public static void testStrategy(StorageStrategy strategy, long elementsNumber) {
        Helper.printMessage(strategy.getClass().getSimpleName());
        Set<String> testGenerateString = new HashSet<>();
        Set<Long> testLongSet;
        Set<String> testStringSet;
        for (long i = 0; i < elementsNumber; i++) {
            testGenerateString.add(Helper.generateRandomString());
        }
        Shortener shortener = new Shortener(strategy);
        long startID = new Date().getTime();
        testLongSet = getIds(shortener, testGenerateString);
        long endID = new Date().getTime();
        Long duration = endID - startID;
        Helper.printMessage(duration.toString());
        long startStr = new Date().getTime();
        testStringSet = getStrings(shortener, testLongSet);
        long endStr = new Date().getTime();
        Long durationStr = endStr - startStr;
        Helper.printMessage(durationStr.toString());
        if (testGenerateString.size() == testStringSet.size()) {
            Helper.printMessage("Тест пройден.");
        } else Helper.printMessage("Тест не пройден.");
    }

    public static void main(String[] args) {
        testStrategy(new HashMapStorageStrategy(), 10000);
        testStrategy(new FileStorageStrategy(),100);
        testStrategy(new OurHashMapStorageStrategy(),10000);
        testStrategy(new OurHashBiMapStorageStrategy(),10000);
        testStrategy(new OurHashBiMapStorageStrategy(),10000);
        testStrategy(new DualHashBidiMapStorageStrategy(),10000);
    }
}

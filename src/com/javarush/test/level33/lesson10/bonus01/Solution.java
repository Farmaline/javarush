package com.javarush.test.level33.lesson10.bonus01;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/* Комментарий внутри xml
Реализовать метод toXmlWithComment, который должен возвращать строку - xml представление объекта obj.
В строке перед каждым тэгом tagName должен быть вставлен комментарий comment.
Сериализация obj в xml может содержать CDATA с искомым тегом. Перед ним вставлять комментарий не нужно.

Пример вызова:  toXmlWithComment(firstSecondObject, "second", "it's a comment")
Пример результата:
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<first>
    <!--it's a comment-->
    <second>some string</second>
    <!--it's a comment-->
    <second>some string</second>
    <!--it's a comment-->
    <second><![CDATA[need CDATA because of < and >]]></second>
    <!--it's a comment-->
    <second/>
</first>
*/
public class Solution {
    public static String toXmlWithComment(Object obj, String tagName, String comment) throws JAXBException {
        StringWriter stringWriter = new StringWriter();
        JAXBContext context = JAXBContext.newInstance(obj.getClass());
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        marshaller.marshal(obj, stringWriter);

        List<String> strings = new ArrayList<>();
        strings = Arrays.asList(stringWriter.toString().split("\n"));
        /*for (int i =0; i < strings.size(); i++){
           strings.set(i, strings.get(i).trim());
        }*/
        String firstTag = "<" + tagName + ">";
        String secondTag = firstTag.substring(0, firstTag.length() - 1) + "/>";

       /* for (String st : strings){
            System.out.println(st);
        }*/
        String resultString = "";
        int isCDdataIncide = 0;
        for (String line : strings) {
            if (line.trim().contains("<![CDATA[")) {
                isCDdataIncide++;
            }
            if (line.trim().contains("]]>")) {
                isCDdataIncide--;
            }
            if ((line.trim().startsWith(firstTag) || line.trim().startsWith(secondTag)) && isCDdataIncide == 0) {
                resultString += line.substring(0, line.indexOf("<")) + "<!--" + comment + "-->\n";
            }
            resultString += line + "\n";
        }
        return resultString.trim();
    }
}

package com.javarush.test.level30.lesson06.home01;

import java.util.concurrent.RecursiveTask;

/**
 * Created by Administrator on 23.10.2015.
 */
public class BinaryRepresentationTask extends RecursiveTask {
    private int binaty;
    public BinaryRepresentationTask(int i) {
        binaty = i;
    }

    @Override
    protected String compute() {
        int a = binaty % 2;
        int b = binaty / 2;
        String result = String.valueOf(a);
        if (b > 0) {
            BinaryRepresentationTask task = new BinaryRepresentationTask(b);
            task.fork();
            return task.join() + result;
        }
        return  result;
    }
}

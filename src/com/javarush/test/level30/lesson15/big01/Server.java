package com.javarush.test.level30.lesson15.big01;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Administrator on 27.10.2015.
 */


public class Server {
    private static Map<String, Connection> connectionMap = new ConcurrentHashMap<>();

    public static void main(String[] args) {
        ConsoleHelper.writeMessage("enter socket id:");
        try (ServerSocket serverSocket = new ServerSocket(ConsoleHelper.readInt())) {
            ConsoleHelper.writeMessage("server ON");
            while (true) {
                Socket socket = serverSocket.accept();
                Handler handler = new Handler(socket);
                handler.start();
            }
        } catch (IOException e) {
            ConsoleHelper.writeMessage("server ERROR");
        }

    }

    private static class Handler extends Thread {
        private Socket socket;

        public Handler(Socket socket) {
            this.socket = socket;
        }

        public void run() {
            ConsoleHelper.writeMessage("connction success ip:" + socket.getRemoteSocketAddress());

            String nameOfNewUser = null;
            try (Connection connection = new Connection(socket)) {
                nameOfNewUser = serverHandshake(connection);
                sendBroadcastMessage(new Message(MessageType.USER_ADDED, nameOfNewUser));
                sendListOfUsers(connection, nameOfNewUser);
                serverMainLoop(connection, nameOfNewUser);
            } catch (IOException e) {
                ConsoleHelper.writeMessage("connection data remote socket error");
            } catch (ClassNotFoundException e1) {
                ConsoleHelper.writeMessage("connection data remote socket error");
            } finally {
                if (nameOfNewUser != null) {
                    connectionMap.remove(nameOfNewUser);
                    sendBroadcastMessage(new Message(MessageType.USER_REMOVED, nameOfNewUser));
                }
            }
            ConsoleHelper.writeMessage("connection close");
        }

        private String serverHandshake(Connection connection) throws IOException, ClassNotFoundException {
            Message massageGetName = new Message(MessageType.NAME_REQUEST);
            Message receiveMassage;
            boolean isNameMessage = true;
            String name = "";
            while (isNameMessage) {
                connection.send(massageGetName);
                receiveMassage = connection.receive();
                if (receiveMassage.getType()==MessageType.USER_NAME) {
                    name = receiveMassage.getData();
                    if (name != null && name.length() > 0) {
                        if (!connectionMap.containsKey(name)) {
                            connectionMap.put(name, connection);
                            isNameMessage = false;
                        }
                    }
                }
            }
            connection.send(new Message(MessageType.NAME_ACCEPTED));
            return name;
        }

        private void sendListOfUsers(Connection connection, String userName) throws IOException {
            for (Map.Entry<String, Connection> elOfMap : connectionMap.entrySet()) {
                if (!userName.equals(elOfMap.getKey())) {
                    connection.send(new Message(MessageType.USER_ADDED, elOfMap.getKey()));
                }
            }
        }

        private void serverMainLoop(Connection connection, String userName) throws IOException, ClassNotFoundException {
            Message userMassage;
            while (true) {
                userMassage = connection.receive();
                if (userMassage.getType()==MessageType.TEXT) {
                    sendBroadcastMessage(new Message(MessageType.TEXT, String.format("%s: %s", userName, userMassage.getData())));
                } else {
                    ConsoleHelper.writeMessage("message error");
                }
            }
        }


    }

    public static void sendBroadcastMessage(Message message) {

        try {
            for (Map.Entry<String, Connection> el : connectionMap.entrySet()) {
                el.getValue().send(message);
            }

        } catch (IOException e) {
            ConsoleHelper.writeMessage("message error");
        }
    }

}

package com.javarush.test.level30.lesson15.big01.client;

import com.javarush.test.level30.lesson15.big01.ConsoleHelper;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.Random;

/**
 * Created by Administrator on 06.11.2015.
 */

public class BotClient extends Client {
    @Override
    protected SocketThread getSocketThread() {
        return new BotSocketThread();
    }

    @Override
    protected boolean shouldSentTextFromConsole() {
        return false;
    }

    @Override
    protected String getUserName() {
        return String.format("date_bot_%d", new Random().nextInt(101));
    }

    public static void main(String[] args) {
        BotClient botClient = new BotClient();
        botClient.run();
    }


    public class BotSocketThread extends SocketThread {
        @Override
        protected void processIncomingMessage(String message) {

            ConsoleHelper.writeMessage(message);
            String[] array = message.split(": ");
            if (array.length == 2) {
                String truString = array[1].toLowerCase();
                String nameUser = array[0];
                SimpleDateFormat simpleDateFormat = null;


                switch (truString) {
                    case "дата":
                        simpleDateFormat = new SimpleDateFormat("d.MM.YYYY");
                        break;
                    case "день":
                        simpleDateFormat = new SimpleDateFormat("d");
                        break;
                    case "месяц":
                        simpleDateFormat = new SimpleDateFormat("MMMM");
                        break;
                    case "год":
                        simpleDateFormat = new SimpleDateFormat("YYYY");
                        break;
                    case "время":
                        simpleDateFormat = new SimpleDateFormat("H:mm:ss");
                        break;
                    case "час":
                        simpleDateFormat = new SimpleDateFormat("H");
                        break;
                    case "минуты":
                        simpleDateFormat = new SimpleDateFormat("m");
                        break;
                    case "секунды":
                        simpleDateFormat = new SimpleDateFormat("s");
                        break;
                }

                if (simpleDateFormat!=null) {
                    truString = String.format("Информация для %s: %s", nameUser, simpleDateFormat.format(new GregorianCalendar().getTime()));
                    sendTextMessage(truString);
                }
            }

        }

        @Override
        protected void clientMainLoop() throws IOException, ClassNotFoundException {
            sendTextMessage("Привет чатику. Я бот. Понимаю команды: дата, день, месяц, год, время, час, минуты, секунды.");
            super.clientMainLoop();
        }
    }

}

package com.javarush.test.level20.lesson02.task01;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/* Читаем и пишем в файл: Human
Реализуйте логику записи в файл и чтения из файла для класса Human
Поле name в классе Human не может быть пустым
В файле your_file_name.tmp может быть несколько объектов Human
Метод main реализован только для вас и не участвует в тестировании
*/
public class Solution {
    public static void main(String[] args) {
        //you can find your_file_name.tmp in your TMP directory or fix outputStream/inputStream according to your real file location
        //вы можете найти your_file_name.tmp в папке TMP или исправьте outputStream/inputStream в соответствии с путем к вашему реальному файлу
        try {

            //File your_file_name = File.createTempFile("C:\\k.txt", null);
            File your_file_name = new File("C:\\k.txt");
            OutputStream outputStream = new FileOutputStream(your_file_name);
            InputStream inputStream = new FileInputStream(your_file_name);

            Human ivanov = new Human("Ivanov", new Asset("home"), new Asset("car"));

            //

            OutputStream fileOut = new FileOutputStream(new File("C:\\l.txt"));
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(ivanov);
            out.close();

            //read
            FileInputStream fileIn = new FileInputStream("C:\\l.txt");
            ObjectInputStream in = new ObjectInputStream(fileIn);
            Human e = (Human) in.readObject();
            in.close();

            System.out.println(e.name + " "+ e.assets);

            //
            ivanov.save(outputStream);
            outputStream.flush();

            Human somePerson = new Human();
            somePerson.load(inputStream);
            //check here that ivanov equals to somePerson - проверьте тут, что ivanov и somePerson равны
           // System.out.println(ivanov.name+ ivanov.assets +" " + somePerson.name + somePerson.assets);
            inputStream.close();

        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Oops, something wrong with my file");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Oops, something wrong with save/load method");
        }
    }


    public static class Human implements Serializable {
        public String name;
        public List<Asset> assets = new ArrayList<>();

        public Human() {
        }

        public Human(String name, Asset... assets) {
            this.name = name;
            if (assets != null) {
                this.assets.addAll(Arrays.asList(assets));
            }
        }

        public void save(OutputStream outputStream) throws Exception {
            //implement this method - реализуйте этот метод
            PrintWriter printWriter = new PrintWriter(outputStream);
            String hasName = (name !=null)? "Yes" : "No";
            printWriter.println(hasName);
            if(hasName.equals("Yes"))
            {
                printWriter.println(name);
            }
            printWriter.flush();
            //String assetsPresent = (!assets.isEmpty())? "Yes":"No";
            //printWriter.println(assetsPresent);
            //printWriter.flush();
            //if (assetsPresent.equals("Yes"))
            //{
                for(Asset as : assets)
                {
                    printWriter.println("Yes");
                    printWriter.flush();
                    as.save(outputStream);
                }
            printWriter.println("No");

            //}
            printWriter.flush();



        }

        public void load(InputStream inputStream) throws Exception {
            //implement this method - реализуйте этот метод
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String hasName = bufferedReader.readLine();

            if("Yes".equals(hasName)){
                this.name = bufferedReader.readLine();


                while (true) {
                    String hasAsser = bufferedReader.readLine();
                    if (hasAsser.equals("Yes"))
                    {

                        Asset asset = new Asset("");
                        asset.load(bufferedReader);
                        assets.add(asset);

                    }else {break;}
                }



            }


        }
    }
}

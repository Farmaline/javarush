package com.javarush.test.level20.lesson02.task01;

import java.io.*;

public class Asset implements Serializable{
    public Asset(String name) {
        this.name = name;
    }

    private String name;
    private double price;

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Asset{" +
                "name='" + name + '\'' +
                ", price=" + price +
                '}';
    }

    public void save(OutputStream outputStream) throws Exception {
        PrintWriter printWriter = new PrintWriter(outputStream);
        printWriter.println(name);
        String hasPrice = (price != 0) ? "Yes" : "No";
        printWriter.println(hasPrice);
        if (hasPrice.equals("Yes")) {
            printWriter.println(price);
        }

        printWriter.flush();


    }
    public void load(BufferedReader bufferedReader) throws Exception {



        this.name = bufferedReader.readLine();

        String hasPrice = bufferedReader.readLine();
        if(hasPrice.equals("Yes")){

            setPrice(Double.parseDouble(bufferedReader.readLine()));
        }

    }


}

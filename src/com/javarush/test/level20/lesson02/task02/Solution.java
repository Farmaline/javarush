package com.javarush.test.level20.lesson02.task02;

import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/* Читаем и пишем в файл: JavaRush
Реализуйте логику записи в файл и чтения из файла для класса JavaRush
В файле your_file_name.tmp может быть несколько объектов JavaRush
Метод main реализован только для вас и не участвует в тестировании
*/
public class Solution {
    public static void main(String[] args) {
        //you can find your_file_name.tmp in your TMP directory or fix outputStream/inputStream according to your real file location
        //вы можете найти your_file_name.tmp в папке TMP или исправьте outputStream/inputStream в соответствии с путем к вашему реальному файлу
        try {
            //File your_file_name = File.createTempFile("your_file_name", null);
            File your_file_name = new File("C:\\k.txt");
            OutputStream outputStream = new FileOutputStream(your_file_name);
            InputStream inputStream = new FileInputStream(your_file_name);

            JavaRush javaRush = new JavaRush();
            //initialize users field for the javaRush object here - инициализируйте поле users для объекта javaRush тут
            User user = new User();
            user.setFirstName("Nord1");
            user.setLastName("Patiuk");
            user.setMale(true);
            user.setBirthDate(new Date(23));
            user.setCountry(User.Country.UKRAINE);
            System.out.println(user.getFirstName() + " " + user.getLastName() + " " + user.isMale() + " " + user.getBirthDate() + " " + user.getCountry());
            javaRush.users.add(user);

            User user2 = new User();
            user2.setFirstName("Nord2");
            user2.setLastName("Patiuk");
            System.out.println(user2.getFirstName() + " " + user2.getLastName() + " " + user2.isMale() + " " + user2.getBirthDate() + " " + user2.getCountry());
            javaRush.users.add(user2);

            javaRush.save(outputStream);
            outputStream.flush();

            //second JavaRush
            JavaRush javaRush2 = new JavaRush();
            //initialize users field for the javaRush object here - инициализируйте поле users для объекта javaRush тут
            System.out.println(user.getFirstName() + " " + user.getLastName() + " " + user.isMale() + " " + user.getBirthDate() + " " + user.getCountry());
            javaRush2.users.add(user);

            System.out.println(user2.getFirstName() + " " + user2.getLastName() + " " + user2.isMale() + " " + user2.getBirthDate() + " " + user2.getCountry());
            javaRush2.users.add(user2);

            javaRush2.save(outputStream);
            outputStream.flush();

            JavaRush loadedObject = new JavaRush();
            loadedObject.load(inputStream);
            System.out.println("Loaded users:"+loadedObject.users.size());
            for(User loadedUser : loadedObject.users){
                System.out.println(loadedUser.getFirstName() + " " + loadedUser.getLastName() + " " + loadedUser.isMale() + " " + loadedUser.getBirthDate() + " " + loadedUser.getCountry());

            }

            JavaRush loadedObject2 = new JavaRush();
            loadedObject2.load(inputStream);
            System.out.println("Loaded users:"+loadedObject2.users.size());
            for(User loadedUser : loadedObject2.users){
                System.out.println(loadedUser.getFirstName() + " " + loadedUser.getLastName() + " " + loadedUser.isMale() + " " + loadedUser.getBirthDate() + " " + loadedUser.getCountry());

            }
            //check here that javaRush object equals to loadedObject object - проверьте тут, что javaRush и loadedObject равны

            outputStream.close();
            inputStream.close();

        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Oops, something wrong with my file");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Oops, something wrong with save/load method");
        }
    }

    public static class JavaRush {
        public List<User> users = new ArrayList<>();

        public void save(OutputStream outputStream) throws Exception {
            PrintWriter printWriter = new PrintWriter(outputStream);
            printWriter.println("YesJavaRush");
            if (users.size() != 0) {
                for (User element : users) {
                    printWriter.println("Yes");
                    printWriter.flush();

                    element.save(outputStream);


                }
                printWriter.println("No");
                printWriter.flush();
            } else printWriter.println("No");

        }

        public void load(InputStream inputStream) throws Exception {

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));


            String hasName = bufferedReader.readLine();

            if("YesJavaRush".equals(hasName)) {

                while (true) {
                    String hasUser = bufferedReader.readLine();
                    if (hasUser.equals("Yes")) {

                        User user = new User();
                        user.load(bufferedReader);
                        users.add(user);

                    } else {
                        break;
                    }
                }
            }
        }
    }
}

package com.javarush.test.level20.lesson02.task02;

import java.io.BufferedReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Date;

public class User{
    private String firstName;
    private String lastName;
    private Date birthDate;
    private boolean isMale;
    private Country country;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public boolean isMale() {
        return isMale;
    }

    public void setMale(boolean male) {
        isMale = male;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public static enum Country{
        UKRAINE("Ukraine"),
        RUSSIA("Russia"),
        OTHER("Other");

        private String name;

        private Country(String name) {
            this.name = name;
        }

        public String getDisplayedName() {
            return this.name;
        }
    }
    public void save(OutputStream outputStream) throws Exception {
        PrintWriter printWriter = new PrintWriter(outputStream);
        //1
        String hasElement = (firstName != null) ? "Yes" : "No";
        printWriter.println(hasElement);
        if (hasElement.equals("Yes")) {
            printWriter.println(firstName);
        }
        //2
        hasElement = (lastName != null) ? "Yes" : "No";
        printWriter.println(hasElement);
        if (hasElement.equals("Yes")) {
            printWriter.println(lastName);
        }
        //3
        hasElement = (birthDate != null) ? "Yes" : "No";
        printWriter.println(hasElement);
        if (hasElement.equals("Yes")) {
            printWriter.println(birthDate.getTime());
        }
        //4
        hasElement = (isMale == true) ? "true" : "false";
        printWriter.println(hasElement);

        //5
        hasElement = (country != null) ? "Yes" : "No";
        printWriter.println(hasElement);
        if (hasElement.equals("Yes")) {
            printWriter.println(country.getDisplayedName());
        }

        printWriter.flush();



    }
    public void load(BufferedReader bufferedReader) throws Exception {




        String hasElement = bufferedReader.readLine();
        if(hasElement.equals("Yes")){
            setFirstName(bufferedReader.readLine());
        }

        hasElement = bufferedReader.readLine();
        if(hasElement.equals("Yes")){
            setLastName(bufferedReader.readLine());

        }

        hasElement = bufferedReader.readLine();
        if(hasElement.equals("Yes")){
            setBirthDate( new Date (Long.parseLong(bufferedReader.readLine())));

        }

        hasElement = bufferedReader.readLine();
        if(hasElement.equals("true")){
            setMale(true);

        }else {setMale(false);}

        hasElement = bufferedReader.readLine();
        if(hasElement.equals("Yes")){
            String countryLoad = bufferedReader.readLine();
            if (countryLoad.equals(Country.UKRAINE.getDisplayedName()))
            {
                setCountry(Country.UKRAINE);
            }
            if(countryLoad.equals(Country.RUSSIA.getDisplayedName()))
            {
                setCountry(Country.RUSSIA);
            }
            if(countryLoad.equals(Country.OTHER.getDisplayedName()))
            {
                setCountry(Country.OTHER);
            }


        }


    }

}

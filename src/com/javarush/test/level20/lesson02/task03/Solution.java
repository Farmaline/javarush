package com.javarush.test.level20.lesson02.task03;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/* Знакомство с properties
В методе fillInPropertiesMap считайте имя файла с консоли и заполните карту properties данными из файла.
Про .properties почитать тут - http://ru.wikipedia.org/wiki/.properties
Реализуйте логику записи в файл и чтения из файла для карты properties.
*/
public class Solution {
    public static Map<String, String> properties = new HashMap<>();
    Properties props = new java.util.Properties();
    public void fillInPropertiesMap() throws Exception {
        //implement this method - реализуйте этот метод
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        FileInputStream fis =  new FileInputStream(bufferedReader.readLine());
        load(fis);


    }

    public void save(OutputStream outputStream) throws Exception {
        //implement this method - реализуйте этот метод
        for(Map.Entry<String,String> elem : properties.entrySet()){
            props.setProperty(elem.getKey(), elem.getValue());
        }
        props.store(outputStream,"");
        outputStream.close();
    }

    public void load(InputStream inputStream) throws Exception {
        //implement this method - реализуйте этот метод

        props.load(inputStream);

        for(Map.Entry<Object,Object> elem : props.entrySet()){

            properties.put(String.valueOf(elem.getKey()), String.valueOf(elem.getValue()));
        }
    }
}

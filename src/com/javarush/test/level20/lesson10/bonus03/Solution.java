package com.javarush.test.level20.lesson10.bonus03;

import java.util.ArrayList;
import java.util.List;

/* Кроссворд
1. Дан двумерный массив, который содержит буквы английского алфавита в нижнем регистре.
2. Метод detectAllWords должен найти все слова из words в массиве crossword.
3. Элемент(startX, startY) должен соответствовать первой букве слова, элемент(endX, endY) - последней.
text - это само слово, располагается между начальным и конечным элементами
4. Все слова есть в массиве.
5. Слова могут быть расположены горизонтально, вертикально и по диагонали как в нормальном, так и в обратном порядке.
6. Метод main не участвует в тестировании
*/
public class Solution {
    public static void main(String[] args) {
        int[][] crossword = new int[][]{
                {'f', 'd', 'e', 'r', 'l', 'k'},
                {'u', 's', 'a', 'm', 'e', 'o'},
                {'l', 'n', 'g', 'r', 'o', 'v'},
                {'m', 'l', 'p', 'r', 'r', 'h'},
                {'p', 'o', 'e', 'e', 'j', 'j'}
        };
        detectAllWords(crossword, "home", "same");
       //System.out.println(crossword[0][0] == 'f');
       // System.out.println(detectAllWords(crossword, "home", "same", "rrmr","plgml", "nord", "ell", "r"));
       /* int[] intChats = {'я', 'Я', 'й'};
        String text = "яЯй";
        System.out.println("write text getByte");
        for (byte b : text.getBytes()) {
            System.out.println((char)(b&255));
        }
        System.out.println();
        System.out.println("write intCarsInt");
        for (int b : intChats) {
            System.out.println(b);
        }
        char [] getCharFromWord = new char[text.length()];
        text.getChars(0,text.length() , getCharFromWord,0);
        for (char c : getCharFromWord){
            System.out.println((int)c);
        }*/



        /*
Ожидаемый результат
home - (5, 3) - (2, 0)
same - (1, 1) - (4, 1)
         */
    }

    public static List<Word> detectAllWords(int[][] crossword, String... words) {
        List<Word> resultList = new ArrayList<>();
        for (String wrd : words) {
            String word = wrd;
            int sizeWord = word.length();
            char[] charArray = word.toCharArray();
            int crosswordXsize = crossword[0].length;
            int crosswordYsize = crossword.length;
            System.out.println("Xsize = " + crosswordXsize);
            System.out.println("Ysize = " + crosswordYsize);
            for (int y = 0; y < crosswordYsize; y++) {
                for (int x = 0; x < crosswordXsize; x++) {
                    if (crossword[y][x] == charArray[0]) {
                        resultList.addAll(giveWordIfFinde(y, x, crossword, charArray));
                    }
                }
            }
        }
        return resultList;
    }

    public static List<Word> giveWordIfFinde(int y, int x, int[][] cross, char[] charArray) {
        List<Word> wordsList = new ArrayList<>();
        int xSizeXXX = cross[0].length;
        int ySizeYYY = cross.length;
        int wordSize = charArray.length;
        //horizontally and vertically

        if ((y + 1 - wordSize) >= 0) {
            //up Y
            boolean isMatched = true;
            for (int i = 0; i < wordSize; i++) {
                if (cross[y - i][x] != charArray[i]) {
                    isMatched = false;
                    break;
                }

            }
            if (isMatched) {
                System.out.println("up Y");
                wordsList.add(giveWord(y,x, y - (wordSize - 1), x, charArray));
            }
        }
        if ((ySizeYYY - y - wordSize) >= 0) {
            //to the down Y
            boolean isMatched = true;
            for (int i = 0; i < wordSize; i++) {
                if (cross[y + i][x] != charArray[i]) {
                    isMatched = false;
                    break;
                }

            }
            if (isMatched) {
                System.out.println("to the down Y");
                wordsList.add(giveWord(y,x, y + (wordSize - 1), x, charArray));
            }
        }

        if ((x + 1 - wordSize) >= 0) {
            //to the left X
            boolean isMatched = true;
            for (int i = 0; i < wordSize; i++) {
                if (cross[y][x - i] != charArray[i]) {
                    isMatched = false;
                    break;
                }

            }
            if (isMatched) {
                System.out.println("to the left X");
                wordsList.add(giveWord(y,x,y, x - (wordSize -1), charArray));
            }
        }

        if ((xSizeXXX - x - wordSize) >= 0) {
            //to the right X
            boolean isMatched = true;
            for (int i = 0; i < wordSize; i++) {
                if (cross[y][x + i] != charArray[i]) {
                    isMatched = false;
                    break;
                }

            }
            if (isMatched) {
                System.out.println("to the right X");
                wordsList.add(giveWord(y,x,y, x + (wordSize -1), charArray));
            }
        }

        //diagonally
        if (((y + 1 - wordSize) >= 0) && ((xSizeXXX - x - wordSize) >= 0)) {
            //up right Y-- X++
            boolean isMatched = true;
            for (int i = 0; i < wordSize; i++) {
                if (cross[y - i][x + i] != charArray[i]) {
                    isMatched = false;
                    break;
                }

            }
            if (isMatched) {
                System.out.println("up right Y-- X++");
                wordsList.add(giveWord(y,x, y - (wordSize - 1), x + (wordSize -1), charArray));
            }
        }

        if ((((xSizeXXX - x - wordSize) >= 0) && (ySizeYYY - y - wordSize) >= 0)) {
            //down right Y++ X++
            boolean isMatched = true;
            for (int i = 0; i < wordSize; i++) {
                if (cross[y + i][x + i] != charArray[i]) {
                    isMatched = false;
                    break;
                }

            }
            if (isMatched) {
                System.out.println("down right Y++ X++");
                wordsList.add(giveWord(y,x, y + (wordSize - 1), x + (wordSize -1), charArray));
            }
        }

        if (((y + 1 - wordSize) >= 0) && ((x + 1 - wordSize) >= 0)) {
            //up left Y-- X--
            boolean isMatched = true;
            for (int i = 0; i < wordSize; i++) {
                if (cross[y - i][x - i] != charArray[i]) {
                    isMatched = false;
                    break;
                }

            }
            if (isMatched) {
                System.out.println("up left Y-- X--");
                wordsList.add(giveWord(y,x, y - (wordSize - 1), x - (wordSize -1), charArray));
            }
        }
        if (((x + 1 - wordSize) >= 0) && ((ySizeYYY - y - wordSize) >= 0)) {
            //down left Y++ X--
            boolean isMatched = true;
            for (int i = 0; i < wordSize; i++) {
                if (cross[y + i][x - i] != charArray[i]) {
                    isMatched = false;
                    break;
                }

            }
            if (isMatched) {
                System.out.println("down left Y++ X--");
                wordsList.add(giveWord(y,x, y + (wordSize - 1), x - (wordSize -1), charArray));
            }
        }


        return wordsList;
    }

    public static Word giveWord (int startY, int startX, int endY, int endX, char [] charsArray){
        Word word = new Word(String.copyValueOf(charsArray));
        word.setStartPoint(startX, startY);
        word.setEndPoint(endX,endY);
        return word;
    }


    public static class Word {
        private String text;
        private int startX;
        private int startY;
        private int endX;
        private int endY;

        public Word(String text) {
            this.text = text;
        }

        public void setStartPoint(int i, int j) {
            startX = i;
            startY = j;
        }

        public void setEndPoint(int i, int j) {
            endX = i;
            endY = j;
        }

        @Override
        public String toString() {
            return String.format("%s - (%d, %d) - (%d, %d)", text, startX, startY, endX, endY);
        }
    }
}

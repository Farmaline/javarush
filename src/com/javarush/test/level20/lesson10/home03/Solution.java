package com.javarush.test.level20.lesson10.home03;


import java.io.*;

/* Найти ошибки
Почему-то при сериализации/десериализации объекта класса B возникают ошибки.
Найдите проблему и исправьте ее.
Класс A не должен реализовывать интерфейсы Serializable и Externalizable.
Сигнатура класса В не содержит ошибку :)
*/
public class Solution  implements Serializable{
    private static final long serialVersionUID = 1L;
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("C:\\k.txt"));

        Solution instanse = new Solution();
        Solution.B instanseB = instanse.new B("name");
        oos.writeObject(instanseB);
        System.out.println( instanseB);
        //deser
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("C:\\k.txt"));
        B b = (B) ois.readObject();
        System.out.println(b);

    }
    public static class A {
        protected String name = "A";

        protected A(){}

        @Override
        public String toString() {
            return "A{" +
                    "name='" + name + '\'' +
                    '}';
        }

        public A(String name) {
            this.name += name;
        }

    }

    public class B extends A implements Serializable {
        private static final long serialVersionUID = 2L;
        public B(String name) {
            super(name);
            this.name += name;
        }
        private void readObject(ObjectInputStream s) throws IOException,
                ClassNotFoundException {
            s.defaultReadObject();

            this.name = (String) s.readObject();
        }

        private void writeObject(ObjectOutputStream s) throws IOException {
            s.defaultWriteObject();
            s.writeObject(this.name);

        }
    }
}

package com.javarush.test.level20.lesson10.home1;
import java.io.*;

/* Минимум изменений
Используя минимум изменений кода сделайте так, чтобы сериализация класса C стала возможной.
*/
public class Solution {

    public static class A  implements Serializable{
        String name = "A";

        public A(String name) {
            this.name += name;
        }

        @Override
        public String toString() {
            return name;
        }

        public A(){

        }
    }

    public static class B extends A {
        String name = "B";

        public B(String name) {
            super(name);
            this.name += name;
        }

        public B(){

        }
    }

    public static class C extends B {
        String name = "C";

        public C(String name) {
            super(name);
            this.name = name;
        }
    }

//    public static void main(String[] args) throws IOException,ClassNotFoundException {
//        FileOutputStream fileOutputStream = new FileOutputStream("C:\\Users\\panser\\Dropbox\\02.home\\IdeaProjects\\JavaRushHomeWork\\src\\com\\javarush\\test\\level20\\lesson10\\home01\\file1.dat");
//        ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
//        FileInputStream fileInputStream = new FileInputStream("C:\\Users\\panser\\Dropbox\\02.home\\IdeaProjects\\JavaRushHomeWork\\src\\com\\javarush\\test\\level20\\lesson10\\home01\\file1.dat");
//        ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
//
//        Solution solution = new Solution();
////        Solution.C c1 = solution.new C("first");
//        Solution.C c1 = new C("first");
//        objectOutputStream.writeObject(c1);
//        System.out.println(c1);
//
////        Solution.C c2 = solution.new C("second");
//        Solution.C c2 = new C("second");
//        c2 = ©objectInputStream.readObject();
//        System.out.println(c2);
  }

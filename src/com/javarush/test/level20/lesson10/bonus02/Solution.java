package com.javarush.test.level20.lesson10.bonus02;

/* Алгоритмы-прямоугольники
1. Дан двумерный массив N*N, который содержит несколько прямоугольников.
2. Различные прямоугольники не соприкасаются и не накладываются.
3. Внутри прямоугольник весь заполнен 1.
4. В массиве:
4.1) a[i, j] = 1, если элемент (i, j) принадлежит какому-либо прямоугольнику
4.2) a[i, j] = 0, в противном случае
5. getRectangleCount должен возвращать количество прямоугольников.
6. Метод main не участвует в тестировании
*/
public class Solution {
    public static void main(String[] args) {
        byte[][] a = new byte[][]{
                {1, 1, 0, 0},
                {1, 1, 0, 0},
                {1, 1, 0, 0},
                {1, 1, 0, 1}
        };
        int count = getRectangleCount(a);
        System.out.println("count = " + count + ". Должно быть 2");
    }

    public static int getRectangleCount(byte[][] a) {

        int N = a.length;

        int result = 0;

        for (int i = 0; i<N;i++ ){
            for (int j=0;j<N;j++){

                /*
                [i=0 j=0] [i=0 j=1] [i=0 j=2] [i=0 j=3]
                [i=1 j=0] [i=1 j=1] [i=1 j=2] [i=1 j=3]
                [i=2 j=0] [i=2 j=1] [i=2 j=2] [i=2 j=3]
                [i=3 j=0] [i=3 j=1] [i=3 j=2] [i=3 j=3]
                */
                //System.out.print("[i="+i + " j=" + j + "] ");

                //central square
                int centralX = j;
                int centralY = i;


                for (int endY = centralY; endY<N;endY++){
                    for(int endX = centralX; endX<N;endX++ ){

                        boolean wasFound = checkBrackets(a, centralX, centralY, endX, endY);
                        if (wasFound){
                            result++;
                        }

                    }
                }

            }
            //System.out.println();
        }



        return result;
    }

    private static boolean checkBrackets(byte[][] a, int centralX, int centralY, int endX, int endY) {

        //If all equals 1 proceed
        for (int m = centralY; m <= endY; m++) {
            for (int k = centralX; k <= endX; k++) {
                if (a[m][k] != 1) {
                    return false;
                }
            }
        }


        int left = centralX - 1;
        int up = centralY - 1;
        //int right = centralX + 1;
        int right = endX + 1;
        //int down = centralY + 1;
        int down = endY + 1;

        //check left
        boolean isLeftZero = false;
        for (int middleY = centralY; middleY <= endY; middleY++) {
            if (left < 0 || a[middleY][left] == 0) {
                isLeftZero = true;
            } else {
                isLeftZero = false;
                break;
            }
        }
        //check up
        boolean isUpZero = false;
        for (int middleX = centralX; middleX <= endX; middleX++) {
            if (up < 0 || a[up][middleX] == 0) {
                isUpZero = true;
            } else {
                isUpZero = false;
                break;
            }
        }
        //check right
        boolean isRightZero = false;
        for (int middleY = centralY; middleY <= endY; middleY++) {
            if (right >= a.length || a[middleY][right] == 0) {
                isRightZero = true;
            } else {
                isRightZero = false;
                break;
            }
        }
        //check down
        boolean isDownZero = false;
        for (int middleX = centralX; middleX <= endX; middleX++) {
            if (down >= a.length || a[down][middleX] == 0) {
                isDownZero = true;
            } else {
                isDownZero = false;
                break;
            }
        }

        //check left-up corner
        boolean isLeftUpCorner = false;
        if (left <0 || up <0 || a[up][left]==0){
            isLeftUpCorner = true;
        }

        //check left-down corner
        boolean isLeftDownCorner = false;
        if (left<0 || down >=a.length || a[down][left]==0){
            isLeftDownCorner = true;
        }

        //check right-up corner
        boolean isRightUpCorner = false;
        if (right >=a.length || up < 0 || a[up][right]==0){
            isRightUpCorner = true;
        }

        //check right-down corner
        boolean isRightDownCorner = false;
        if (right >=a.length || down >=a.length || a[down][right]==0){
            isRightDownCorner = true;
        }

        if (isLeftZero && isUpZero && isRightZero && isDownZero && isLeftUpCorner && isLeftDownCorner && isRightUpCorner && isRightDownCorner){
            return true;
        }

        return false;
    }
}

package com.javarush.test.level20.lesson10.home07;

import java.io.*;

/* Переопределение сериализации в потоке
Сериализация/десериализация Solution не работает.
Исправьте ошибки не меняя сигнатуры методов и класса.
Метод main не участвует в тестировании.
*/
public class Solution implements Serializable, AutoCloseable {
    static final long serialVersionUID = 123456789;
    transient private FileOutputStream stream;
    private String streamName;

    public Solution() {
    }

    public Solution(String fileName) throws FileNotFoundException {
        this.stream = new FileOutputStream(fileName);
        this.streamName = fileName;
    }



    public void writeObject(String string) throws IOException {
        stream.write(string.getBytes());
        stream.write("\n".getBytes());
        stream.flush();
    }

    private void writeObject(ObjectOutputStream out) throws IOException {
        out.defaultWriteObject();

        //out.close();
    }

    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        in.defaultReadObject();
        this.stream = new FileOutputStream(this.streamName,true);


        //in.close();
    }

    @Override
    public void close() throws Exception {
        System.out.println("Closing everything!");
        stream.close();
    }

    public static void main(String[] args) throws Exception {
        Solution solution = new Solution("C:\\t.txt");

        solution.writeObject("Nord");

        solution.close();

        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("C:\\l.txt"));
        oos.writeObject(solution);

        System.out.println(solution);


        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("C:\\l.txt"));
        Solution singleton = (Solution) ois.readObject();
        ois.close();

        System.out.println(singleton);

        singleton.writeObject("Kain");

        singleton.close();
    }
}

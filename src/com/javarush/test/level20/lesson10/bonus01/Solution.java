package com.javarush.test.level20.lesson10.bonus01;

import java.util.ArrayList;
import java.util.List;

/* Алгоритмы-числа
Число S состоит из M чисел, например, S=370 и M(количество цифр)=3
Реализовать логику метода getNumbers, который должен среди натуральных чисел меньше N (long)
находить все числа, удовлетворяющие следующему критерию:
число S равно сумме его цифр, возведенных в M степень
getNumbers должен возвращать все такие числа в порядке возрастания

Пример искомого числа:
370 = 3*3*3 + 7*7*7 + 0*0*0
8208 = 8*8*8*8 + 2*2*2*2 + 0*0*0*0 + 8*8*8*8

На выполнение дается 10 секунд и 50 МБ памяти.
*/
public class Solution {

   /* public static void main(String[] args) {

        long startTime = System.currentTimeMillis();

        int[] test = getNumbers(8209596);

        for (int i = 0; i < test.length; i++) {
            System.out.println(test[i]);
        }
        System.out.println("Work time=" + (System.currentTimeMillis() - startTime) / 1000 + " seconds");


//        int a = 9 % 10;
//        System.out.println(a);
        // getNumbers(100);
    }*/

    public static int[] getNumbers(int N) {

        int num = 1;
        int redNum = 10;
        List<Integer> finalArray = new ArrayList<>();
        int temp = 0;
        double nord[] = new double[10];
        for(int g = 1;g<10;g++){
            nord[g] = Math.pow(g,num);
        }
        for (int i = 1; i < N; i++) {

            if (i == redNum) {
                redNum = redNum * 10;
                num++;
                for(int g = 1;g<10;g++){
                    nord[g] = Math.pow(g,num);
                }
            }

            double resultint = 0;
            int numberInt = i;







            for (int k = 0; k < num; k++) {
                double a = numberInt % 10;
                //System.out.println(a);
                numberInt = numberInt / 10;


                resultint = resultint + nord[(int)a];
                ///resultint = resultint + Math.pow(a, num);
                //System.out.println(resultint);
            }
            temp = (int) resultint;
            if (i == temp) {
                finalArray.add(temp);
            }


        }


        int[] result = new int[finalArray.size()];
        for (int j = 0; j < finalArray.size(); j++) {
            result[j] = finalArray.get(j);
        }
        //System.out.println(finalArray);
        return result;
    }
}

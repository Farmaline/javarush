package com.javarush.test.level25.lesson05.home01;

/**
 * Created by Administrator on 12.06.2015.
 */
public class LoggingStateThread extends Thread {
    private Thread thread;

    public LoggingStateThread(Thread thread) {
        this.thread = thread;
        setDaemon(true);
    }

    @Override
    public void run() {

        System.out.println(thread.getState().toString());
        State state = thread.getState();


        while (thread.getState() != State.TERMINATED) {
            if (thread.getState() != state && thread.getState() != State.TERMINATED) {
                state = thread.getState();
                System.out.println(state.toString());
            }


        }
        System.out.println(thread.getState().toString());

    }
}

package com.javarush.test.level25.lesson07.home01;

public class TaskManipulator implements Runnable, CustomThreadManipulator {
    private String name;
    private Thread thread;

    @Override
    public void run() {
        try {

            Thread.sleep(0);
            System.out.println(name);

            while (!thread.isInterrupted()) {

                Thread.sleep(90);
             System.out.println(name);

            }
        } catch (InterruptedException e) {

        }


    }

    @Override
    public void start(String threadName) {
        name = threadName;
        thread = new Thread(this);
        thread.start();
    }

    @Override
    public void stop() {
        thread.interrupt();

    }
}

package com.javarush.test.level09.lesson11.home08;

import java.util.ArrayList;

/* Список из массивов чисел
Создать список, элементами которого будут массивы чисел. Добавить в список пять объектов–массивов длиной 5, 2, 4, 7, 0 соответственно. Заполнить массивы любыми данными и вывести их на экран.
*/

public class Solution
{
    public static void main(String[] args)
    {
        ArrayList<int[]> list = createList();
        printList(list);
    }

    public static ArrayList<int[]> createList()
    {
        //Написать тут ваш код
        ArrayList<int[]> bigList = new ArrayList<>();


        bigList.add(new int[]{1, 2, 3, 4, 5});
        bigList.add(new int[]{1, 2});
        bigList.add(new int[]{1, 2, 3, 4});
        bigList.add(new int[]{1, 2, 3, 4, 5, 6, 7});
        bigList.add(new int[]{});


        return bigList;
    }

    public static void printList(ArrayList<int[]> list)
    {
        for (int[] array : list)
        {
            for (int x : array)
            {
                System.out.println(x);
            }
        }
    }
}

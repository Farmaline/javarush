package com.javarush.test.level09.lesson11.home05;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;

/* Гласные и согласные буквы
Написать программу, которая вводит с клавиатуры строку текста.
Программа должна вывести на экран две строки:
1. первая строка содержит только гласные буквы
2. вторая - только согласные буквы и знаки препинания из введённой строки.
Буквы соединять пробелом.

Пример ввода:
Мама мыла раму.
Пример вывода:
а а ы а а у
М м м л р м .
*/

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        //Написать тут ваш код
        ArrayList<Character> first = new ArrayList<>();
        ArrayList<Character> second = new ArrayList<>();


        BufferedReader scan = new BufferedReader(new InputStreamReader(System.in));
        String mama = scan.readLine();
        int size = mama.length();

        for (int i = 0; i < size; i++)
        {

            if (mama.charAt(i) == ' ')
            {
                continue;

            }

            if (isVowel(mama.charAt(i)))
            {

                first.add(mama.charAt(i));


            } else
            {

                second.add(mama.charAt(i));
            }
        }
        Iterator<Character> iterator = first.iterator();
        Iterator<Character> iterator1 = second.iterator();
        while (iterator.hasNext())
        {
            System.out.print(iterator.next());
            if (iterator.hasNext()) System.out.print(" ");

        }
        System.out.println();
        while (iterator1.hasNext())
        {
            System.out.print(iterator1.next());
            if (iterator1.hasNext()) System.out.print(" ");

        }
    }


    public static char[] vowels = new char[]{'а', 'я', 'у', 'ю', 'и', 'ы', 'э', 'е', 'о', 'ё'};

    //метод проверяет, гласная ли буква
    public static boolean isVowel(char c)
    {
        c = Character.toLowerCase(c);  //приводим символ в нижний регистр - от заглавных к строчным буквам

        for (char d : vowels)   //ищем среди массива гласных
        {
            if (c == d)
                return true;
        }
        return false;
    }
}

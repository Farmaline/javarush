package com.javarush.test.level09.lesson11.home04;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/* Конвертер дат
Ввести с клавиатуры дату в формате «08/18/2013»
Вывести на экран эту дату в виде «AUG 18, 2013».
Воспользоваться объектом Date и SimpleDateFormat.
*/

public class Solution {

    public static void main(String[] args) throws Exception {
        //Напишите тут ваш код

        BufferedReader scan = new BufferedReader(new InputStreamReader(System.in));
        String dataToday = scan.readLine();


        SimpleDateFormat d1 = new SimpleDateFormat("MM/dd/yyyy");
        SimpleDateFormat s1 = new SimpleDateFormat("MMM dd, yyyy");
        Date today = d1.parse(dataToday);

        System.out.println(s1.format(today).toUpperCase());

    }
}

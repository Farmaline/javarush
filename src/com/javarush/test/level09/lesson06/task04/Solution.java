package com.javarush.test.level09.lesson06.task04;

import java.util.ArrayList;

/* Исключение при работе с коллекциями List
Перехватить исключение (и вывести его на экран), указав его тип, возникающее при выполнении кода:
ArrayList<String> list = new ArrayList<String>();
String s = list.get(18);
*/

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        //Напишите тут ваш код
        try
        {


            ArrayList<String> list = new ArrayList<String>();
            String s = list.get(18);

            //Напишите тут ваш код
        }catch (NullPointerException e1){

            System.out.println("NPE Exception is" + e1);
        }catch (ArithmeticException e2){

            System.out.println("HoL` Exception is" + e2);
        }catch (ArrayIndexOutOfBoundsException e3){

            System.out.println("array is" + e3);
        }catch (Exception e4){
            System.out.println("Exception is" + e4);
        }

    }
}

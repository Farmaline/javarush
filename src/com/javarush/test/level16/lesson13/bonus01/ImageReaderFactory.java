package com.javarush.test.level16.lesson13.bonus01;

import com.javarush.test.level16.lesson13.bonus01.common.*;

/**
 * Created by Administrator on 05.10.2014.
 */
public class ImageReaderFactory
{
   public static ImageReader getReader(ImageTypes imageTipe ){

       ImageReader nReader = null;

       if(imageTipe == ImageTypes.JPG )
       {
           nReader = new JpgReader();
       }
       if(imageTipe == ImageTypes.BMP )
       {
           nReader = new BmpReader();
       }
       if(imageTipe == ImageTypes.PNG )
       {
           nReader = new PngReader();
       }

       if (nReader == null)
       {

         throw   new IllegalArgumentException("Неизвестный тип картинки");

       }




        return nReader;
    }


}

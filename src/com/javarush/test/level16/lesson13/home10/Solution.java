package com.javarush.test.level16.lesson13.home10;

import java.io.*;

/* Последовательный вывод файлов
1. Разберись, что делает программа.
2. В статическом блоке считай 2 имени файла firstFileName и secondFileName.
3. Внутри класса Solution создай нить public static ReadFileThread, которая реализует
интерфейс ReadFileInterface (Подумай, что больше подходит - Thread или Runnable).
3.1. Метод setFileName должен устанавливать имя файла, из которого будет читаться содержимое.
3.2. Метод getFileContent должен возвращать содержимое файла.
3.3. В методе run считай содержимое файла, закрой поток. Раздели пробелом строки файла.
4. Подумай, в каком месте нужно подождать окончания работы нити, чтобы обеспечить последовательный вывод файлов.
4.1. Для этого добавь вызов соответствующего метода.
Ожидаемый вывод:
[все тело первого файла]
[все тело второго файла]
*/

public class Solution
{
    public static String firstFileName;
    public static String secondFileName;

    static
    {
        BufferedReader scan = new BufferedReader(new InputStreamReader(System.in));
        try
        {
            firstFileName = scan.readLine();
            secondFileName = scan.readLine();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws InterruptedException
    {
        systemOutPrintln(firstFileName);


        systemOutPrintln(secondFileName);
    }

    public static void systemOutPrintln(String fileName) throws InterruptedException
    {
        ReadFileInterface f = new ReadFileThread();
        f.setFileName(fileName);
        f.start();
        f.join();
        System.out.println(f.getFileContent());
    }

    public static interface ReadFileInterface
    {

        void setFileName(String fullFileName);

        String getFileContent();

        void join() throws InterruptedException;

        void start();
    }

    public static class ReadFileThread extends Thread implements ReadFileInterface
    {
        public File file = null;

        @Override
        public void setFileName(String fullFileName)
        {
            file = new File(fullFileName);

        }

        @Override
        public String getFileContent()
        {
            String fileContentString = "";
            FileReader fileReader = null;
            try
            {
                fileReader = new FileReader(file);
               // System.out.println(Thread.currentThread().getName() + " try to read " + file.getName());

                int ch;
                do{
                    ch=fileReader.read();
                    if(ch!=-1){
                        fileContentString += (char) ch;
                        //System.out.print(Thread.currentThread().getName().substring(Thread.currentThread().getName().length()-2) + " ");
                    }

                }while(ch!=-1);
                fileReader.close();

                /*while (fileReader.ready())
                {
                    fileContentString += (char) fileReader.read();
                    System.out.print(Thread.currentThread().getName().substring(Thread.currentThread().getName().length()-2) + " ");

                    try
                    {
                        Thread.sleep(600);
                    }
                    catch (InterruptedException e)
                    {
                        e.printStackTrace();
                    }

                }*/

            }
            catch (FileNotFoundException e)
            {
                e.printStackTrace();

            }
            catch (IOException e)
            {
            }/*finally
            {
                if (fileReader != null)
                {
                    try
                    {
                        fileReader.close();
                    }
                    catch (IOException e)
                    {
                        e.printStackTrace();
                    }
                }
            }*/


            return fileContentString;
        }

        @Override
        public void run()
        {

            getFileContent();


        }
    }
}

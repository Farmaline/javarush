package com.javarush.test.level08.lesson06.task04;

import java.util.*;

/* Измерить сколько времени занимает 10 тысяч вызовов get для каждого списка
Измерить, сколько времени занимает 10 тысяч вызовов get для каждого списка.
Метод getTimeMsOfGet  должен вернуть время его исполнения в миллисекундах.
*/

public class Solution
{
    public static void main(String[] args)
    {
        System.out.println("ArrayList ms="+getTimeMsOfGet(fill(new ArrayList())));
        System.out.println("LinkedList ms="+getTimeMsOfGet(fill(new LinkedList())));
    }

    private static List fill(List list)
    {
        for (int i = 0; i < 1110; i++)
        {
            list.add(new Object());
        }
        return list;
    }

    public static long getTimeMsOfGet(List list)
    {
        //Напишите тут ваш код
        Date timeBefaore = new Date();

        get10000(list);

        //Напишите тут ваш код
        Date timeAfter = new Date();

        long timeDifference = timeAfter.getTime() - timeBefaore.getTime();
        return timeDifference;

    }

    public static void get10000(List list)
    {
        if (list.isEmpty()) return;
        int x = list.size() / 2;

        for (int i = 0; i < 100000; i++)
        {
            list.add(new Object());
        }
    }
}

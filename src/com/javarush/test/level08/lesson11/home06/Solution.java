package com.javarush.test.level08.lesson11.home06;

/* Вся семья в сборе
1. Создай класс Human с полями имя (String), пол (boolean), возраст (int), дети (ArrayList<Human>).
2. Создай объекты и заполни их так, чтобы получилось: два дедушки, две бабушки, отец, мать, трое детей.
3. Вывести все объекты Human на экран.
*/

import java.util.ArrayList;

public class Solution
{
    public static void main(String[] args)
    {
        //Написать тут ваш код
        System.out.println("Hello world");
        Human baby1 = new Human("Alena", true, 2, new ArrayList<Human>());
        Human baby2 = new Human("Alena1", true, 2, new ArrayList<Human>());
        Human baby3 = new Human("Alena2", false, 2, new ArrayList<Human>());

        ArrayList<Human> children = new ArrayList<Human>();
        children.add(baby1);
        children.add(baby2);
        children.add(baby3);

        Human mama = new Human("Kolian", false, 60, children);
        Human batia = new Human("Umer", true, 75, children);

        ArrayList<Human> deti = new ArrayList<Human>();
        deti.add(mama);
        deti.add(batia);
        Human baba1 = new Human("babushka1", false, 12000, deti);
        Human baba2 = new Human("babushka2", false, 12001, deti);
        Human ded1 = new Human("ded1", true, 120094, deti);
        Human ded2 = new Human("ded2", true, 120093, deti);

        System.out.println(ded1);
        System.out.println(ded2);
        System.out.println(baba1);
        System.out.println(baba2);
        System.out.println(batia);
        System.out.println(mama);
        System.out.println(baby1);
        System.out.println(baby2);
        System.out.println(baby3);


    }

    public static class Human
    {
        //Написать тут ваш ко
        public Human(String name, boolean sex, int age, ArrayList<Human> children)
        {
            this.name = name;
            this.sex = sex;
            this.age = age;
            this.children = children;
        }

        public String name;
        public boolean sex;
        public int age;
        public ArrayList<Human> children = new ArrayList<Human>();


        public String toString()
        {
            String text = "";
            text += "Имя: " + this.name;
            text += ", пол: " + (this.sex ? "мужской" : "женский");
            text += ", возраст: " + this.age;

            int childCount = this.children.size();
            if (childCount > 0)
            {
                text += ", дети: " + this.children.get(0).name;

                for (int i = 1; i < childCount; i++)
                {
                    Human child = this.children.get(i);
                    text += ", " + child.name;
                }
            }

            return text;
        }
    }

}

package com.javarush.test.level08.lesson11.home05;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/* Мама Мыла Раму. Теперь с большой буквы
Написать программу, которая вводит с клавиатуры строку текста.
Программа заменяет в тексте первые буквы всех слов на заглавные.
Пример ввода:
мама     мыла раму.
Пример вывода:
Мама     Мыла Раму.
*/

public class Solution
{
    public static void main(String[] args) throws IOException
    {


        String inputString = "mama     mila ramu";
        Long startTime = System.currentTimeMillis();
        int N = 10000;
        for (int i = 0; i < N; i++)
        {
            foo1(inputString);
        }
        Long result1 = System.currentTimeMillis() - startTime;

        startTime = System.currentTimeMillis();
        for (int i = 0; i < N; i++)
        {
            foo2(inputString);
        }
        Long result2 = System.currentTimeMillis() - startTime;

        System.out.println("r1="+result1  + " r2=" + result2);



        /*BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Vvedite text");
        String s = reader.readLine();

        //Напишите тут ваш код
        */

    }

    public static String foo1(String s)
    {
        String[] masS = s.split("\\ ");
        for (int j = 0; j < masS.length; j++)
        {
            if (masS[j].isEmpty())
            {
                masS[j] = " ";
            } else
            {

                String firstCharacter = masS[j].substring(0, 1);
                firstCharacter = firstCharacter.toUpperCase();
                String otherCharacters = masS[j].substring(1);
                masS[j] = firstCharacter + otherCharacters;
            }

        }

        String result = "";
        for (int i = 0; i < masS.length; i++)
        {
            if (i == 0)
            {
                result += masS[i];


            } else
            {
                if (!masS[i].equals(" "))
                {
                    result += " ";
                }

                result += masS[i];

            }
        }

        return result;

    }


    public static String foo2(String s)
    {
        String result = "";
        //System.out.println("Input String=" + s);

        int n = s.length();
        String previous = " ";
        for (int i = 0; i < n; i++)
        {
            String symbol = s.substring(i, i + 1);
            //System.out.println("Checking symbol[" + i + "]=" + symbol);
            if (!symbol.equals(" "))
            {
                if (previous.equals(" "))
                {
                    symbol = symbol.toUpperCase();
                    //System.out.println("Changing to upperCase symbol[" + i + "]=" + symbol);
                }
            }
            result += symbol;
            previous = symbol;
        }

        return result;
    }


}

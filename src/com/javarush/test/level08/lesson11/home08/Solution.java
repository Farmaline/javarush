package com.javarush.test.level08.lesson11.home08;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/* Пять наибольших чисел
Создать массив на 20 чисел. Заполнить его числами с клавиатуры. Вывести пять наибольших чисел.
*/

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int[] array = new int[20];
        for (int i = 0; i < array.length; i++)
        {
            array[i] = Integer.parseInt(reader.readLine());
        }

        sort(array);


       /* for (int j = 0; j < array.length; j++)
        {
            System.out.println(array[j]);
        }*/


        System.out.println(array[0]);
        System.out.println(array[1]);
        System.out.println(array[2]);
        System.out.println(array[3]);
        System.out.println(array[4]);
    }

    public static void sort(int[] array)
    {
        //Напишите тут ваш код
        int num = array.length;

        /*for (int f = 0; f < array.length; f++, num--)
        {
            for (int i = 0; i < num - 1; i++)
            {
                if (array[i] < array[i + 1])
                {

                    int swop = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = swop;
                }


            }
        }*/

        for (int i = 0; i < 5; i++)
        {
            for (int j = i + 1; j < array.length - 1; j++)
            {
                if (array[i] < array[j])
                {
                    int swop = array[i];
                    array[i] = array[j];
                    array[j] = swop;
                }
            }
        }

    }
}


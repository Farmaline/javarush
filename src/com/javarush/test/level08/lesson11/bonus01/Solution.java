package com.javarush.test.level08.lesson11.bonus01;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.*;

/* Номер месяца
Программа вводит с клавиатуры имя месяца и выводит его номер на экран в виде: «May is 5 month».
*/

public class Solution
{
    public static void main(String[] args) throws IOException
    {
        //add your code here - напиши код тут

        BufferedReader scan = new BufferedReader(new InputStreamReader(System.in));

        String month1 = scan.readLine();
        Date month = new Date(month1 + " 1 1990");

        SimpleDateFormat d1 = new SimpleDateFormat("M");
        SimpleDateFormat d2 = new SimpleDateFormat("MMMMM");




        System.out.println(d2.format(month)+ " is " + d1.format(month) + " month");


    }

}

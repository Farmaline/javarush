package com.javarush.test.level08.lesson08.task05;

import java.util.*;

/* Удалить людей, имеющих одинаковые имена
Создать словарь (Map<String, String>) занести в него десять записей по принципу «фамилия» - «имя».
Удалить людей, имеющих одинаковые имена.
*/

public class Solution
{
    public static void main(String[] args)
    {
        System.out.println("Hello Nord");


        ArrayList<String> list2 = new ArrayList<String>();


        HashMap<String, String> baby = createMap();
        System.out.println("map size = " + baby.size());
        removeTheFirstNameDuplicates(baby);
    }

    public static HashMap<String, String> createMap()
    {
        //Напишите тут ваш код
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("Petin1", "Afanasiy9");
        map.put("Putin2", "Afanasiy9");
        map.put("Petin3", "Afanasiy9");
        map.put("Petin4", "Afanasiy4");
        map.put("Petin5", "Afanasiy5");
        map.put("Petin6", "Afanasiy5");
        map.put("Petin7", "Afanasiy6");
        map.put("Petin8", "Afanasiy7");
        map.put("Petin9", "Afanasiy5");
        map.put("Petin10", "Afanasiy9");
        return map;

    }

    public static void removeTheFirstNameDuplicates(HashMap<String, String> map)
    {

        Set<String> result = new HashSet<String>();

        //Напишите тут ваш код
        Iterator<Map.Entry<String, String>> iterator = map.entrySet().iterator();
        while (iterator.hasNext())
        {
            String name = iterator.next().getValue();
            if (result.contains(name))
            {
                System.out.println("Skipping " + name);
                continue;
            }
            boolean isSame = false;
            Iterator<Map.Entry<String, String>> second = map.entrySet().iterator();
            while (second.hasNext())
            {
                String otherName = second.next().getValue();
                System.out.println(name + " vs " + otherName);
                if (name.equals(otherName))
                {
                    if (isSame == false)
                    {
                        isSame = true;
                        continue;
                    }
                    if (result.contains(otherName))
                    {
                        continue;
                    } else
                    {
                        result.add(otherName);
                    }
                }
            }
        }

        System.out.println("////////////////////////////////");

        Iterator<String> resultIterator = result.iterator();
        while (resultIterator.hasNext())
        {
            String resultEntry = resultIterator.next();
            System.out.println("result=" + resultEntry);
            removeItemFromMapByValue(map, resultEntry);
        }

        System.out.println("TOTAL");
        Iterator<Map.Entry<String, String>> totalIterator = map.entrySet().iterator();
        while (totalIterator.hasNext())
        {
            Map.Entry<String, String> totalEntry = totalIterator.next();
            System.out.println("total=" + totalEntry);
        }
    }

    public static void removeItemFromMapByValue(HashMap<String, String> map, String value)
    {
        HashMap<String, String> copy = new HashMap<String, String>(map);
        for (Map.Entry<String, String> pair : copy.entrySet())
        {
            if (pair.getValue().equals(value))
                map.remove(pair.getKey());
        }
    }
}

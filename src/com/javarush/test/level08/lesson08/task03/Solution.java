package com.javarush.test.level08.lesson08.task03;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* Одинаковые имя и фамилия
Создать словарь (Map<String, String>) занести в него десять записей по принципу «Фамилия» - «Имя».
Проверить сколько людей имеют совпадающие с заданным имя или фамилию.
*/

public class Solution
{
    public static void main(String[] args)
    {

        HashMap<String, String> baby = createMap();
        System.out.println(getCountTheSameFirstName(baby, "Afanasiy"));
        System.out.println(getCountTheSameLastName(baby, "Petin1"));
    }

    public static HashMap<String, String> createMap()
    {
        //Напишите тут ваш код
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("Petin1", "Afanasiy");
        map.put("Putin2", "Afanasiy");
        map.put("Petin3", "Afanasiy");
        map.put("Petin4", "Afanasiy");
        map.put("Petin5", "Afanasiy");
        map.put("Petin6", "Afanasiy");
        map.put("Petin7", "Afanasiy");
        map.put("Petin8", "Afanasiy");
        map.put("Petin9", "Afanasiy");
        map.put("Petin10", "Afanasiy");
        return map;
    }

    public static int getCountTheSameFirstName(HashMap<String, String> map, String name)
    {


        //Напишите тут ваш код

        int count = 0;
        Iterator<Map.Entry<String, String>> iterator = map.entrySet().iterator();
        while (iterator.hasNext())
        {
            Map.Entry<String, String> devil = iterator.next();
            //System.out.println(devil.getValue());
            if (devil.getValue().equals(name))
            {
                count++;
            }

        }
        return count;
    }

    public static int getCountTheSameLastName(HashMap<String, String> map, String familiya)
    {
/*
        if (map.containsKey(familiya))
        {
            return 1;
        } else
        {
            return 0;
        }*/

        //Напишите тут ваш код
        int count = 0;
        Iterator<Map.Entry<String, String>> iterator = map.entrySet().iterator();
        while (iterator.hasNext())
        {
            Map.Entry<String, String> devil = iterator.next();
            if (devil.getKey().equals(familiya))
            {
                count++;
            }

        }
        return count;

    }
}

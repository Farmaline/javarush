package com.javarush.test.level08.lesson08.task04;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* Удалить всех людей, родившихся летом
Создать словарь (Map<String, Date>) и занести в него десять записей по принципу: «фамилия» - «дата рождения».
Удалить из словаря всех людей, родившихся летом.
*/

public class Solution
{

    public static void main(String[] args)
    {
        System.out.println("Hello world");


        Date doday = new Date();
        SimpleDateFormat dt1 = new SimpleDateFormat("MM");
        System.out.println(dt1.format(doday));

        HashMap<String, Date> supMap = createMap();
        System.out.println(supMap.size());

        removeAllSummerPeople(supMap);

        System.out.println(supMap.size());

    }

    public static HashMap<String, Date> createMap()
    {
        HashMap<String, Date> map = new HashMap<String, Date>();

        map.put("Сталлоне1", new Date("JUNE 1 1980"));
        map.put("Сталлоне2", new Date("JUNE 1 1980"));
        map.put("Сталлоне3", new Date("MARCH 1 1980"));
        map.put("Сталлоне4", new Date("MARCH 1 1980"));
        map.put("Сталлоне5", new Date("May 1 1980"));
        map.put("Сталлоне6", new Date("MARCH 1 1980"));
        map.put("Сталлоне7", new Date("July 1 1980"));
        map.put("Сталлоне8", new Date("August 1 1980"));
        map.put("Сталлоне9", new Date("September 1 1980"));
        map.put("Сталлоне10", new Date("JANUARY 1 1980"));


        //Напишите тут ваш код
        return map;

    }

    public static void removeAllSummerPeople(HashMap<String, Date> map)
    {
        HashMap<String, Date> clone = (HashMap<String, Date>) map.clone();
        //Напишите тут ваш код
        for (Map.Entry<String, Date> pair : clone.entrySet())
        {
            SimpleDateFormat dateFormat = new SimpleDateFormat("MM");
            Date doday = pair.getValue();
            int month = Integer.parseInt(dateFormat.format(doday));

            if (month > 5 && month < 9)
            {

                System.out.println(pair.getValue());
                map.remove(pair.getKey());
            }



        }

    }
}

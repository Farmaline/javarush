package com.javarush.test.level08.lesson08.task02;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/* Удалить все числа больше 10
Создать множество чисел(Set<Integer>), занести туда 20 различных чисел.
Удалить из множества все числа больше 10.
*/

public class Solution
{
    public static HashSet<Integer> createSet()
    {
        HashSet<Integer> setInt = new HashSet<Integer>();
        //Напишите тут ваш код
        for (int i = 0; i < 20; i++)
        {

            setInt.add(i);
        }

        return setInt;

    }

    public static HashSet<Integer> removeAllNumbersMoreThen10(HashSet<Integer> set)
    {
        //Напишите тут ваш код
        Iterator<Integer> it = set.iterator();
        while (it.hasNext())
        {
            Integer element = it.next();
            if (element > 10)
            {

                it.remove();
            }


        }

        return set;


    }
}

package com.javarush.test.level07.lesson12.home06;

/* Семья
Создай класс Human с полями имя(String), пол(boolean),возраст(int), отец(Human), мать(Human). Создай объекты и заполни их так, чтобы получилось: Два дедушки, две бабушки, отец, мать, трое детей. Вывести объекты на экран.
Примечание:
Если написать свой метод String toString() в классе Human, то именно он будет использоваться при выводе объекта на экран.
Пример вывода:
Имя: Аня, пол: женский, возраст: 21, отец: Павел, мать: Катя
Имя: Катя, пол: женский, возраст: 55
Имя: Игорь, пол: мужской, возраст: 2, отец: Михаил, мать: Аня
…
*/

public class Solution
{
    public static void main(String[] args)
    {
        //Написать тут ваш код

        Human baba1 = new Human("Ромашка",false, 90, null, null);
        Human baba2 = new Human("Какашка",false, 90, null, null);
        Human ded1 = new Human("Пердун",true, 90, null, null);
        Human ded2 = new Human("Сирун",true, 90, null, null);
        Human pavel = new Human("Павел",true, 40, ded1, baba1);
        Human katia = new Human("Катя",false, 55,ded2, baba2);
        Human anya = new Human("Аня", false, 21, pavel,katia);
        Human igor = new Human("Игорь", true, 2, pavel,katia);
        Human kondrar = new Human("Кондрат", true, 5, pavel,katia);



        System.out.println(anya);
        System.out.println(katia);
        System.out.println(igor);
        System.out.println(baba1);
        System.out.println(baba2);
        System.out.println(ded1);
        System.out.println(ded2);
        System.out.println(pavel);
        System.out.println(kondrar);

    }

    public static class Human
    {
        //Написать тут ваш код
        private String name;
        private  boolean sex;
        private int age;
        private Human father;
        private Human mother;

        Human (String name, boolean sex,int age, Human father, Human mother ){

            this.name = name;
            this.sex = sex;
            this.age = age;
            this.father = father;
            this.mother = mother;




        }

        public String toString()
        {
            String text = "";
            text += "Имя: " + this.name;
            text += ", пол: " + (this.sex ? "мужской" : "женский");
            text += ", возраст: " + this.age;

            if (this.father != null)
                text += ", отец: " + this.father.name;

            if (this.mother != null)
                text += ", мать: " + this.mother.name;

            return text;
        }
    }

}

package com.javarush.test.level07.lesson09.task01;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tpe on 6/3/2014.
 */
public class NoR
{


    public static void main(String args[]){

        System.out.println("hello world");

        int N = 10;
        List<Integer> input = new ArrayList<Integer>(N);
        for(int i =-N;i<N;i++){
            input.add(i);
        }

        String kainResult = getKainResult(input);
        String nordResult = getNoRDResult(input);

        System.out.println(kainResult);
        System.out.println(nordResult);

        System.out.println("RESULT="+kainResult.equals(nordResult));

    }


    private static String getKainResult(List<Integer> input){
        ArrayList<Integer> test1 = new ArrayList<Integer>();
        ArrayList<Integer> test2 = new ArrayList<Integer>();
        ArrayList<Integer> test3 = new ArrayList<Integer>();

        for (int x : input)
        {
            if (x % 3 == 0)
            {
                test1.add(x);
            }
            if (x % 2 == 0)
            {
                test2.add(x);
            } else
            {
                test3.add(x);
            }
        }

        return test1 + " " + test2 + " " + test3;
    }
    private static String getNoRDResult(List<Integer> input){

        ArrayList<Integer> even = new ArrayList<Integer>();
        ArrayList<Integer> multipleOf3 = new ArrayList<Integer>();
        ArrayList<Integer> others = new ArrayList<Integer>();

        for (int x : input)
        {
            if (x%3!=0 & x%2!=0){
                others.add(x);
                continue;
            }

            if (x % 3 == 0)
            {
                multipleOf3.add(x);
            }
            if (x % 2 == 0)
            {
                even.add(x);
            }
        }

        return multipleOf3 + " " + even + " " + others;
    }



}

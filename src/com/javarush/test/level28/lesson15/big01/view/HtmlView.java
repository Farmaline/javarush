package com.javarush.test.level28.lesson15.big01.view;

import com.javarush.test.level28.lesson15.big01.Controller;
import com.javarush.test.level28.lesson15.big01.vo.Vacancy;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.*;
import java.util.List;

/**
 * Created by Administrator on 01.10.2015.
 */


public class HtmlView implements View {
    private final String filePath = "./src/" + this.getClass().getPackage().getName().replace(".", "/") + "/vacancies.html";


    private Controller controller;

    protected Document getDocument() throws IOException {
        return Jsoup.parse(filePath, "UTF-8");
    }

    @Override
    public void update(List<Vacancy> vacancies) {
        try {
            String newVacanciesFileHtml = getUpdatedFileContent(vacancies);
            updateFile(newVacanciesFileHtml);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Some exception occurred");
        }

    }

    @Override
    public void setController(Controller controller) {
        this.controller = controller;
    }

    public void userCitySelectEmulationMethod() {
        controller.onCitySelect("Odessa");
    }

    private String getUpdatedFileContent(List<Vacancy> vacancies) throws IOException {

        Document doc = getDocument();
        Element element = doc.getElementsByClass("template").first();
        Element cloneElement = element.clone();
        cloneElement.removeClass("template").removeAttr("style");
        doc.getElementsByAttributeValue("class","vacancy").remove();
        for (Vacancy vacancy : vacancies)
        {
            Element vac = cloneElement.clone();
            vac.getElementsByAttributeValue("class", "city").get(0).text(vacancy.getCity());
            vac.getElementsByAttributeValue("class", "companyName").get(0).text(vacancy.getCompanyName());
            vac.getElementsByAttributeValue("class", "salary").get(0).text(vacancy.getSalary());
            Element link = vac.getElementsByTag("a").get(0);
            link.text(vacancy.getTitle());
            link.attr("href", vacancy.getUrl());
            element.before(vac.outerHtml());
        }
        return doc.html();
    }

    private void updateFile(String newData) throws FileNotFoundException, UnsupportedEncodingException {

        PrintWriter pw = new PrintWriter(new OutputStreamWriter(new FileOutputStream(filePath), "UTF-8"));
        pw.write(newData);
        pw.close();
    }
}

package com.javarush.test.level28.lesson06.home01;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Administrator on 24.09.2015.
 */
public class MyThread extends Thread {
    public static AtomicInteger priorityOfThread = new AtomicInteger(1);

    public MyThread(ThreadGroup group, Runnable target, String name, long stackSize) {
        super(group, target, name, stackSize);
        setPriority(givePriority());

    }

    public MyThread() {
        setPriority(givePriority());
    }

    public MyThread(Runnable target) {
        super(target);
        setPriority(givePriority());
    }

    public MyThread(ThreadGroup group, Runnable target) {
        super(group, target);
        setPriority(givePriority());
    }

    public MyThread(String name) {
        super(name);
        setPriority(givePriority());
    }

    public MyThread(ThreadGroup group, String name) {
        super(group, name);
        setPriority(givePriority());
    }

    public MyThread(Runnable target, String name) {
        super(target, name);
        setPriority(givePriority());
    }

    public MyThread(ThreadGroup group, Runnable target, String name) {
        super(group, target, name);
        setPriority(givePriority());
    }

    public synchronized int givePriority() {
        int i;
        if (priorityOfThread.intValue() == 11) {
            priorityOfThread.set(1);
            i = priorityOfThread.getAndIncrement();
        } else {
            i = priorityOfThread.getAndIncrement();
        }
        return i;
    }
}

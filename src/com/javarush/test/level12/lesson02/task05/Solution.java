package com.javarush.test.level12.lesson02.task05;

/* Или «Корова», или «Кит», или «Собака», или «Неизвестное животное»
Написать метод, который определяет, объект какого класса ему передали, и возвращает результат – одно значение из: «Корова», «Кит», «Собака», «Неизвестное животное».
*/


public class Solution
{
    public static void main(String[] args)
    {
        System.out.println(getObjectType(new Cow()));
        System.out.println(getObjectType(new Dog()));
        System.out.println(getObjectType(new Whale()));
        System.out.println(getObjectType(new Pig()));
    }

    public static String getObjectType(Object o)
    {

        System.out.println(o instanceof Dog);
        if (o instanceof Dog){
            return "Собака";
        }if (o instanceof Cow){
            return "korova";
        }if (o instanceof Whale){
            return "kit";
        }
        return "unknown";

        //Напишите тут ваше решение
        /*if (o.getClass() == new Dog().getClass())
        {
            return "Собака";
        }
        if (o.getClass() == new Cow().getClass())
        {
            return "Корова";
        }
        if (o.getClass() == new Whale().getClass())
        {
            return "Кит";
        } else
        {
            return "Неизвестное животное";
        }*/
    }

    public static class Cow
    {
    }

    public static class Dog
    {
    }

    public static class Whale
    {
    }

    public static class Pig
    {
    }
}

package com.javarush.test.level32.lesson15.big01.actions;

import javax.swing.*;
import javax.swing.text.*;
import java.awt.event.ActionEvent;

/**
 * Created by Administrator on 16.12.2015.
 */

/* /*12.1.	Напиши реализацию класса RedoAction:
12.1.1.	Добавь в класс поле View. Добавь его инициализацию в конструкторе.
12.1.2.	Реализуй метод actionPerformed(ActionEvent actionEvent), он должен вызывать метод
redo() у представления.
12.2.	Напиши реализацию класса UndoAction по аналогии с RedoAction.
12.3.	Изучи реализацию класса StrikeThroughAction, которую ты получил вместе с заданием и
реализуй аналогичным образом классы:
12.3.1.	SubscriptAction
12.3.2.	SuperscriptAction
Запусти программу и убедись, что пункты меню Подстрочный знак, Надстрочный знак и
Зачеркнутый работают. Пункты, отвечающие за отмену и возврат действия пока не
подключены к контроллеру и мы не сможем их проверить.*/
public class SubscriptAction extends StyledEditorKit.StyledTextAction {

    public SubscriptAction() {
        super(StyleConstants.Subscript.toString());
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JEditorPane editor = getEditor(e);
        if (editor != null) {
            MutableAttributeSet mutableAttributeSet = getStyledEditorKit(editor).getInputAttributes();
            SimpleAttributeSet simpleAttributeSet = new SimpleAttributeSet();
            StyleConstants.setSubscript(simpleAttributeSet, !StyleConstants.isSubscript(mutableAttributeSet));
            setCharacterAttributes(editor, simpleAttributeSet, false);
        }
    }
}

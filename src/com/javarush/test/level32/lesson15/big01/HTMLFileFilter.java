package com.javarush.test.level32.lesson15.big01;

import javax.swing.filechooser.FileFilter;
import java.io.File;

/**
 * Created by Administrator on 23.12.2015.
 */

public class HTMLFileFilter extends FileFilter {
    @Override
    public boolean accept(File f) {
        boolean isAccept = false;
        String fileName = f.getName().toLowerCase();
        if (fileName.endsWith(".html")|| fileName.endsWith(".htm")){
            isAccept = true;
        }
        if (f.isDirectory()){
            isAccept = true;
        }
        return isAccept;
    }

    @Override
    public String getDescription() {
        return "HTML и HTM файлы";
    }
}

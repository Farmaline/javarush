package com.javarush.test.level32.lesson06.task01;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Random;

/* Генератор паролей
Реализуйте логику метода getPassword, который должен возвращать ByteArrayOutputStream, в котором будут байты пароля.
Требования к паролю:
1) 8 символов
2) только цифры и латинские буквы разного регистра
3) обязательно должны присутствовать цифры, и буквы разного регистра
Все сгенерированные пароли должны быть уникальные.
Пример правильного пароля:
wMh7SmNu
*/
public class Solution {
    public static void main(String[] args) {
        ByteArrayOutputStream password = getPassword();
        System.out.println(password.toString());
    }

    public static ByteArrayOutputStream getPassword() {
        Random r = new Random();
        String alphabet = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        char number = alphabet.substring(0, 10).charAt(r.nextInt(10));
        char bigLetter = alphabet.substring(10, 36).charAt(r.nextInt(26));
        char littleLetter = alphabet.substring(36, alphabet.length()).charAt(r.nextInt(26));

        char[] chars = new char[8];

        int indexA = 0;
        int indexB = 0;
        int indexC = 0;

        while (indexA==indexB | indexA==indexC | indexB==indexC){
            indexA =r.nextInt(8);
            indexB =r.nextInt(8);
            indexC =r.nextInt(8);
        }
        chars[indexA] = number;
        chars[indexB] = bigLetter;
        chars[indexC] = littleLetter;

        for (int i = 0; i < 8; i++) {
            if ((int)chars[i]==0)
                chars[i] = alphabet.charAt(r.nextInt(alphabet.length()));
        }
        String result = new String(chars);
        try {
            byteArrayOutputStream.write(result.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return byteArrayOutputStream;
    }
}

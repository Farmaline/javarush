package com.javarush.test.level32.lesson02.task01;

import java.io.IOException;
import java.io.RandomAccessFile;

/* Запись в файл
В метод main приходят три параметра:
1) fileName - путь к файлу
2) number - число, позиция в файле
3) text - текст
Записать text в файл fileName начиная с позиции number.
Если файл слишком короткий, то записать в конец файла.
*/
public class Solution {
    public static void main(String... args) {

        fileWriterOnNubner(args[0],args[1],args[2]);
    }

    public static void fileWriterOnNubner (String fileName, String number, String someText){
        int intNumber = Integer.parseInt(number);
        try {
            RandomAccessFile randomAccessFile = new RandomAccessFile(fileName,"rw");
            if (randomAccessFile.length() < intNumber){
                randomAccessFile.seek(randomAccessFile.length());
                randomAccessFile.writeBytes(someText);
            }else {
                randomAccessFile.seek(intNumber);
                randomAccessFile.writeBytes(someText);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}

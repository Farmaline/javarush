package com.javarush.test.level05.lesson05.task05;

/* Провести три боя  попарно между котами
Создать три кота используя класс Cat.
Провести три боя попарно между котами.
Класс Cat создавать не надо. Для боя использовать метод boolean fight(Cat anotherCat).
Результат каждого боя вывести на экран.
*/

import com.sun.org.apache.xpath.internal.SourceTree;

public class Solution {
    public static void main(String[] args) {
        //add your code here
        Cat cat1 = new Cat("Vaska", 14, 20, 10);
        Cat cat2 = new Cat("Murzik",5, 18, 15);
        Cat cat3 = new Cat("Killer", 10, 25, 30);

        boolean f1 = cat1.fight(cat2);
        System.out.println(f1);
        boolean f2 = cat1.fight(cat3);
        System.out.println(f2);
        boolean f3 = cat2.fight(cat3);
        System.out.println(f3);
    }

    public static class Cat {

        public static int count = 0;
        public static int fightCount = 0;

        protected String name;
        protected int age;
        protected int weight;
        protected int strength;

        public Cat(String name, int age, int weight, int strength) {
            count++;

            this.name = name;
            this.age = age;
            this.weight = weight;
            this.strength = strength;
        }

        public boolean fight(Cat anotherCat) {
            fightCount++;

            int agePlus = this.age > anotherCat.age ? 1 : 0;
            int weightPlus = this.weight > anotherCat.weight ? 1 : 0;
            int strengthPlus = this.strength > anotherCat.strength ? 1 : 0;

            int score = agePlus + weightPlus + strengthPlus;
            return score > 2; //эквивалентно return score > 2 ? true : false;
        }
    }
}

package com.javarush.test.level05.lesson05.task02;

/**
 * Created by tpe on 5/23/2014.
 */
public class Main
{




    public static void main(String args[]){
        System.out.println("hello");

        Cat cat1 = new Cat();
        cat1.name = "Myrzik";
        cat1.age = 500;
        cat1.weight = 500;
        cat1.strength = 500;

        Cat cat2 = new Cat();
        cat2.name = "Vasyka";
        cat2.age = 100;
        cat2.weight = 100;
        cat2.strength = 100;
        //cat1.fight(cat2) = true;
        //cat2.fight(cat1) = false;

        Cat cat3 = new Cat();
        cat3.name = "Bars";
        cat3.age = 2;
        cat3.weight = 3;
        cat3.strength = 30000;

        boolean r1 = cat1.fight(cat2);  // = true;
        boolean r2 = cat2.fight(cat1); //  = false;


        System.out.println("r1="+r1+ " r2="+ r2);

    }
}

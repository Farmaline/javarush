package com.javarush.test.level05.lesson05.task02;

/* Реализовать метод fight
Реализовать метод boolean fight(Cat anotherCat):
реализовать механизм драки котов в зависимости от их веса, возраста и силы.
Зависимость придумать самому. Метод должен определять, выиграли ли мы (this) бой или нет,
т.е. возвращать true, если выиграли и false - если нет.
Должно выполняться условие:
если cat1.fight(cat2) = true , то cat2.fight(cat1) = false
*/

public class Cat
{
    public String name;
    public int age;
    public int weight;
    public int strength;

    public boolean fight(Cat anotherCat)
    {
        System.out.print("Start fighting cat=" + name + " age=" + age + " weight=" + weight + " strength=" + strength);
        System.out.println(" VS anotherCat=" + anotherCat.name + " age=" + anotherCat.age + " weight=" + anotherCat.weight + " strength=" + anotherCat.strength);
        //Напишите тут ваш код

        int sMy = age + weight + strength;
        int sAnother = anotherCat.age + anotherCat.weight + anotherCat.strength;

        System.out.print("Damage of " + name + "=" + sMy);
        System.out.println(" Damage of " + anotherCat.name + "=" + sAnother);

        if (sMy > sAnother)
        {
            return true;
        } else
        {
            return false;
        }
    }
}


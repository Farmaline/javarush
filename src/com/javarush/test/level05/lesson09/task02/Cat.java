package com.javarush.test.level05.lesson09.task02;

/* Создать класс Cat
Создать класс Cat (кот) с пятью конструкторами:
- Имя,
- Имя, вес, возраст
- Имя, возраст (вес стандартный)
- вес, цвет, (имя, адрес и возраст – неизвестные. Кот - бездомный)
- вес, цвет, адрес ( чужой домашний кот)
Задача инициализатора – сделать объект валидным. Например, если вес не известен, то нужно указать какой-нибудь средний вес. Кот не может ничего не весить. То же касательно возраста. А вот имени может и не быть (null). То же касается адреса: null.
*/

public class Cat
{
    int count = 0;
    public String adress;
    public int sage = 4;
    public int sweight = 4;
    public String name;
    public int age = 4;
    public int weight = sweight;
    public String color;


    public Cat(String name)
    {
        this.name = name;
        count++;
    }

    public Cat(String name, int age, int weight)
    {
        count++;
        this.name = name;
        this.age = age;
        this.weight = weight;
        sweight = (sweight + weight) / 2;

    }

    public Cat(String name, int age)
    {
        count++;
        this.name = name;
        this.age = age;
        this.weight = sweight;
    }

    public Cat(int weight, String color)
    {
        count++;
        this.color = color;
        this.weight = weight;
        sweight = (sweight + weight) / 2;


    }

    public Cat(int weight, String color, String adress)
    {
        this.weight = weight;
        this.color = color;
        this.adress = adress;
    }

}

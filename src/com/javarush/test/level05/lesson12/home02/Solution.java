package com.javarush.test.level05.lesson12.home02;

/* Man and Woman
1. Внутри класса Solution создай public static классы Man и Woman.
2. У классов должны быть поля: name(String), age(int), address(String).
3. Создай конструкторы, в которые передаются все возможные параметры.
4. Создай по два объекта каждого класса со всеми данными используя конструктор.
5. Объекты выведи на экран в таком формате [name + " " + age + " " + address].
*/

public class Solution
{
    public static void main(String[] args)
    {
        // Создай по два объекта каждого класса тут
        Man man1 = new Man("Andrey", 24, "Yuzhny Kh");
        System.out.println(man1.toString());
        Woman woman1 = new Woman("Shama");
        System.out.println(woman1.toString());

        // Выведи их на экран тут
    }

    public static class Man
    {
        @Override
        public String toString()
        {
            return "[" +
                    name + " " +
                    + age +
                    " " + address +
                    "]";
        }

        String name;
        int age;
        String address;

        public Man(String name)
        {
            this.name = name;
        }

        public Man(String name, int age)
        {
            this.name = name;
            this.age = age;
        }

        public Man(String name, int age, String address)
        {
            this.name = name;
            this.age = age;
            this.address = address;
        }


    }

    public static class Woman
    {
        @Override
        public String toString()
        {
            return "[" +
                    name + " " +
                    + age +
                    " " + address +
                    "]";
        }

        String name;
        int age;
        String address;

        public Woman(String name)
        {
            this.name = name;
        }

        public Woman(String name, int age)
        {
            this.name = name;
            this.age = age;
        }

        public Woman(String name, int age, String address)
        {
            this.name = name;
            this.age = age;
            this.address = address;
        }

    }

}

package com.javarush.test.level05.lesson12.bonus02;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/* Нужно добавить в программу новую функциональность
Задача: Программа вводит два числа с клавиатуры и выводит минимальное из них на экран.
Новая задача: Программа вводит пять чисел с клавиатуры и выводит минимальное из них на экран.
*/

public class Solution
{

    public static void main(String[] args) throws Exception
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int a = Integer.parseInt(reader.readLine());
        int b = Integer.parseInt(reader.readLine());
        int c = Integer.parseInt(reader.readLine());
        int d = Integer.parseInt(reader.readLine());
        int e = Integer.parseInt(reader.readLine());
        int[] mass = {a, b, c, d, e};

        //int minimum = min(a, b);

        int minimum = getMin(mass);
        //System.out.println(minimum);
        System.out.println("Minimum = " + minimum);
    }

    public static int getMin(int[] arr)
    {
        int s = arr[0];
        for (int i = 1; i < 5; i++)
        {
            s = min(s,arr[i]);
            /*if (s > arr[i])
            {
                s = arr[i];
            }*/


        }

        return s;
    }


    public static int min(int a, int b)
    {
        return a < b ? a : b;
    }

}

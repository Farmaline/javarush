package com.javarush.test.level05.lesson12.home05;

/* Вводить с клавиатуры числа и считать их сумму
Вводить с клавиатуры числа и считать их сумму, пока пользователь не введёт слово «сумма». Вывести на экран полученную сумму.
*/

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Date;

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        BufferedReader scan = new BufferedReader(new InputStreamReader(System.in));
        int g =0;
        while (true)
        {

            String s = scan.readLine();
            if (s.equals("сумма"))
            {
                System.out.println(g);
                break;
            } else
            {
                int d = Integer.parseInt(s);

                g = g + d;
            }


        }
        //Напишите тут ваш код
    }
}

package com.javarush.test.level05.lesson07.task02;

/* Создать класс Cat
Создать класс Cat (кот) с пятью инициализаторами:
- Имя,
- Имя, вес, возраст
- Имя, возраст (вес стандартный)
- вес, цвет, (имя, адрес и возраст неизвестны, это бездомный кот)
- вес, цвет, адрес ( чужой домашний кот)
Задача инициализатора – сделать объект валидным. Например, если вес неизвестен, то нужно указать какой-нибудь средний вес. Кот не может ничего не весить. То же касательно возраста. А вот имени может и не быть (null). То же касается адреса: null.
*/

public class Cat
{
    int count = 0;
    public String adress;
    public int sage = 4;
    public int sweight = 4;
    public String name;
    public int age = 4;
    public int weight = sweight;
    public String color;


    public void initialize(String name)
    {
        this.name = name;
        count++;
    }

    public void initialize(String name, int age, int weight)
    {
        count++;
        this.name = name;
        this.age = age;
        this.weight = weight;
        sweight = (sweight + weight) / 2;

    }

    public void initialize(String name, int age)
    {
        count++;
        this.name = name;
        this.age = age;
        this.weight = sweight;
    }

    public void initialize(int weight, String color)
    {
        count++;
        this.color = color;
        this.weight = weight;
        sweight = (sweight + weight)/2;


    }
    public void initialize (int weight, String color, String adress){
        this.weight = weight;
        this.color = color;
        this.adress = adress;
    }

    @Override
    public String toString()
    {
        return "Cat{" +
                "count=" + count +
                ", adress='" + adress + '\'' +
                ", sage=" + sage +
                ", sweight=" + sweight +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", weight=" + weight +
                ", color='" + color + '\'' +
                '}';
    }
}

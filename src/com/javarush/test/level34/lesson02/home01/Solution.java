package com.javarush.test.level34.lesson02.home01;

/* Рекурсия для мат.выражения
На вход подается строка - математическое выражение.
Выражение включает целые и дробные числа, скобки (), пробелы, знак отрицания -, возведение в степень ^, sin(x), cos(x), tan(x)
Для sin(x), cos(x), tan(x) выражение внутри скобок считать градусами, например, cos(3 + 19*3)=0.5
Степень задается так: a^(1+3) и так a^4, что эквивалентно a*a*a*a.
С помощью рекурсии вычислить выражение и количество математических операций. Вывести через пробел результат в консоль.
Результат выводить с точностью до двух знаков, для 0.33333 вывести 0.33, использовать стандартный принцип округления.
Не создавайте статические переменные и поля класса.
Не пишите косвенную рекурсию.
Пример, состоящий из операций sin * - + * +:
sin(2*(-5+1.5*4)+28)
Результат:
0.5 6
*/
public class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();
        //solution.recursion("sin(2*(-5+1.5*4)+28)", 0); //expected output 0.5 6
        //solution.recursion("-5+1.5*4", 0); //expected output 0.5 6

        String s = "sin(2*(-5+1.5*4)+28)";
        System.out.print(s + " expected output 0.5 6 actually ");
        solution.recursion(s, 0);
/*
        s = "tan(45)";
        System.out.print(s + " expected output 1 1 actually ");
        solution.recursion(s, 0);

        s = "0.305";
        System.out.print(s + " expected output 0.3 0 actually ");
        solution.recursion(s, 0);

        s = "0.3051";
        System.out.print(s + " expected output 0.31 0 actually ");
        solution.recursion(s, 0);

        s = "1+(1+(1+1)*(1+1))*(1+1)+1";
        System.out.print(s + " expected output 12 8 actually ");
        solution.recursion(s, 0);

        s = "tan(44+sin(89-cos(180)^2))";
        System.out.print(s + " expected output 1 6 actually ");
        solution.recursion(s, 0);

        s = "-2+(-2+(-2)-2*(2+2))";
        System.out.print(s + " expected output -14 8 actually ");
        solution.recursion(s, 0);*/

        s = "sin(80+(2+(1+1))*(1+1)+2)";
        System.out.print(s + " expected output 1 7 actually ");
        solution.recursion(s, 0);

        s = "1+4/2/2+2^2+2*2-2^(2-1+1)";
        System.out.print(s + " expected output 6 11 actually ");
        solution.recursion(s, 0);

        s = "2^10+2^(5+5)";
        System.out.print(s + " expected output 2048 4 actually ");
        solution.recursion(s, 0);

        s = "1.01+(2.02-1+1/0.5*1.02)/0.1+0.25+41.1";
        System.out.print(s + " expected output 72.96 8 actually ");
        solution.recursion(s, 0);

        s = "0.000025+0.000012";
        System.out.print(s + " expected output 0 1 actually ");
        solution.recursion(s, 0);

        s = "-2-(-2-1-(-2)-(-2)-(-2-2-(-2)-2)-2-2)";
        System.out.print(s + " expected output -3 16 actually ");
        solution.recursion(s, 0);

        s = "cos(3 + 19*3)";
        System.out.print(s + " expected output 0.5 3 actually ");
        solution.recursion(s, 0);

        s = "2*(589+((2454*0.1548/0.01*(-2+9^2))+((25*123.12+45877*25)+25))-547)";
        System.out.print(s + " expected output 8302231.36 14 actually ");
        solution.recursion(s, 0);

        s = "(-1 + (-2))";
        System.out.print(s + " expected output -3 3 actually ");
        solution.recursion(s, 0);

        s = "-sin(2*(-5+1.5*4)+28)";
        System.out.print(s + " expected output -0.5 7 actually ");
        solution.recursion(s, 0);

        s = "sin(100)-sin(100)";
        System.out.print(s + " expected output 0 3 actually ");
        solution.recursion(s, 0);

    }

    final private String POW = "^";
    final private String PLUS = "+";
    final private String MINUS = "-";
    final private String MULT = "*";
    final private String DIV1 = ":";
    final private String DIV2 = "/";


    final private String COS = "cos";
    final private String SIN = "sin";
    final private String TAN = "tan";

    private double signToMethod(String sign, double a, double b) {
        double result;
        switch (sign) {
            case POW:
                result = pow(a, b);
                break;
            case PLUS:
                result = sum(a, b);
                break;
            case MINUS:
                result = minus(a, b);
                break;
            case MULT:
                result = mult(a, b);
                break;
            case DIV1:
                result = div(a, b);
                break;
            case DIV2:
                result = div(a, b);
                break;

            case COS:
                result = cos(a, b);
                break;
            case SIN:
                result = sin(a, b);
                break;
            case TAN:
                result = tan(a, b);
                break;
            default:
                throw new IllegalArgumentException("Unknown sign " + sign);
        }
        return result;
    }


    private double pow(double a, double b) {
        return Math.pow(a, b);
    }

    private double sum(double a, double b) {
        return a + b;
    }

    private double minus(double a, double b) {
        return a - b;
    }

    private double mult(double a, double b) {
        return a * b;
    }

    private double div(double a, double b) {
        return a / b;
    }


    private double cos(double a, double b) {
        return Math.cos(radianToDegree(b));
    }

    private double sin(double a, double b) {
        return Math.sin(radianToDegree(b));
    }

    private double tan(double a, double b) {
        return Math.tan(radianToDegree(b));
    }

    private double radianToDegree(double num) {
        return num * Math.PI / 180;
    }

    private static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }


    public void recursion(final String expression, int countOperation) {

        String inExpression = expression;



        //Boolean finished = false;
        String output = "empty";
        try {
            double number = round(Double.parseDouble(expression), 2);

            if (countOperation==0 && number<0){
                countOperation++;
            }

            output = number + " " + countOperation;
            output = output.replaceAll("\\.0 ", " ");
            System.out.println(output);
            //finished = true;
            //return output;
            return ;
        } catch (NumberFormatException e) {
            ///System.out.println("finished");
        }
       // if (!finished) {
            //call recursion again
            //recursion(expression, countOperation);
       // }


        //implement
        ///System.out.println("hello " + inExpression);

        //====================================================================================================
        //what to calculate
        //brackets
        //get last opened bracket
        int lastOpenedBracketIndex = inExpression.lastIndexOf("(");
        int firstClosedFromLastOpenedIndex = -1;
        String subExpression = inExpression;
        if (lastOpenedBracketIndex != -1) {
            String fromBracketToEnd = inExpression.substring(lastOpenedBracketIndex);

            //find first closed for last opened
            firstClosedFromLastOpenedIndex = fromBracketToEnd.indexOf(")");
            if (firstClosedFromLastOpenedIndex != -1) {
                String bestBracket = fromBracketToEnd.substring(1, firstClosedFromLastOpenedIndex);
                ///System.out.println(bestBracket);
                subExpression = bestBracket;

                //try to get number
                Double num = null;
                try {
                    num = Double.valueOf(removeBrackets(subExpression));

                } catch (NumberFormatException e) {
                    ///System.out.println("is not a num");
                    num = null;
                }

                if (num!=null || (!subExpression.contains(POW) &&
                        !subExpression.contains(PLUS) &&
                        !subExpression.contains(MINUS) &&
                        !subExpression.contains(MULT) &&
                        !subExpression.contains(DIV1) &&
                        !subExpression.contains(DIV2) &&
                        !subExpression.contains(COS) &&
                        !subExpression.contains(SIN) &&
                        !subExpression.contains(TAN))) {

                    ///System.out.println("old exp=" + inExpression + " sub=" + subExpression);

                    String part1 = inExpression.substring(0, lastOpenedBracketIndex);
                    String part2 = inExpression.substring(lastOpenedBracketIndex + firstClosedFromLastOpenedIndex + 1);
                    inExpression = part1 + subExpression + part2;
                    ///System.out.println("new exp=" + inExpression + " sub=" + subExpression);
                    recursion(inExpression, countOperation);
                    //return "need this return";
                    return ;
                }
            } else {
                throw new IllegalArgumentException("We do not have closed bracket " + fromBracketToEnd);
            }
        }
        //====================================================================================================

        ///System.out.println("IN subExpression=" + subExpression);
        //String oneSignExpression = getSimpleExpression(expression);
        String oneSignExpression = subExpression;

        int signIndex = -1;
        int signLength = 0;
        boolean needsLeftNumber = true;
        boolean needsRightNumber = true;
        boolean noSignFound = true;

        //find cos
        if (noSignFound) {
            signIndex = oneSignExpression.indexOf(COS);
            if (signIndex != -1) {
                noSignFound = false;
                signLength = COS.length();
                needsLeftNumber = false;
            }
        }
        //find sin
        if (noSignFound) {
            signIndex = oneSignExpression.indexOf(SIN);
            if (signIndex != -1) {
                noSignFound = false;
                signLength = SIN.length();
                needsLeftNumber = false;
            }
        }
        //find tan
        if (noSignFound) {
            signIndex = oneSignExpression.indexOf(TAN);
            if (signIndex != -1) {
                noSignFound = false;
                signLength = TAN.length();
                needsLeftNumber = false;
            }
        }

        //find ^
        if (noSignFound) {
            signIndex = oneSignExpression.indexOf(POW);
            if (signIndex != -1) {
                noSignFound = false;
                signLength = POW.length();
            }
        }

        //find *
        if (noSignFound) {
            signIndex = oneSignExpression.indexOf(MULT);
            if (signIndex != -1) {
                noSignFound = false;
                signLength = MULT.length();
            }
        }
        //find /
        if (noSignFound) {
            signIndex = oneSignExpression.indexOf(DIV1);
            if (signIndex != -1) {
                noSignFound = false;
                signLength = DIV1.length();
            }
        }
        if (noSignFound) {
            signIndex = oneSignExpression.indexOf(DIV2);
            if (signIndex != -1) {
                noSignFound = false;
                signLength = DIV2.length();
            }
        }
        //find +
        if (noSignFound) {
            signIndex = oneSignExpression.indexOf(PLUS);
            if (signIndex != -1) {
                noSignFound = false;
                signLength = PLUS.length();
            }
        }
        //find -
        if (noSignFound) {
            signIndex = oneSignExpression.indexOf(MINUS);
            if (signIndex != -1) {
                noSignFound = false;
                signLength = MINUS.length();
            }
        }

        System.out.println(oneSignExpression);
        //calculate
        //get sign
        String sign = oneSignExpression.substring(signIndex, signIndex + signLength);
        ///System.out.println("Sign=" + sign);
        //get left number
        Double left = null;
        int leftSignIndex = 0;
        if (needsLeftNumber) {

            //try to find left sign

            while (true) {

                String leftNumberString = oneSignExpression.substring(leftSignIndex, signIndex);
                //workaround +1.5
                if (leftNumberString.charAt(0)=='+'){
                    leftSignIndex++;
                    leftNumberString = oneSignExpression.substring(leftSignIndex, signIndex);
                }
                try {
                    left = Double.valueOf(removeBrackets(leftNumberString));
                    if (left<0){
                        countOperation++;
                    }
                } catch (NumberFormatException e) {
                    ///System.out.println("reducing left...");
                    left = null;
                }
                if (left == null) {
                    leftSignIndex++;
                } else {
                    break;
                }
            }
        }
        //get right number
        Double right = null;
        int rightSignIndex = oneSignExpression.length();
        if (needsRightNumber) {


            while (true) {

                String rightNumberString = oneSignExpression.substring(signIndex + signLength, rightSignIndex);
                try {
                    right = Double.valueOf(removeBrackets(rightNumberString));
                    if (right<0){
                        countOperation++;
                    }
                } catch (NumberFormatException e) {
                    ///System.out.println("reducing right...");
                    right = null;
                }
                if (right == null) {
                    rightSignIndex--;
                } else {
                    break;
                }
            }
            if (!needsLeftNumber) {
                left = (double) 0;
                leftSignIndex = signIndex;
            }
        }
        //if we get sign increment countOperation
        countOperation++;
        double result = round(signToMethod(sign, left, right),2);

        ///System.out.println("replace " + oneSignExpression + " with result " + result);

        String megaPart1 = oneSignExpression.substring(0,leftSignIndex);
        String megaPart2 = String.valueOf(result);
        String megaPart3 = oneSignExpression.substring(rightSignIndex);

        ///System.out.println("megaParts="+megaPart1 + megaPart2 + megaPart3);
        subExpression = megaPart1 + megaPart2 + megaPart3;
        //general part replace
        ///System.out.println("general old exp=" + inExpression + " sub=" + subExpression);



        /*if (firstClosedFromLastOpenedIndex==-1){
            firstClosedFromLastOpenedIndex = inExpression.length();
        }*/

        String part1 = "";
        String part2 = "";
        if (lastOpenedBracketIndex!=-1){
            part1 = inExpression.substring(0, lastOpenedBracketIndex+1);
            part2 = inExpression.substring(lastOpenedBracketIndex + firstClosedFromLastOpenedIndex);
        }

        inExpression = part1 + subExpression + part2;
        ///System.out.println("general new exp=" + inExpression + " sub=" + subExpression);


        //replace
        //String replacedExpression = String.valueOf(result);
        String replacedExpression = inExpression;

        ///System.out.println("replacedExpression=" + replacedExpression);

        recursion(replacedExpression,countOperation);
        //return output;
        return ;
    }

    // from (-3) to -3
    private String removeBrackets(String numberString) {
        if ((numberString.substring(0, 1).equals("(") && numberString.substring(numberString.length() - 1, numberString.length()).equals(")")) ||
                (numberString.substring(0, 1).equals("[") && numberString.substring(numberString.length() - 1, numberString.length()).equals("]"))) {
            return numberString.substring(1, numberString.length() - 1);
        }
        return numberString;
    }



    public Solution() {
        //don't delete
    }
}

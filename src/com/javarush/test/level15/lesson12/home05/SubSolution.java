package com.javarush.test.level15.lesson12.home05;

/**
 * Created by Administrator on 9/21/2014.
 */
public class SubSolution extends Solution
{


    public SubSolution(int a)
    {
        super(a);
    }

    public SubSolution(Integer a)
    {
        super(a);
    }

    public SubSolution(String a)
    {
        super(a);
    }

    protected SubSolution(Object a)
    {
        super(a);
    }

    protected SubSolution(char a)
    {
        super(a);
    }

    protected SubSolution(boolean a)
    {
        super(a);
    }

     SubSolution(byte a)
    {
        super(a);
    }

     SubSolution(short a)
    {
        super(a);
    }

    SubSolution(long a)
    {
        super(a);
    }

    private SubSolution(Float a){
        super(a);

    }
    private SubSolution(double a){
        super(a);

    }
    private SubSolution(Double a){

        super(a);

    }
}

package com.javarush.test.level15.lesson12.bonus02;

/**
 * Created by Administrator on 9/23/2014.
 */
public class TeaMaker extends DrinkMaker
{
    @Override
    void getRightCup()
    {
        System.out.println("Берем чашку для чая");
    }

    @Override
    void putIngredient()
    {
        System.out.println("Насыпаем чай");
    }

    @Override
    public void makeDrink()
    {
        super.makeDrink();
    }

    @Override
    void pour()
    {
        System.out.println("Заливаем водой");
    }
}

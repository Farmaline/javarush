package com.javarush.test.level15.lesson12.home09;

/* Парсер реквестов
Считать с консоли URl ссылку.
Вывести на экран через пробел список всех параметров (Параметры идут после ? и разделяются &, например, lvl=15).
URL содержит минимум 1 параметр.
Если присутствует параметр obj, то передать его значение в нужный метод alert.
alert(double value) - для чисел (дробные числа разделяются точкой)
alert(String value) - для строк

Пример 1
Ввод:
http://javarush.ru/alpha/index.html?lvl=15&view&name=Amigo
Вывод:
lvl view name

Пример 2
Ввод:
http://javarush.ru/alpha/index.html?obj=3.14&name=Amigo
Вывод:
obj name
double 3.14
*/

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;


public class Solution
{
    public static void main(String[] args)
    {
        //add your code here
        BufferedReader scan = new BufferedReader(new InputStreamReader(System.in));
        String magicValue = null;
        try
        {

            URI webRes = new URI(scan.readLine());

            String querty = webRes.getQuery();


            String[] splitArray = querty.split("&");

            String result = "";
            Double magic = null;
            for (String text : splitArray)
            {

                if (text.contains("="))
                {
                    String variable = (text.substring(0, text.indexOf("=")));
                    if (variable.equals("obj"))
                    {

                        magicValue = text.substring(text.indexOf("=") + 1, text.length());
                        try
                        {
                            magic = Double.valueOf(magicValue);
                        }
                        catch (NumberFormatException e)
                        {
                        }

                    }

                    result = (result + " " + variable);
                } else
                {
                    result = result + " " + (text);
                }
            }
            System.out.println(result.trim());
            if (magic != null)
            {
                alert(magic);
            } else
            {
                if (magicValue != null)
                {
                    alert(magicValue);
                }
            }
        }
        catch (URISyntaxException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }


    }

    public static void alert(double value)
    {
        System.out.println("double " + value);
    }

    public static void alert(String value)
    {
        System.out.println("String " + value);
    }
}

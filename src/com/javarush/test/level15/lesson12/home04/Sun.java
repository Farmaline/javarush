package com.javarush.test.level15.lesson12.home04;

/**
 * Created by Administrator on 9/20/2014.
 */
public class Sun implements Planet
{
    private static Sun ourInstance = null;

    public static Sun getInstance()
    {
        if (ourInstance == null)
        {
            synchronized (Sun.class)
            {
                if (ourInstance == null)
                {
                    ourInstance = new Sun();
                }
            }
        }
        return ourInstance;
    }
    private Sun()
    {
    }
}

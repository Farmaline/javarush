package com.javarush.test.level15.lesson12.home04;

/**
 * Created by Administrator on 9/20/2014.
 */
public class Moon implements Planet
{
    private static Moon ourInstance = null;

    public static Moon getInstance()
    {
        if (ourInstance == null)
        {
            synchronized (Moon.class)
            {
                if (ourInstance == null)
                {
                    ourInstance = new Moon();
                }
            }
        }
        return ourInstance;
    }

    private Moon()
    {
    }
}

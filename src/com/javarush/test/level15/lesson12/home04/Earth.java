package com.javarush.test.level15.lesson12.home04;

/**
 * Created by Administrator on 9/20/2014.
 */
public class Earth implements Planet
{
    private static Earth ourInstance = null;

    public static Earth getInstance() {
        if (ourInstance == null) {
            synchronized (Earth.class) {
                if (ourInstance == null) {
                    ourInstance = new Earth();
                }
            }
        }
        return ourInstance;
    }
    private Earth()
    {
    }
}

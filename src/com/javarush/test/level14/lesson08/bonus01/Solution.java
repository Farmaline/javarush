package com.javarush.test.level14.lesson08.bonus01;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/* Нашествие эксепшенов
Заполни массив exceptions 10 различными эксепшенами.
Первое исключение уже реализовано в методе initExceptions.
*/

public class Solution
{
    public static List<Exception> exceptions = new ArrayList<Exception>();

    public static void main(String[] args)
    {
        initExceptions();

        for (Exception exception : exceptions)
        {
            System.out.println(exception);
        }
    }

    private static void initExceptions()
    {   //it's first exception
        try//1
        {
            float i = 1 / 0;

        }
        catch (Exception e)
        {
            exceptions.add(e);
        }

        try//2
        {
            int[] num = new int[10];
            for (int i = 0; i < num.length + 1; i++)
            {
                int temp = num[i];
            }
        }
        catch (Exception e1)
        {
            exceptions.add(e1);
        }
        try//3
        {
            String string = null;
            string.isEmpty();
        }
        catch (Exception e2)
        {
            exceptions.add(e2);
        }
        try//4
        {
            Object mama = "Mama";
            Integer num = (Integer) mama;
        }
        catch (Exception e)
        {
            exceptions.add(e);
        }
        try//5
        {
            InputStream file = new FileInputStream("/C:/dfsdf");
        }
        catch (Exception e)
        {
            exceptions.add(e);
        }
        try//6
        {
            String str = "abc";
            Integer.parseInt(str);
            //Integer.parseInt(str);
        }
        catch (Exception e)
        {
            exceptions.add(e);
        }

        try//7
        {
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");

            sdf.parse("goo");
        }
        catch (ParseException e)
        {
            exceptions.add(e);
        }
        try//8
        {
            throw new IllegalStateException();
        }catch (Exception e){
            exceptions.add(e);
        }
        try//9
        {
            throw new RuntimeException();
        }catch (Exception e){
            exceptions.add(e);
        }
        try//10
        {
           throw new NullPointerException();
        }
        catch (Exception e)
        {
            exceptions.add(e);
        }



        //Add your code here

    }
}

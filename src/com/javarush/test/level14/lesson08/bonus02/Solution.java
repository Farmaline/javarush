package com.javarush.test.level14.lesson08.bonus02;

/* НОД
Наибольший общий делитель (НОД).
Ввести с клавиатуры 2 целых положительных числа.
Вывести в консоль наибольший общий делитель.
*/

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        BufferedReader scan = new BufferedReader(new InputStreamReader(System.in));



        int a = Integer.parseInt(scan.readLine());
        int b = Integer.parseInt(scan.readLine());

        int min;
        int maxNOD = 1;
        if (a < b)
        {
            min = a;
        } else
        {
            min = b;
        }
        for (int i=1; i<=min; i++){

            if (a % i ==0 && b % i==0){
                maxNOD = i;
            }
        }
        System.out.println(maxNOD);

    }




}

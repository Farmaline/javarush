package com.javarush.test.level14.lesson06.home01;

/**
 * Created by Administrator on 9/11/2014.
 */
public class UkrainianHen extends Hen
{

    @Override
    int getCountOfEggsPerMonth()
    {
        return 50;
    }

    @Override
    String getDescription()
    {
        return (super.getDescription() + " Моя страна - " + Country.UKRAINE + ". Я несу " + getCountOfEggsPerMonth() + " яиц в месяц.");
    }
}
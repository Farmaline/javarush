package com.javarush.test.level14.lesson06.home01;

/**
 * Created by Administrator on 9/11/2014.
 */
public class MoldovanHen extends Hen
{

    @Override
    int getCountOfEggsPerMonth()
    {
        return 20;
    }

    @Override
    String getDescription()
    {
        return (super.getDescription() + " Моя страна - " + Country.MOLDOVA + ". Я несу " + getCountOfEggsPerMonth() + " яиц в месяц.");
    }
}

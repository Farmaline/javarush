package com.javarush.test.level14.lesson06.home01;

/**
 * Created by Administrator on 9/11/2014.
 */
public class RussianHen extends Hen

{
    @Override
    String getDescription()
    {
        return (super.getDescription() + " Моя страна - " + Country.RUSSIA + ". Я несу " + getCountOfEggsPerMonth() + " яиц в месяц.");

    }

    @Override
    int getCountOfEggsPerMonth()
    {
        return 10;
    }
}

package com.javarush.test.level06.lesson08.task04;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/* Класс ConsoleReader
Сделать класс ConsoleReader, у которого будут 4 статических метода:
String readString() – читает с клавиатуры строку
int readInt() – читает с клавиатуры число
double readDouble() – читает с клавиатуры дробное число
void readLn() – ждет нажатия enter [использовать readString()]
*/

public class ConsoleReader
{

    private static BufferedReader scan = new BufferedReader(new InputStreamReader(System.in));


    public static String readString() throws Exception
    {
        String s= scan.readLine();
        return s;

    }

    public static int readInt() throws Exception
    {
        int s = Integer.parseInt(scan.readLine());
        return s;

    }

    public static double readDouble() throws Exception
    {
        double s = Double.parseDouble(scan.readLine());
        return s;

    }

    public static void readLn() throws Exception
    {


            

    }
}

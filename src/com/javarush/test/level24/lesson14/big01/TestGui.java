package com.javarush.test.level24.lesson14.big01;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Administrator on 10.06.2015.
 */
public class TestGui {
    private JButton button1;

    public TestGui() {
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Arcanoid game = new Arcanoid(20, 30);

                Ball ball = new Ball(10, 29, 2, 95);
                game.setBall(ball);

                Stand stand = new Stand(10, 30);
                game.setStand(stand);

                game.getBricks().add(new Brick(3, 3));
                game.getBricks().add(new Brick(7, 5));
                game.getBricks().add(new Brick(12, 5));
                game.getBricks().add(new Brick(16, 3));

                try {
                    game.run();
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        });
    }
}

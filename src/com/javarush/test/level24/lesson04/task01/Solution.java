package com.javarush.test.level24.lesson04.task01;

import java.math.BigDecimal;

/* Наследование от внутреннего класса
Внутри класса Solution создайте 2 внутренних public класса Apt3Bedroom, BigHall.
Унаследуйте их от Apartments и Hall.
*/
public class Solution {
    public class Building {
        public class Hall {
            private BigDecimal square;

            public Hall(BigDecimal square) {
                this.square = square;
            }
        }

        public class Apartments {
        }
    }

    public class Apt3Bedroom extends Building.Apartments //объект класса который унаследовался от внутреннего класса
    {
        Apt3Bedroom (Building building)     //в конструктор класса Apt3Bedroom нужно неявно передать объект класса Building
        {
            building.super();               //это делаеться с помощью специальной контсрукции "building.super();"
        }

    }

    public class BigHall extends Building.Hall   //в этом варианте присутствовала приватная переменная класса "BigDecimal"
    {


         BigHall( Building building, BigDecimal bigDecimal) { //ее я тоже передал в конструктор, что в итоге оказалось правильным

           building.super(bigDecimal);                        //если создавать пустой конструктор то код не компилится вообще

        }
    }
}

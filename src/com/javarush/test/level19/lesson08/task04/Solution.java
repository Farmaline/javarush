package com.javarush.test.level19.lesson08.task04;

/* Решаем пример
В методе main подмените объект System.out написанной вами ридер-оберткой по аналогии с лекцией
Ваша ридер-обертка должна выводить на консоль решенный пример
Вызовите готовый метод printSomething(), воспользуйтесь testString
Верните переменной System.out первоначальный поток

Возможные операции: + - *
Шаблон входных данных и вывода: a [знак] b = c
Отрицательных и дробных чисел, унарных операторов - нет.

Пример вывода:
3 + 6 = 9
*/

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class Solution {
    public static TestString testString = new TestString();

    public static void main(String[] args) {

        PrintStream consolePrint = System.out;
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        PrintStream printStream = new PrintStream(outputStream);
        System.setOut(printStream);

        testString.printSomething();
        String resulr = outputStream.toString();
        int fina = 0;
        System.setOut(consolePrint);

        resulr.trim();
        String[] array = resulr.split(" ");
        if (array[1].equals("+")) {
            fina = Integer.parseInt(array[0]) + Integer.parseInt(array[2]);
        }
        if (array[1].equals("-")) {
            fina = Integer.parseInt(array[0]) - Integer.parseInt(array[2]);
        }
        if (array[1].equals("*")) {
            fina = Integer.parseInt(array[0]) * Integer.parseInt(array[2]);
        }

        System.out.println(resulr.replace(System.lineSeparator(),"")+fina);

    }

    public static class TestString {
        public void printSomething() {
            System.out.println("3 + 6 = ");
        }
    }
}


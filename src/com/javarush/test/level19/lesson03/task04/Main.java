package com.javarush.test.level19.lesson03.task04;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

/**
 * Created by Administrator on 17.11.2014.
 */
public class Main {

    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(new File("C:\\l.txt"), "Cp1251");

        Solution.PersonScannerAdapter adapter = new Solution.PersonScannerAdapter(scanner);

        System.out.println(adapter.read());
    }
}

package com.javarush.test.level19.lesson05.task02;

/* Считаем слово
Считать с консоли имя файла.
Вывести в консоль количество слов "world", которые встречаются в файле.
Закрыть поток ввода.
*/

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Solution {



    public static void main(String[] args) throws IOException {

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        BufferedReader fileBuf = new BufferedReader(new FileReader(bufferedReader.readLine()));

        int result = 0;

        while (fileBuf.ready()) {
            String text = fileBuf.readLine().toLowerCase();
            String patternString = "(\\s|^)(world\\W|world$)";

            Pattern pattern = Pattern.compile(patternString);
            Matcher matcher = pattern.matcher(text);

            while (matcher.find()) {
                result++;
            }

        }


        System.out.println(result);
    }
}

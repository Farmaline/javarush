package com.javarush.test.level19.lesson05.task03;

/* Выделяем числа
Считать с консоли 2 имени файла.
Вывести во второй файл все числа, которые есть в первом файле.
Числа выводить через пробел.
Закрыть потоки ввода-вывода.

Пример тела файла:
12 text var2 14 8v 1

Результат:
12 14 1
*/

import java.io.*;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));


        File fileOne = new File(bufferedReader.readLine());
        File fileTwo = new File(bufferedReader.readLine());
        Scanner scanner = new Scanner(fileOne);
        FileWriter fileWriter = new FileWriter(fileTwo);
        String result ="";
        while (scanner.hasNext())
        {
            scanner.useDelimiter("\\s+");
            String element =scanner.next();

            String patternString = "^\\d+$";

            Pattern pattern = Pattern.compile(patternString);
            Matcher matcher = pattern.matcher(element);

            if (matcher.matches())
            {
                result += " " +element;
            }


        }
        result.trim();

        fileWriter.write(result);
        fileWriter.flush();
        fileWriter.close();
        scanner.close();
        bufferedReader.close();
    }
}

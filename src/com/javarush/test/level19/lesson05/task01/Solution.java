package com.javarush.test.level19.lesson05.task01;

/* Четные байты
Считать с консоли 2 имени файла.
Вывести во второй файл все байты с четным индексом.
Пример: второй байт, четвертый байт, шестой байт и т.д.
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException {

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));


        File fileOne = new File(bufferedReader.readLine());
        File fileTwo = new File(bufferedReader.readLine());

        FileReader fileReader = new FileReader(fileOne);
        FileWriter fileWriter = new FileWriter(fileTwo);
        int temp = 1;
        while (fileReader.ready()) {
            int data = fileReader.read();

            if ((temp % 2) == 0) {

                fileWriter.write(data);
            }
            temp++;
        }

        fileReader.close();
        fileWriter.close();
    }
}

package com.javarush.test.level19.lesson10.home05;

/* Слова с цифрами
В метод main первым параметром приходит имя файла1, вторым - файла2.
Файл1 содержит слова, разделенные пробелом.
Записать через пробел в Файл2 все слова, которые содержат цифры, например, а1 или abc3d
Закрыть потоки
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) {

        String fileNameInput = args[0];
        String fileNameOutput = args[1];
        InputStreamReader inputStreamReader = null;
        FileWriter fileWriter = null;
        try {
            inputStreamReader = new InputStreamReader(new FileInputStream(fileNameInput));
            fileWriter = new FileWriter(fileNameOutput);
        } catch (IOException e) {
            e.printStackTrace();
        }
        BufferedReader reader = new BufferedReader(inputStreamReader);

        try {
            while (reader.ready())
            {
                String line = reader.readLine();
                String [] spliter = line.split(" ");
                String resultWrite ="";
              for(int i =0; i < spliter.length; i++)
              {
                  spliter[i] = spliter[i].replaceAll("(^\\d+$)|(^\\D+$)|(^\\W$)", "");
                  if(!spliter[i].equals(""))
                  resultWrite += spliter[i] + " ";
              }
                resultWrite = resultWrite.trim();

                fileWriter.write(resultWrite);
                if(reader.ready()){
                    fileWriter.write(System.lineSeparator());
                }

                fileWriter.flush();
            }

            reader.close();
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}

package com.javarush.test.level19.lesson10.home02;

/* Самый богатый
В метод main первым параметром приходит имя файла.
В этом файле каждая строка имеет следующий вид:
имя значение
где [имя] - String, [значение] - double. [имя] и [значение] разделены пробелом

Для каждого имени посчитать сумму всех его значений
Вывести в консоль имена, у которых максимальная сумма
Имена разделять пробелом либо выводить с новой строки
Закрыть потоки

Пример входного файла:
Петров 0.501
Иванов 1.35
Петров 0.85

Пример вывода:
Петров
*/

import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) throws IOException {
        TreeMap<String, Double> map = new TreeMap<String, Double>();

        String fileName = args[0];
        String name = "";
        double dig = 0;
        InputStreamReader inputStreamReader = new InputStreamReader(new FileInputStream(fileName));
        BufferedReader br = new BufferedReader(inputStreamReader);

        while (br.ready()) {
            String[] arr = br.readLine().split(" ");
            name = arr[0];
            dig = Double.parseDouble(arr[1]);
            if (map.containsKey(name))
                dig = dig + map.get(name);
            map.put(name, dig);
        }
        inputStreamReader.close();
        br.close();


        List<String> result = new ArrayList<>();
        result.add(0, "0");
        for (Map.Entry<String, Double> print : map.entrySet()) {
            double tempDouble = print.getValue();
            String winner = print.getKey();

            if (Double.parseDouble(result.get(0)) < tempDouble) {
                result.clear();
                result.add(0, String.valueOf(tempDouble));
                result.add(winner);
            } else {
                if ((Double.parseDouble(result.get(0)) == tempDouble)) {
                    result.add(winner);
                }
            }

        }
        Map<String, String> resultMap = new TreeMap<String, String>();
        int flag = 0;
        for (String temp : result) {
            if (flag > 0) {
                resultMap.put(temp, "0");
            }
            flag++;
        }

        for (Map.Entry<String, String> entry : resultMap.entrySet()) {
            System.out.println(entry.getKey());

        }

    }
}

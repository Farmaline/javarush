package com.javarush.test.level19.lesson10.home08;

/* Перевертыши
1 Считать с консоли имя файла.
2 Для каждой строки в файле:
2.1 переставить все символы в обратном порядке
2.2 вывести на экран
3 Закрыть поток

Пример тела входного файла:
я - программист.
Амиго

Пример результата:
.тсиммаргорп - я
огимА
*/

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Solution {
    public static void main(String[] args) throws IOException {

        BufferedReader reed = new BufferedReader(new InputStreamReader(System.in));
        String path = reed.readLine();


        BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(path)));

        List<String> list = new ArrayList<>();

        while (br.ready())
        {
            String line = br.readLine();
            StringBuilder stringBuilder = new StringBuilder(line);
            line = stringBuilder.reverse().toString();
            list.add(line);
        }

        for(String text : list)
        {
            System.out.println(text);
        }
        reed.close();
        br.close();
    }
}

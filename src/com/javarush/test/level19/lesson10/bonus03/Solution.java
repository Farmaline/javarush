package com.javarush.test.level19.lesson10.bonus03;

/* Знакомство с тегами
Считайте с консоли имя файла, который имеет HTML-формат
Пример:
Info about Leela <span xml:lang="en" lang="en"><b><span>Turanga Leela
</span></b></span>
Первым параметром в метод main приходит тег. Например, "span"
Вывести на консоль все теги, которые соответствуют заданному тегу
Каждый тег на новой строке, порядок должен соответствовать порядку следования в файле
Количество пробелов, \n, \r не влияют на результат
Файл не содержит тег CDATA, для всех открывающих тегов имеется отдельный закрывающий тег, одиночных тегов нету
Тег может содержать вложенные теги
Пример вывода:
<span xml:lang="en" lang="en"><b><span>Turanga Leela</span></b></span>
<span>Turanga Leela</span>

Шаблон тега:
<tag>text1</tag>
<tag text2>text1</tag>
<tag
text2>text1</tag>

text1, text2 могут быть пустыми
*/

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Solution {
    private static String arg;
    private static String fileText;


    public static void main(String[] args) throws IOException {
        arg = args[0];


        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        BufferedReader fileBuf = new BufferedReader(new FileReader(bufferedReader.readLine()));
        //BufferedReader fileBuf = new BufferedReader(new FileReader("C:\\page2.html"));
        fileText = "";
        while (fileBuf.ready()) {
            fileText += fileBuf.readLine();
        }
        //foo(fileText);
        List<Integer> start = new ArrayList<Integer>();
        List<Integer> end = new ArrayList<>();

        String patternString = "<" + arg;
        // String patternString = "</?" + arg + "[^<>]*>";

        Pattern pattern = Pattern.compile(patternString);
        Matcher matcher = pattern.matcher(fileText);

       /* int nordCount = -1;
        int left = 0;
        int right = 0;*/
       /* while (matcher.find()) {
            String found = matcher.group();
            if (found.contains("/")) {
                System.out.println("END TAG=" + found);
                nordCount--;
                if (nordCount==-1){
                    right = matcher.end();
                }
            } else {
                System.out.println("START TAG=" + found);
                nordCount++;
                if (nordCount==0){
                    left = matcher.start();
                }
            }
            //System.out.println(nordCount + " " + found);
            if (nordCount==-1){
                System.out.println("G="+fileText.substring(left,right));

            }
        }*/

        int count = 0;
        while (matcher.find()) {
            count++;
            start.add(matcher.start());

           /* System.out.println("found: " + count + " : "
                    + matcher.start() + " - " + matcher.end() + " substring " + fileText.substring(matcher.start(), matcher.end()));*/
        }

        patternString = "</" + arg + ">";

        pattern = Pattern.compile(patternString);
        matcher = pattern.matcher(fileText);

        int count2 = 0;
        while (matcher.find()) {
            count++;
            end.add(matcher.end());

           /* System.out.println("found: " + count + " : "
                    + matcher.start() + " - " + matcher.end() + " substring " + fileText.substring(matcher.start(), matcher.end()));*/
        }
       /* System.out.println(start);
        System.out.println(end);*/
        TreeMap<Integer, Boolean> map = new TreeMap<>();

        for (int i = 0; i < start.size(); i++) {
            map.put(start.get(i), true);
            map.put(end.get(i) - 1, false);
        }
        ArrayList<String> stringsArray = new ArrayList<>();
        for (Map.Entry<Integer, Boolean> element : map.entrySet()) {
            stringsArray.add(element.getKey() + " " + element.getValue());
        }
        conlosePrint(stringsArray);


    }

    private static void conlosePrint(ArrayList<String> arrayList) {
        if (arrayList.size() == 2) {
            print(arrayList.get(0),arrayList.get(1));
            return;
        }

        int flag = 0;
        for (int i = 1; i < arrayList.size(); i++) {

            if (arrayList.get(i).contains("false")) {
                if (flag == 0) {
                    print(arrayList.get(0),arrayList.get(i));
                    arrayList.remove(i);
                    arrayList.remove(0);
                    conlosePrint(arrayList);
                } else {
                    flag--;
                }
            } else {
                flag++;
            }
        }
    }
    private static void print (String start, String end)
    {
        String [] starts = start.split(" ");
        int s1 = Integer.parseInt(starts[0]);
        String [] ends = end.split(" ");
        int e1 = Integer.parseInt(ends[0]);
        String console = fileText.substring(s1,e1+1);
        System.out.println(console);
    }

   /* private static void foo(String fileText) {
        String patternString = "</?" + arg + "[^<>]*>";
        //System.out.println("pattern = " + patternString);

        Pattern pattern = Pattern.compile(patternString);
        Matcher matcher = pattern.matcher(fileText);

        int nordCount = -1;
        int left = -1;
        int right = -1;

        int inLeft = -1;
        int inRight = -1;
        boolean hasChild = false;
        while (matcher.find()) {
            String found = matcher.group();
            if (found.contains("/")) {
                //System.out.println("END TAG=" + found);
                nordCount--;
                if (nordCount == -1) {
                    right = matcher.end();
                    inRight = matcher.start();
                }
            } else {
                //System.out.println("START TAG=" + found);
                nordCount++;
                if (nordCount == 0) {
                    left = matcher.start();
                    inLeft = matcher.end();
                } else {
                    if (nordCount > 0) {
                        hasChild = true;
                    }
                }
            }
            //System.out.println("sdfsdfsdfsdf"+nordCount + " " + found + " hasChild" + hasChild);
            if (nordCount == -1) {
                System.out.println(fileText.substring(left, right));
                if (hasChild) {
                    //System.out.println("INNER" + fileText.substring(inLeft,inRight));
                    foo(fileText.substring(inLeft, inRight));
                }
                hasChild = false;
            }
        }
    }*/

}

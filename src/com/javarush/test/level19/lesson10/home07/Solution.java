package com.javarush.test.level19.lesson10.home07;

/* Длинные слова
В метод main первым параметром приходит имя файла1, вторым - файла2
Файл1 содержит слова, разделенные пробелом.
Записать через запятую в Файл2 слова, длина которых строго больше 6
Закрыть потоки

Пример выходных данных:
длинное,короткое,аббревиатура
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) {

       String fileNameInput = args[0];
        String fileNameOutput = args[1];
        InputStreamReader inputStreamReader = null;
        FileWriter fileWriter = null;
        try {
            inputStreamReader = new InputStreamReader(new FileInputStream(fileNameInput));
           fileWriter = new FileWriter(fileNameOutput);
        } catch (IOException e) {
            e.printStackTrace();
        }
        BufferedReader reader = new BufferedReader(inputStreamReader);


        try {
            String resultWrite = "";
            while (reader.ready()){
                String line = reader.readLine();
                String [] lineArray = line.split(" ");
                for (int i= 0; i < lineArray.length; i++)
                {
                    if(lineArray[i].length()>6)
                    {
                        resultWrite += lineArray[i] + ",";
                    }
                }


            }
            resultWrite = resultWrite.replaceAll(",$", "");
            fileWriter.write(resultWrite);

            reader.close();
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}

package com.javarush.test.level19.lesson10.home03;



import java.io.*;
import java.util.*;

/* Хуан Хуанович
В метод main первым параметром приходит имя файла.
В этом файле каждая строка имеет следующий вид:
имя день месяц год
где [имя] - может состоять из нескольких слов, разделенных пробелами, и имеет тип String
[день] - int, [месяц] - int, [год] - int
данные разделены пробелами

Заполнить список PEOPLE импользуя данные из файла
Закрыть потоки

Пример входного файла:
Иванов Иван Иванович 31 12 1987
Вася 15 5 2013
*/

public class Solution {
    public static final List<Person> PEOPLE = new ArrayList<Person>();

    public static void main(String[] args) throws IOException {

        String fileName = args[0];
        InputStreamReader inputStreamReader = new InputStreamReader(new FileInputStream(fileName));
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

        while (bufferedReader.ready())
        {
           String splitter = bufferedReader.readLine();
           String name = splitter.replaceAll("[0-9]","");
            name = name.trim();
            String birthday = splitter.replaceAll("(\\s?\\D+\\s)|(\\s\\D+\\s?)", "");
            String [] resultInt = birthday.split(" ");
            int day = Integer.parseInt(resultInt[0]);
            int month = Integer.parseInt(resultInt[1]);
            int year = Integer.parseInt(resultInt[2]);

           Calendar calendar = new GregorianCalendar(year, --month, day);

            Solution.PEOPLE.add(new Person(name, calendar.getTime()));
        }
        bufferedReader.close();


    }

}

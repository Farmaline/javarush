package com.javarush.test.level19.lesson10.bonus01;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/* Отслеживаем изменения
Считать в консоли 2 имени файла - file1, file2.
Файлы содержат строки, file2 является обновленной версией file1, часть строк совпадают.
Нужно создать объединенную версию строк, записать их в список lines
Операции ADDED и REMOVED не могут идти подряд, они всегда разделены SAME
Пример:
[Файл 1]
строка1
строка2
строка3

[Файл 2]
строка1
строка3
строка4

[Результат - список lines]
SAME строка1
REMOVED строка2
SAME строка3
ADDED строка4

*/

public class Solution {
    public static List<LineItem> lines = new ArrayList<LineItem>();

    public static void main(String[] args) throws IOException {
        BufferedReader scan = new BufferedReader(new InputStreamReader(System.in));
        String fileName1 = scan.readLine();
        String fileName2 = scan.readLine();
        InputStreamReader inputStreamReader1 = new InputStreamReader(new FileInputStream(fileName1));
        InputStreamReader inputStreamReader2 = new InputStreamReader(new FileInputStream(fileName2));

        BufferedReader bufferedReader1 = new BufferedReader(inputStreamReader1);
        BufferedReader bufferedReader2 = new BufferedReader(inputStreamReader2);

        List<String> firstFile = new ArrayList<>();
        List<String> secondFile = new ArrayList<>();

        while (bufferedReader1.ready())
        {
            firstFile.add(bufferedReader1.readLine());
        }

        while (bufferedReader2.ready())
        {
            secondFile.add(bufferedReader2.readLine());
        }
       /* List<String> firstFile = new ArrayList<>();
        List<String> secondFile = new ArrayList<>();

        //delete this
        firstFile.add("строка1");
        firstFile.add("строка1");
        firstFile.add("строка3");
        //firstFile.add("строка4");

        secondFile.add("строка1");
        secondFile.add("строка2");
        secondFile.add("строка1");*/

        //begin business logic
       /* SAME строка1
        REMOVED строка2
        SAME строка3
        ADDED строка4*/

        int firstFileSize = firstFile.size();
        int secondFileSize = secondFile.size();


        String firstLine = "";
        String secondLine= "";

        int firstIndex = 0;
        int secondIndex = 0;

        for(int i=0;i<100;i++){
            //getting first line
            if (firstIndex<firstFileSize){
                firstLine = firstFile.get(firstIndex);
            }else{
                firstLine = null;
            }
            //getting second line
            if (secondIndex<secondFileSize){
                secondLine = secondFile.get(secondIndex);
            }else {
                secondLine = null;
            }

            if (firstLine==null){
                if (secondLine!=null){
                    lines.add(new LineItem(Type.ADDED,secondLine));
                    secondIndex++;
                    continue;
                }else{
                    break;
                }
            }else{
                if (secondLine==null){
                    lines.add(new LineItem(Type.REMOVED,firstLine));
                    firstIndex++;
                    continue;
                }
            }

            if (firstLine.equals(secondLine)){
                lines.add(new LineItem(Type.SAME,firstLine));
                firstIndex++;
                secondIndex++;
            }else {

                if (firstFile.get(firstIndex +1).equals(secondLine)){
                    lines.add(new LineItem(Type.REMOVED,firstLine));
                    firstIndex++;
                }
                if (secondFile.get(secondIndex+1).equals(firstLine)){
                    lines.add(new LineItem(Type.ADDED,secondLine));
                    secondIndex++;
                }
            }

        }

        for(LineItem g: lines ){
            System.out.println(g.type + " "+ g.line);
        }

    }


    public static enum Type {
        ADDED,        //добавлена новая строка
        REMOVED,      //удалена строка
        SAME          //без изменений
    }

    public static class LineItem {
        public Type type;
        public String line;

        public LineItem(Type type, String line) {
            this.type = type;
            this.line = line;
        }
    }
}

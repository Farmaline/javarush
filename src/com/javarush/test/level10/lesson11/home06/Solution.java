package com.javarush.test.level10.lesson11.home06;

/* Конструкторы класса Human
Напиши класс Human с 6 полями. Придумай и реализуй 10 различных конструкторов для него. Каждый конструктор должен иметь смысл.
*/

public class Solution
{
    public static void main(String[] args)
    {

    }

    public static class Human
    {
        //напишите тут ваши переменные и конструкторы
        public int age;
        public String name;
        public int weight;
        public boolean sex;
        public int high;
        public Human children;

        public Human(String name)
        {

            this.name = name;
        }

        public Human(String name, Human children)
        {

            this.name = name;
            this.children = children;
        }

        public Human(String name, int age, boolean sex)
        {

            this.name = name;
            this.age = age;
            this.sex = sex;

        }

        public Human(String name, int age, int weight, boolean sex, int high)
        {

            this.name = name;
            this.age = age;
            this.weight = weight;
            this.sex = sex;
            this.high = high;

        }

        public Human(String name, int age, int weight, boolean sex, int high, Human children)
        {

            this.name = name;
            this.age = age;
            this.weight = weight;
            this.sex = sex;
            this.high = high;
            this.children = children;

        }

        public Human(int age, int weight, boolean sex, int high, Human children)
        {


            this.age = age;
            this.weight = weight;
            this.sex = sex;
            this.high = high;
            this.children = children;

        }

        public Human(String name, int age, int weight, int high, Human children)
        {

            this.name = name;
            this.age = age;
            this.weight = weight;
            this.high = high;
            this.children = children;

        }
        public Human(String name,int age, Human children)
        {

            this.name = name;
            this.age = age;
            this.children = children;
        }
        public Human(String name, int age, int weight, boolean sex)
        {

            this.name = name;
            this.age = age;
            this.weight =weight;
            this.sex = sex;

        }
        public Human(String name, int age, int weight)
        {

            this.name = name;
            this.age = age;
            this.weight =weight;


        }


    }
}

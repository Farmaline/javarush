package com.javarush.test.level10.lesson11.bonus02;

import java.io.*;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* Нужно добавить в программу новую функциональность
Задача: Программа вводит с клавиатуры пару (число и строку) и выводит их на экран.
Новая задача: Программа вводит с клавиатуры пары (число и строку), сохраняет их в HashMap.
Пустая строка – конец ввода данных. Числа могу повторяться. Строки всегда уникальны. Введенные данные не должны потеряться!
Затем программа выводит содержание HashMap на экран.

Пример ввода:
1
Мама
2
Рама
1
Мыла

Пример вывода:
1 Мама
2 Рама
1 Мыла
*/

public class Solution
{
    public static void main(String[] args) throws IOException
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        //int id = Integer.parseInt(reader.readLine());
        //String name = reader.readLine();

        //System.out.println("Id=" + id + " Name=" + name);

        Map<String, Integer> map = new HashMap<>();
        while (true)
        {
            String breakProgram = reader.readLine();
            if (breakProgram.isEmpty())
            {
                break;
            } else
            {
                Integer id = Integer.parseInt(breakProgram);
                String name = reader.readLine();
                map.put(name, id);
            }
        }


        //Map.Entry<String, Integer> iterator = map.entrySet().iterator();

        //Iterator<Map.Entry<String, Integer>> foo = map.entrySet().iterator();
       /* while(foo.hasNext()){
            Map.Entry<String,Integer> elem = foo.next();
            System.out.println(elem.getKey() + " " + elem.getValue());
        }
        */

        for (Map.Entry<String, Integer> element : map.entrySet())
        {
            System.out.println(element.getValue() + " " + element.getKey());
        }
        map.clear();


    }
}


package com.javarush.test.level10.lesson11.home08;

import java.util.ArrayList;

/* Массив списков строк
Создать массив, элементами которого будут списки строк. Заполнить массив любыми данными и вывести на их экран.
*/

public class Solution
{
    public static void main(String[] args)
    {
        ArrayList<String>[] arrayOfStringList =  createList();
        printList(arrayOfStringList);
    }

    public static ArrayList<String>[] createList()
    {
        ArrayList<String>[] supList = new ArrayList[2];

        ArrayList<String> strings0 = new ArrayList<>();
        strings0.add("sdf");
        ArrayList<String> strings1 = new ArrayList<>();
        strings1.add("gs");

        supList[0] = strings0;
        supList[1] = strings1;

        /*ArrayList<String> strings2 = new ArrayList<>();
        ArrayList<String> strings3 = new ArrayList<>();
        ArrayList<String> strings4 = new ArrayList<>();
        ArrayList<String> strings5 = new ArrayList<>();
        ArrayList<String> strings6 = new ArrayList<>();

        supList[0] = strings0;
        supList[1] = strings1;
        supList[2] = strings2;
        supList[3] = strings3;
        supList[4] = strings4;
        supList[5] = strings5;
        supList[6] = strings6;


        for (int i = 0; i < 6; i++)
        {

            for (int j = 0; j < 5; j++)
            {
                supList[i].add((String.valueOf(j)));

            }

        }*/


        return supList;
    }

    public static void printList(ArrayList<String>[] arrayOfStringList)
    {
        for (ArrayList<String> list: arrayOfStringList)
        {
            for (String s : list)
            {
                System.out.println(s);
            }
        }
    }
}
package com.javarush.test.level27.lesson15.big01.ad;


import com.javarush.test.level27.lesson15.big01.ConsoleHelper;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Administrator on 03.09.2015.
 */
public class AdvertisementManager {
    private final AdvertisementStorage storage = AdvertisementStorage.getInstance();

    private int timeSeconds;

    public AdvertisementManager(int timeSeconds) {
        this.timeSeconds = timeSeconds * 60;
    }

    public void processVideos(){
        if (storage.list().isEmpty()) throw new NoVideoAvailableException();
        List<Advertisement> advertisements = storage.list();
        //Sorting to play list: 1 - by amount per play descending; 2 - by cost of 1 second ascending
        Collections.sort(advertisements, new Comparator<Advertisement>()
        {
            @Override
            public int compare(Advertisement o1, Advertisement o2)
            {
                if (o1.getAmountPerOneDisplaying() != o2.getAmountPerOneDisplaying())
                    return Long.compare(o2.getAmountPerOneDisplaying(), o1.getAmountPerOneDisplaying());
                else {
                    long oneSecondCost2 = (o2.getAmountPerOneDisplaying() * 1000 / o2.getDuration());
                    long oneSecondCost1 = o1.getAmountPerOneDisplaying() * 1000 / o1.getDuration();
                    return Long.compare(oneSecondCost1, oneSecondCost2);
                }
            }
        });
        int timeLeft = timeSeconds;
        //Showing ads to customer
        for (Advertisement advertisement : advertisements){
            if (timeLeft >= advertisement.getDuration()){
                ConsoleHelper.writeMessage(String.format("%s is displaying... %d, %d",
                        advertisement.getName(),
                        advertisement.getAmountPerOneDisplaying(),
                        advertisement.getAmountPerOneDisplaying() * 1000 / advertisement.getDuration()));
                advertisement.revalidate();
                timeLeft -= advertisement.getDuration();
            }
            if (timeLeft == 0) break;
        }
        if(timeLeft == timeSeconds){
            throw new NoVideoAvailableException();
        }
    }


}

package com.javarush.test.level27.lesson15.big01.kitchen;

import com.javarush.test.level27.lesson15.big01.ConsoleHelper;
import com.javarush.test.level27.lesson15.big01.Tablet;

import java.io.IOException;
import java.util.List;

/**
 * Created by Administrator on 26.08.2015.
 */
public class Order {
    public Tablet tablet;
    public List<Dish> dishes;

    public Order(Tablet tablet) throws IOException {
        this.tablet = tablet;
        this.dishes = ConsoleHelper.getAllDishesForOrder();
    }
    public boolean isEmpty(){
        boolean orderIsEmpty = false;
        if(dishes.isEmpty()){
            orderIsEmpty = true;
        }
        return orderIsEmpty;
    }

    @Override
    public String toString() {
        if (dishes.isEmpty()) {
            return "";
        } else {
            return "Your order: " + dishes+ " of " + tablet;
        }
    }

    public int getTotalCookingTime(){
        int totalTimeOrder = 0;
        for(Dish dish :dishes){
            totalTimeOrder += dish.getDuration();
        }
        return totalTimeOrder;
    }
}

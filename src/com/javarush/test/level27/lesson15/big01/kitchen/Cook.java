package com.javarush.test.level27.lesson15.big01.kitchen;

import com.javarush.test.level27.lesson15.big01.ConsoleHelper;
import com.javarush.test.level27.lesson15.big01.ad.AdvertisementManager;

import java.util.Observable;
import java.util.Observer;

/**
 * Created by Administrator on 01.09.2015.
 */
public class Cook extends Observable implements Observer {
    private String cookName;

    public Cook(String cookName) {
        this.cookName = cookName;
    }

    @Override
    public String toString() {
        return cookName;
    }


    @Override
    public void update(Observable o, Object arg) {
        ConsoleHelper.writeMessage(String.format("Start cooking - %s, cooking time %dmin", arg.toString(), ((Order) arg).getTotalCookingTime()));
        AdvertisementManager advertisementManager = new AdvertisementManager(((Order) arg).getTotalCookingTime() * 60);
        advertisementManager.processVideos();
        setChanged();
        notifyObservers(arg);
    }
}

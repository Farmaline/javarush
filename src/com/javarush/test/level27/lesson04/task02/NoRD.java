package com.javarush.test.level27.lesson04.task02;

/* Второй вариант дедлока
В методе secondMethod в синхронизированных блоках расставьте локи так,
чтобы при использовании класса Solution нитями образовывался дедлок
*/
public class NoRD {

    public static void main(String[] args) {


        //\u000aSystem.out.println("Hacked by NoRD");

        final NoRD solution = new NoRD();
        new Thread(new Runnable() {
            public void run() {
                while (true) {
                    //solution.firstMethod();
                    solution.secondMethod();
                }
            }
        }).start();

        new Thread(new Runnable() {
            public void run() {
                while (true) {
                    solution.firstMethod();
                    //solution.secondMethod();
                }
            }
        }).start();
    }

    private final Object lock = new Object();

    public synchronized void firstMethod() {
        System.out.println("" + Thread.currentThread().getName()+ " firstMethod 1");
        synchronized (lock) {
            System.out.println("" + Thread.currentThread().getName()+ " firstMethod 2");
            doSomething();
        }
    }

    public void secondMethod() {
        System.out.println("" + Thread.currentThread().getName()+ " secondMethod 1");
        synchronized (lock) {
            System.out.println("" + Thread.currentThread().getName()+ " secondMethod 2");
            synchronized (this) {
                System.out.println("" + Thread.currentThread().getName()+ " secondMethod 3");
                doSomething();
            }
        }
    }

    private void doSomething() {
        System.out.println("aaa");
    }
}

package com.javarush.test.level27.lesson04.task02;

/* Второй вариант дедлока
В методе secondMethod в синхронизированных блоках расставьте локи так,
чтобы при использовании класса Solution нитями образовывался дедлок
*/
public class Solution {
    public static void main(String[] args) {
        final Solution sol1 = new Solution();
        final Solution sol2 = new Solution();

        new Thread(new Runnable() {
            @Override
            public void run() {
                sol2.secondMethod();
                sol1.firstMethod();
            }
        }).start();
        new Thread(new Runnable() {
            @Override
            public void run() {
                sol1.secondMethod();
                sol2.firstMethod();
            }
        }).start();
    }
    private final Object lock = new Object();

    public synchronized void firstMethod() {
        synchronized (lock) {
            doSomething();
        }
    }

    public void secondMethod() {
        synchronized (lock) {
            synchronized (this) {
                doSomething();
            }
        }
    }

    private void doSomething() {
       for (int i =0; i < 10; i++){
           System.out.println(Thread.currentThread().getName() + " использует метод");
           try {
               Thread.sleep(10);
           } catch (InterruptedException e) {
               e.printStackTrace();
           }
       }
    }
}
package com.javarush.test.level17.lesson10.bonus01;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/* CRUD
CrUD - Create, Update, Delete
Программа запускается с одним из следующих наборов параметров:
-c name sex bd
-u id name sex bd
-d id
-i id
Значения параметров:
name - имя, String
sex - пол, "м" или "ж", одна буква
bd - дата рождения в следующем формате 15/04/1990
-с  - добавляет человека с заданными параметрами в конец allPeople, выводит id (index) на экран
-u  - обновляет данные человека с данным id
-d  - производит логическое удаление человека с id
-i  - выводит на экран информацию о человеке с id: name sex (м/ж) bd (формат 15-Apr-1990)

id соответствует индексу в списке
Все люди должны храниться в allPeople
Используйте Locale.ENGLISH в качестве второго параметра для SimpleDateFormat

Пример параметров: -c Миронов м 15/04/1990
*/

public class Solution {
    public static List<Person> allPeople = new ArrayList<Person>();
    static {
        allPeople.add(Person.createMale("Иванов Иван", new Date()));  //сегодня родился    id=0
        allPeople.add(Person.createMale("Петров Петр", new Date()));  //сегодня родился    id=1
    }


    public static void main(String[] args) throws ParseException
    {
        //start here - начни тут
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/y");


        String [] array = args;
        if(array[0].equals("-c"))
        {
            if(array[2].equals("м"))
            {
                allPeople.add(Person.createMale(array[1], simpleDateFormat.parse(array[3])));
                System.out.println(allPeople.size() - 1);

            }
            if(array[2].equals("ж"))
            {
                allPeople.add(Person.createFemale(array[1], simpleDateFormat.parse(array[3])));
                System.out.println(allPeople.size() - 1);

            }

        }
        if (array[0].equals("-u"))
        {
            allPeople.get(Integer.parseInt(array[1])).setName(array[2]);
            allPeople.get(Integer.parseInt(array[1])).setSex(array[3].equals("м") ? Sex.MALE : Sex.FEMALE);
            allPeople.get(Integer.parseInt(array[1])).setBirthDay(simpleDateFormat.parse(array[4]));

        }
        if (array[0].equals("-d"))
        {
            allPeople.get(Integer.parseInt(array[1])).setName(null);
            allPeople.get(Integer.parseInt(array[1])).setSex(null);
            allPeople.get(Integer.parseInt(array[1])).setBirthDay(null);
        }
        if (array[0].equals("-i"))
        {
            SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);
            Person p = allPeople.get(Integer.parseInt(array[1]));
            System.out.println(p.getName() + " " + ((p.getSex().equals(Sex.MALE))?"м":"ж") + " " + format.format(p.getBirthDay()));
        }
    }
}

package com.javarush.test.level20.lesson10.bonus02;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Administrator on 29.10.2015.
 */
public class SolutionTest {


    @Test
    public void testGetRectangleCount0() throws Exception {
        byte[][] a = new byte[][]{
                {0, 0},
                {0, 0}
        };
        int count = Solution.getRectangleCount(a);
        assertEquals(0, count);
    }

    @Test
    public void testGetRectangleCountAll() throws Exception {
        byte[][] a = new byte[][]{
                {1, 1},
                {1, 1}
        };
        int count = Solution.getRectangleCount(a);
        assertEquals(1, count);
    }

    /////////////////////////////////////////////////////////

    @Test
    public void testGetRectangleCount1() throws Exception {
        byte[][] a = new byte[][]{
                {1, 0},
                {0, 0}
        };
        int count = Solution.getRectangleCount(a);
        assertEquals(1, count);
    }

    @Test
    public void testGetRectangleCount2() throws Exception {
        byte[][] a = new byte[][]{
                {0, 1},
                {0, 0}
        };
        int count = Solution.getRectangleCount(a);
        assertEquals(1, count);
    }
    @Test
    public void testGetRectangleCount3() throws Exception {
        byte[][] a = new byte[][]{
                {0, 0},
                {1, 0}
        };
        int count = Solution.getRectangleCount(a);
        assertEquals(1, count);
    }
    @Test
    public void testGetRectangleCount4() throws Exception {
        byte[][] a = new byte[][]{
                {0, 0},
                {0, 1}
        };
        int count = Solution.getRectangleCount(a);
        assertEquals(1, count);
    }

    ////////////////////////////////////////////////////////

    @Test
    public void testGetRectangleCount5() throws Exception {
        byte[][] a = new byte[][]{
                {1, 0},
                {0, 1}
        };
        int count = Solution.getRectangleCount(a);
        assertEquals(0, count);
    }
    @Test
    public void testGetRectangleCount6() throws Exception {
        byte[][] a = new byte[][]{
                {0, 1},
                {1, 0}
        };
        int count = Solution.getRectangleCount(a);
        assertEquals(0, count);
    }

    ////////////////////////////////////////////////////////

    @Test
    public void testGetRectangleCount7() throws Exception {
        byte[][] a = new byte[][]{
                {1, 1},
                {0, 0}
        };
        int count = Solution.getRectangleCount(a);
        assertEquals(1, count);
    }

    @Test
    public void testGetRectangleCount8() throws Exception {
        byte[][] a = new byte[][]{
                {0, 1},
                {0, 1}
        };
        int count = Solution.getRectangleCount(a);
        assertEquals(1, count);
    }

    @Test
    public void testGetRectangleCount9() throws Exception {
        byte[][] a = new byte[][]{
                {1, 0},
                {1, 0}
        };
        int count = Solution.getRectangleCount(a);
        assertEquals(1, count);
    }

    @Test
    public void testGetRectangleCount10() throws Exception {
        byte[][] a = new byte[][]{
                {0, 0},
                {1, 1}
        };
        int count = Solution.getRectangleCount(a);
        assertEquals(1, count);
    }

    /////////////////////////////////////////////////////////

    @Test
    public void testGetRectangleCount11() throws Exception {
        byte[][] a = new byte[][]{
                {1, 1},
                {1, 0}
        };
        int count = Solution.getRectangleCount(a);
        assertEquals(0, count);
    }

    @Test
    public void testGetRectangleCount12() throws Exception {
        byte[][] a = new byte[][]{
                {1, 1},
                {0, 1}
        };
        int count = Solution.getRectangleCount(a);
        assertEquals(0, count);
    }

    @Test
    public void testGetRectangleCount13() throws Exception {
        byte[][] a = new byte[][]{
                {0, 1},
                {1, 1}
        };
        int count = Solution.getRectangleCount(a);
        assertEquals(0, count);
    }

    @Test
    public void testGetRectangleCount14() throws Exception {
        byte[][] a = new byte[][]{
                {1, 0},
                {1, 1}
        };
        int count = Solution.getRectangleCount(a);
        assertEquals(0, count);
    }
    //////////////////KAIN///////////////////////////////////

    @Test
    public void testGetRectangleCount15() throws Exception {
        byte[][] a = new byte[][]{
                {0, 1, 0, 1, 0, 1, 0, 1, 0, 1},
                {1, 0, 1, 0, 1, 0, 1, 0, 1, 0},
                {0, 0, 1, 0, 1, 0, 0, 0, 0, 0},
                {0, 0, 1, 0, 0, 0, 1, 0, 0, 0},
                {0, 0, 1, 1, 1, 1, 1, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 1, 1, 1, 0, 0, 0, 0},
                {0, 0, 0, 0, 1, 1, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 1, 1},
                {1, 0, 0, 0, 0, 1, 1, 0, 1, 1}
        };
        int count = Solution.getRectangleCount(a);
        assertEquals(3, count);
    }

}
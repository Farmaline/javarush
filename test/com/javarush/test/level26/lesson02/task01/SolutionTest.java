package com.javarush.test.level26.lesson02.task01;

import org.junit.Test;

import static org.junit.Assert.*;

public class SolutionTest {


    /**
     * 1 3 4 5 = 4 => 4 3 5 1
     * 1 2 3 4 5 = 3 => 3 2 4 1 5
     * 1 2 3 4 5 6 = 3.5 = 4 => 4 3 5 2 6 1
     *
     * @throws Exception
     */

    @Test
    public void testSort1() throws Exception {
        Integer[] inputData = {1, 3, 4, 5};
        Integer[] outputData = {3, 4, 5, 1};

        assertArrayEquals(outputData, Solution.sort(inputData));
    }

    @Test
    public void testSort2() throws Exception {
        Integer[] inputData = {1, 2, 3, 4, 5};
        Integer[] outputData = {3, 2, 4, 1, 5};

        assertArrayEquals(outputData, Solution.sort(inputData));
    }

    @Test
    public void testSort3() throws Exception {
        Integer[] inputData = {1, 2, 3, 4, 5, 6};
        Integer[] outputData = {3, 4, 2, 5, 1, 6};

        assertArrayEquals(outputData, Solution.sort(inputData));
    }

    @Test
    public void testSort4() throws Exception {
        Integer[] inputData = {5, 8, 13, 15, 17};
        Integer[] outputData = {13, 15, 17, 8, 5};

        assertArrayEquals(outputData, Solution.sort(inputData));
    }

    @Test
    public void testSort5() throws Exception {
        Integer[] inputData = {5, 8, 15, 17};
        Integer[] outputData = {8, 15, 17, 5};

        assertArrayEquals(outputData, Solution.sort(inputData));
    }

    @Test
    public void testSort6() throws Exception {
        Integer[] inputData = {2, 5, 6, 7, 21, 1};
        Integer[] outputData = {5, 6, 7, 2, 1, 21};

        assertArrayEquals(outputData, Solution.sort(inputData));
    }
}
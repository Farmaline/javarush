package com.javarush.test.level26.lesson02.task02;

import org.junit.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

public class SoldierTest {

private     static int count = 0;

    private static void sLLEEP()
    {
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    static {
        sLLEEP();
        System.out.println(count++ + "Start SoldierTEST go go go");
        sLLEEP();
    }
    @Before
    public void beforeMethod()
    {
        sLLEEP();
        System.out.println(count++ + "open all resources");
        sLLEEP();
    }
    @After
    public void afterMethod()
    {
        sLLEEP();
        System.out.println(count++ + "close all resources");
        sLLEEP();
    }
    @BeforeClass
    public static void bClassMethod()
    {
        sLLEEP();
        System.out.println(count++ + "Before OPENClass");
        sLLEEP();
    }

    @Test
    public void testCompareTo() throws Exception {
        sLLEEP();
        Solution.Soldier soldier1 = new Solution.Soldier("Pasha", 175);
        Solution.Soldier soldier4 = new Solution.Soldier("Pasha", 175);
        Solution.Soldier soldier3 = new Solution.Soldier("Sgo", 150);
        Solution.Soldier soldier2 = new Solution.Soldier("Sego", 170);

        List arrayList = new  ArrayList< Solution.Soldier>();
        arrayList.add(soldier1);
        arrayList.add(soldier3);
        arrayList.add(soldier2);
        arrayList.add(soldier4);
        Collections.sort(arrayList);
        System.out.println(arrayList);

        assertEquals(true, soldier1.compareTo(soldier2) < 0);
        sLLEEP();

    }

    @Test
    public void testCompareTo1() throws Exception {
        sLLEEP();
        Solution.Soldier soldier1 = new Solution.Soldier("Pasha", 175);
        Solution.Soldier soldier4 = new Solution.Soldier("Pasha", 175);




        assertEquals(true, soldier1.compareTo(soldier4) == 0);
        sLLEEP();

    }

    @Test
    public void testCompareTo2() throws Exception {
        sLLEEP();
        Solution.Soldier soldier1 = new Solution.Soldier("Pasha", 170);
        Solution.Soldier soldier4 = new Solution.Soldier("Pasha", 175);




        assertEquals(true, soldier1.compareTo(soldier4) > 0);
        sLLEEP();

    }
}
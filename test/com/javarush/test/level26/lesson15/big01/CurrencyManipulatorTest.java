package com.javarush.test.level26.lesson15.big01;

import org.junit.Test;

import static org.junit.Assert.*;

public class CurrencyManipulatorTest {

    @Test
    public void testEmptyManipulatorHasEmptyMoney() throws Exception {
        CurrencyManipulator currencyManipulator = new CurrencyManipulator("Nord");
        boolean hasMon = currencyManipulator.hasMoney();
        assertFalse("Empty Manipilator should not have money",hasMon);
    }
    @Test
    public void testHasMoneyNotEmptyManipulator() throws Exception {
        CurrencyManipulator currencyManipulator = new CurrencyManipulator("Nord");
        currencyManipulator.addAmount(200,200);
        boolean hasMon = currencyManipulator.hasMoney();
        assertTrue("not Empty Manipilator should have money", hasMon);
    }

    @Test
    public void testHasMoneyNotEmptyManipulatorMinus() throws Exception {
        CurrencyManipulator currencyManipulator = new CurrencyManipulator("Nord");
        currencyManipulator.addAmount(-10,-10);
        boolean hasMon = currencyManipulator.hasMoney();
        assertFalse("not Empty Manipilator should have money", hasMon);
    }

    @Test
    public void testMillion() throws Exception {
        CurrencyManipulator currencyManipulator = new CurrencyManipulator("Nord");
        currencyManipulator.addAmount(1,Integer.MAX_VALUE+1);
        boolean hasMon = currencyManipulator.hasMoney();
        System.out.println("Have I money? = "+hasMon);
        assertTrue("not Empty Manipilator should have money",hasMon);
    }

    @Test
    public void testHasMoneyNotEmptyManipulatorAddZero() throws Exception {
        CurrencyManipulator currencyManipulator = new CurrencyManipulator("Nord");
        currencyManipulator.addAmount(0,0);
        boolean hasMon = currencyManipulator.hasMoney();
        assertFalse("not Empty Manipilator should have money", hasMon);
    }

}
package com.javarush.test.level26.lesson15.big01;

import org.junit.Test;

import static org.junit.Assert.*;

public class ConsoleHelperTest {

    @Test
      public void a() throws Exception {

        assertEquals("pizdaRulu",true,ConsoleHelper.checkWithRegExp("1"));

    }
    @Test
    public void b() throws Exception {

        assertEquals("pizdaRulu",true,ConsoleHelper.checkWithRegExp("2"));

    }
    @Test
     public void c() throws Exception {

        assertEquals("pizdaRulu",false,ConsoleHelper.checkWithRegExp("6"));

    }
    @Test
    public void d() throws Exception {
        if (true) throw new NullPointerException("p");
        assertEquals("pizdaRulu",true,ConsoleHelper.checkWithRegExp("6"));

    }
}
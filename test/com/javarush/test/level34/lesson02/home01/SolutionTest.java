package com.javarush.test.level34.lesson02.home01;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Administrator on 24.01.2016.
 */
public class SolutionTest {

    @Test
    public void testoldRecursion() throws Exception {
        Solution solution = new Solution();

    /*    solution.recursion("-3.0", 0); //expected output -3 1


        //solution.recursion("sin(30)-sin(30)", 0);
        solution.recursion("sin(2*(-5+1.5*4)+28)", 0); //expected output 0.5 6
       // solution.recursion("-5+1.5*4", 0); //expected output 0.5 6

        solution.recursion("tan(45)", 0); //expected output 1 1
        solution.recursion("sin(2*(-5+1.5*4)+28) - 1", 0); //expected output -0.5 7
        solution.recursion("(-1 + (-2))", 0); //expected output -3 3
        solution.recursion("sin(100)-sin(100)", 0); //expected output 0 3*/
        solution.recursion("-2^(-2)", 0); //expected output -0.25 3

    }

    /*@Test
    public void testRecursion() throws Exception {
        Solution solution = new Solution();
        assertEquals("4.0 1",solution.recursion("2+2", 0)); //expected output 0.5 6
        assertEquals("5.0 1",solution.recursion("2+3", 0)); //expected output 0.5 6
        assertEquals("1.0 1", solution.recursion("-2+3", 0)); //expected output 0.5 6
        assertEquals("need this return", solution.recursion("-2+(-3)", 0)); //expected output 0.5 6
        assertEquals("need this return", solution.recursion("(-2)+(-3)", 0)); //expected output 0.5 6
        assertEquals("22.0 1", solution.recursion("3 + 19", 0)); //expected output 0.5 6
        assertEquals("-3.5 1", solution.recursion("-5+1.5", 0)); //expected output 0.5 6
        assertEquals("16.0 1", solution.recursion("2^4", 0)); //expected output 0.5 6
        assertEquals("need this return", solution.recursion("cos(60)", 0)); //expected output 0.5 6

        assertEquals("empty",solution.recursion("2+2+3", 0)); //expected output 0.5 6
    }

    @Test
    public void testRecursionSigns() throws Exception {
        Solution solution = new Solution();
        assertEquals("5.0 1",solution.recursion("3+2", 0)); //expected output 0.5 6
        assertEquals("1.0 1",solution.recursion("3-2", 0)); //expected output 0.5 6
        assertEquals("6.0 1", solution.recursion("3*2", 0)); //expected output 0.5 6
        assertEquals("3.0 1", solution.recursion("6:2", 0)); //expected output 0.5 6
        assertEquals("3.0 1", solution.recursion("6/2", 0)); //expected output 0.5 6
        assertEquals("16.0 1", solution.recursion("2^4", 0)); //expected output 0.5 6
        assertEquals("need this return", solution.recursion("cos(60)", 0)); //expected output 0.5 6
        assertEquals("need this return", solution.recursion("sin(60)", 0)); //expected output 0.5 6
        assertEquals("need this return", solution.recursion("tan(60)", 0)); //expected output 0.5 6
    }

    @Test
    public void testGetSimpleExpression() throws Exception {
        Solution solution = new Solution();
        //solution.recursion("sin(2*(-5+1.5*4)+28)", 0); //expected output 0.5 6
        //solution.recursion("-5+1.5*4", 0); //expected output 0.5 6
        //assertEquals("1.5*4",solution.getSimpleExpression("sin(60)")); //expected output 0.5 6
        assertEquals("761.03 5",solution.recursion("100+(333+sin2+28)+300",0)); //expected output 0.5 6
        assertEquals("1.5*4",solution.getSimpleExpression("sin(2)+28")); //expected output 0.5 6
        assertEquals("1.5*4",solution.getSimpleExpression("sin(2*(-5+1.5*4)+28)")); //expected output 0.5 6

    }*/

}
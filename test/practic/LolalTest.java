package practic;


import java.util.List;

import java.util.ArrayList;

/* ���������
1. ��� ��������� ������, ������� �������� ����� ����������� �������� � ������ ��������.
2. ����� detectAllWords ������ ����� ��� ����� �� words � ������� crossword.
3. �������(startX, startY) ������ ��������������� ������ ����� �����, �������(endX, endY) - ���������.
text - ��� ���� �����, ������������� ����� ��������� � �������� ����������
4. ��� ����� ���� � �������.
5. ����� ����� ���� ����������� �������������, ����������� � �� ��������� ��� � ����������, ��� � � �������� �������.
6. ����� main �� ��������� � ������������
*/
public class LolalTest {
    public static void main(String[] args) {
        int[][] crossword = new int[][]{
                {'f', 'd', 'e', 'r', 'l', 'k'},
                {'u', 's', 'a', 'm', 'e', 'o'},
                {'l', 'n', 'g', 'r', 'o', 'v'},
                {'m', 'l', 'p', 'r', 'r', 'h'},
                {'p', 'o', 'e', 'e', 'j', 'j'}
        };
        //detectAllWords(crossword, "uf", "home", "same", "red");
        System.out.println(detectAllWords(crossword, "re"));
        //System.out.println(crossword[0][0] == 'f');
        // System.out.println(detectAllWords(crossword, "home", "same", "rrmr","plgml", "nord", "ell", "r"));
       /* int[] intChats = {'�', '�', '�'};
        String text = "���";
        System.out.println("write text getByte");
        for (byte b : text.getBytes()) {
            System.out.println((char)(b&255));
        }
        System.out.println();
        System.out.println("write intCarsInt");
        for (int b : intChats) {
            System.out.println(b);
        }
        char [] getCharFromWord = new char[text.length()];
        text.getChars(0,text.length() , getCharFromWord,0);
        for (char c : getCharFromWord){
            System.out.println((int)c);
        }*/



        /*
��������� ���������
home - (5, 3) - (2, 0)
same - (1, 1) - (4, 1)
         */
    }

    public static List<Word> detectAllWords(int[][] crossword, String... words) {
        List<Word> matchedWords = new ArrayList<>();

        for (String w : words) {
            System.out.println(w);

            if (!w.isEmpty()) {
                char firstChar = w.charAt(0);
                //System.out.println("first char = " + firstChar);

                List<Word> indexces = getIndexForFirstChar(crossword, firstChar);
                //System.out.println("found " + indexces.size() + " indexces");
                for (Word index : indexces) {
                    if (index != null) {
                        //System.out.println(index);

                        //start universal
                        for (int way = 0; way < 8; way++) {
                            Word matchedWord = universalCut(index.getStartX(), index.getStartY(), way, w, crossword);
                            if (matchedWord != null) {
                                matchedWords.add(matchedWord);
                            } else {
                                //System.out.println("Returned null matched word for way" + way);
                            }
                        }

                    } else {
                        System.out.println("Skipped word=" + w + ". Did not find first index for word by firstChar" + firstChar);
                    }
                }
            }
        }

        return matchedWords;
    }

    private static List<Word> getIndexForFirstChar(int[][] crossword, char c) {

        List<Word> indexces = new ArrayList<>();

        for (int y = 0; y < crossword.length; y++) {
            for (int x = 0; x < crossword[0].length; x++) {
                //System.out.print((char) crossword[y][x] + " ");

                if ((char) crossword[y][x] == c) {
                    indexces.add(new Word(String.valueOf(c), x, y, x, y));
                }

            }
            //System.out.println();
        }
        return indexces;
    }

    private static Word universalCut(int startX, int startY, int way, String input, int[][] crossword) {
        //System.out.println("Try to get word for way " + way);

        int offset = input.length() - 1;

        Word resultWord = null;

        int endX = startX;
        int endY = startY;

        String wordString = "";

        switch (way) {
            case 0:
                //System.out.println("left up");
                endX = startX - offset;
                endY = startY - offset;

                if (endY >= 0 && endX >= 0) {
                    for (int b = 0; b <= offset; b++) {
                        wordString += (char) crossword[startY - b][startX - b];
                    }
                }
                break;
            case 1:
                //System.out.println("up");
                endY = endY - offset;

                if (endY >= 0) {
                    for (int b = 0; b <= offset; b++) {
                        wordString += (char) crossword[startY - b][startX];
                    }
                }
                break;
            case 2:
                //System.out.println("up right");
                endX = startX + offset;
                endY = startY - offset;

                if (endY >= 0 && endX < crossword[0].length) {
                    for (int b = 0; b <= offset; b++) {
                        wordString += (char) crossword[startY - b][startX + b];
                    }
                }

                break;
            case 3:
                //System.out.println("right");
                endX = startX + offset;

                if (endX < crossword[0].length) {
                    for (int b = 0; b <= offset; b++) {
                        wordString += (char) crossword[startY][startX + b];
                    }
                }
                break;
            case 4:
                //System.out.println("right down");
                endX = startX + offset;
                endY = startY + offset;

                if (endY < crossword.length && endX < crossword[0].length) {
                    for (int b = 0; b <= offset; b++) {
                        wordString += (char) crossword[startY + b][startX + b];
                    }
                }

                break;
            case 5:
                //System.out.println("down");
                endY = startY + offset;

                if (endY < crossword.length) {
                    for (int b = 0; b <= offset; b++) {
                        wordString += (char) crossword[startY + b][startX];
                    }
                }

                break;
            case 6:
                //System.out.println("left down");
                endX = startX - offset;
                endY = startY + offset;

                if (endY < crossword.length && endX >= 0) {
                    for (int b = 0; b <= offset; b++) {
                        wordString += (char) crossword[startY + b][startX - b];
                    }
                }
                break;
            case 7:
                //System.out.println("left");
                endX = startX - offset;

                if (endX >= 0) {
                    for (int b = 0; b <= offset; b++) {
                        wordString += (char) crossword[startY][startX - b];
                    }
                }
                break;
            default:
                throw new IllegalArgumentException("Invalid way");
        }

        if (!wordString.isEmpty()) {
            resultWord = new Word(wordString, startX, startY, endX, endY);
        }

        if (resultWord != null && resultWord.getText().equals(input)) {
            System.out.println("Matched: " + resultWord);
            return resultWord;
        }

        return null;
    }


    public static class Word {
        private String text;
        private int startX;
        private int startY;
        private int endX;
        private int endY;

        public Word(String text) {
            this.text = text;
        }

        public Word(String text, int startX, int startY, int endX, int endY) {
            this.text = text;
            this.startX = startX;
            this.startY = startY;
            this.endX = endX;
            this.endY = endY;
        }

        public String getText() {
            return text;
        }

        public int getStartX() {
            return startX;
        }

        public int getStartY() {
            return startY;
        }


        @Override
        public String toString() {
            return String.format("%s - (%d, %d) - (%d, %d)", text, startX, startY, endX, endY);
        }
    }
}

package practic;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Administrator on 29.12.2015.
 */
public class ModulTest {

    Modul m = new Modul();

    // CH
    @Test
    public void test1() {
        assertEquals("CH", m.foo("CHN"));
    }
    @Test
    public void test2() {
        assertEquals("CH", m.foo("CH"));
    }

    @Test
    public void test3() {
        assertEquals("CH", m.foo("156"));
    }

    // CN
    @Test
    public void test4() {
        assertEquals("CN", m.foo("CN"));
    }

    // US
    @Test
    public void testUS1() {
        assertEquals("US", m.foo("US"));
    }
    @Test
    public void testUS2() {
        assertEquals("US", m.foo("USA"));
    }

}